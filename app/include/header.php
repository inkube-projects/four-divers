    <!-- Header-->
    <nav class="navbar navbar-expand-md navbar-default fixed-top">
        <div class="navbar-header">
            <div id="mobile-menu">
                <div class="left-nav-toggle">
                    <a href="#">
                        <i class="stroke-hamburgermenu"></i>
                    </a>
                </div>
            </div>
            <a class="navbar-brand" href="index.html">
                <img src="images/logo_nav.jpg" alt=""/>
            </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <div class="left-nav-toggle">
                <a href="">
                    <i class="stroke-hamburgermenu"></i>
                </a>
            </div>
            <form class="navbar-form mr-auto">
                
            </form>
            <ul class="nav navbar-nav">
              
                <li class="nav-item profil-link">
                    <a href="login.html">
                        <span class="profile-address">Rafa Pastrana</span>
                        <img src="images/profile.jpg" class="rounded-circle" alt="">
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- End header-->

    <!-- Navigation-->
    <aside class="navigation">
        <nav>
            <ul class="nav luna-nav">

                <li class="active">
                    <a href="#">Escritorio</a>
                </li>
				
						<li class="nav-category">
                    GESTIÓN
                </li>
				
						<li>
                    <a href="#">Agenda/Tareas</a>
                </li>
				
                <li>
                    <a href="#reservas" data-toggle="collapse" aria-expanded="false">
                        Reservas<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="reservas" class="nav nav-second collapse">
                        <li><a href="#"> Listado</a></li>
									<li><a href="#"> Salidas</a></li>
                        <li><a href="#"> Cancelaciones</a></li>

                    </ul>
                </li>

				
                <li>
                    <a href="#ventas" data-toggle="collapse" aria-expanded="false">
                        Ventas<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="ventas" class="nav nav-second collapse">
                        <li><a href="#"> Listado</a></li>
                        <li><a href="#"> Abonos</a></li>
									<li><a href="#"> Envios</a></li>
                    </ul>
                </li>
				
					    <li>
                    <a href="#usuarios" data-toggle="collapse" aria-expanded="false">
                        Usuarios<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="usuarios" class="nav nav-second collapse">
                        <li><a href="#"> Clientes</a></li>
                        <li><a href="#"> Usuarios</a></li>
                    </ul>
                </li>
				
				
						<li class="nav-category">
                    COMUNICACIÓN
                </li>
                <li>
                    <a href="#notificaciones" data-toggle="collapse" aria-expanded="false">
                        Notificaciones<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="notificaciones" class="nav nav-second collapse">
                        <li><a href="#">Alertas</a></li>
                        <li><a href="#l">Renovaciones</a></li>
                        <li><a href="#">Avisos</a></li>
                    </ul>
                </li>
						
						 <li>
                    <a href="#blog" data-toggle="collapse" aria-expanded="false">
                        Blog<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="blog" class="nav nav-second collapse">
                        <li><a href="#"> Listado</a></li>
                        <li><a href="#"> Comentarios</a></li>
                    </ul>
                </li>
				
						<li>
              			<a href="#">Slider Home</a>
                </li>
						
						<li>
              			<a href="#">Vales regalo</a>
                </li>
				
				
				
						<li class="nav-category">
                    CATÁLOGO
                </li>
                <li>
                    <a href="#cursos" data-toggle="collapse" aria-expanded="false">
                        Servicios<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="cursos" class="nav nav-second collapse">
                        <li><a href="#"> Cursos</a></li>
                        <li><a href="#l">Actividades</a></li>
                        <li><a href="#"> Alojamiento</a></li>
									<li><a href="#"> Cruceros</a></li>
									<li><a href="#"> Seguros</a></li>
                    </ul>
                </li>
						
						 <li>
                    <a href="#material" data-toggle="collapse" aria-expanded="false">
                        Material<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="material" class="nav nav-second collapse">
                        <li><a href="#"> Productos</a></li>
                        <li><a href="#"> Equipamiento</a></li>
                    </ul>
                </li>
				
						<li>
              			<a href="#">Barco</a>
                </li>
				
				
				
						<li class="nav-category">
                   FOURDIVERS
                </li>
                <li>
                    <a href="#empresa" data-toggle="collapse" aria-expanded="false">
                        Empresa<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="empresa" class="nav nav-second collapse">
                        <li><a href="#"> Perfil</a></li>
                        <li><a href="#l"> Presentación</a></li>
                        <li><a href="#"> Certificaciones</a></li>
                    </ul>
                </li>
						
						 <li>
                    <a href="#equipo" data-toggle="collapse" aria-expanded="false">
                        Equipo<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="equipo" class="nav nav-second collapse">
                        <li><a href="#"> Administradores</a></li>
                        <li><a href="#"> Equipo humano</a></li>
                    </ul>
                </li>


       
                <li class="nav-info">
                    <i class="pe pe-7s-shield text-accent"></i>

                    <div class="m-t-xs">
                        <span class="c-white">FOURDIVERS</span><br> admin BUILDING PRO V.1.
                    </div>
                </li>
            </ul>
        </nav>
    </aside>
    <!-- End navigation-->