    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link  rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" type="text/css" />

   
		
    <!-- Vendor styles -->
    <link rel="stylesheet" href="js/fontawesome/css/font-awesome.css"/>
    <link rel="stylesheet" href="js/animate.css/animate.css"/>
    <link rel="stylesheet" href="js/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="js/toastr/toastr.min.css"/>

    <!-- App styles -->
    <link rel="stylesheet" href="css/pe-icons/pe-icon-7-stroke.css"/>
    <link rel="stylesheet" href="css/pe-icons/helper.css"/>
    <link rel="stylesheet" href="css/stroke-icons/style.css"/>
    <link rel="stylesheet" href="css/style.css">
	 <link rel="stylesheet" href="css/responsive.css">
	 <link rel="stylesheet" href="css/glyphicon.css">