<!DOCTYPE html>
<html>
<head>
    <title>ESCRITRORIO - FORDIVERS</title>
	<?php include('include/head.php'); ?>
</head>
<body>

<!-- Wrapper-->
<div class="wrapper">

<!--Header menu-->
<?php include('include/header.php'); ?>
<!--End header menu-->

    <!-- Main content-->
    <section class="content">
        <div class="container-fluid">

			
					<!--TITULAR  SECCION-->
				  	<div class="row">
                <div class="col-lg-12">
                    <div class="view-header">
 
                        <div class="header-icon">
                            <i class="pe page-header-icon pe-7s-keypad"></i>
                        </div>
                        <div class="header-title">
                            <h3 class="m-b-xs">Inicio/Escritorio</h3>
                            <small>
                                Información más relevante de lo que ocurre en la aplicación.
                            </small>
                        </div>
                    </div>
                    <hr>
                </div>
            </div>
				<!--END. TITULAR  SECCION-->
			
			  <div class="row">

				  
				  <!---PANAEL RESERVAS-->
				  	 <div class="col-md-6 col-lg-4">
                    <div class="panel panel-filled">
						<div class="panel-heading css-text18">Reservas <br><small>pendientes</small></div>
                        <div class="panel-body">
							
										<div class="d-inline-block">
											<i class="fa fa-shopping-cart text-warning css-text34"> </i>
										</div>
                            <h2 class="d-inline-block css-marginL20">
                                10 <span class="slight"><i class="fa fa-play fa-rotate-270 text-warning"> </i></span>
                            </h2>
	
										<button class="btn btn-default float-right css-paddingL20 css-paddingR20" type="button"> ver </button>

                        </div>
                    </div>
                </div>
				  <!--END PANEL RESERRVAS-->
				  
				    <!---PANEL PTE DE COBRO-->
				  	 <div class="col-md-6 col-lg-4">
                    <div class="panel panel-filled">
						<div class="panel-heading css-text18">Facturas <br><small>pte de cobro</small></div>
                        <div class="panel-body">
							
										<div class="d-inline-block">
											<i class="fa fa-eur text-danger css-text34"> </i>
										</div>
                            <h2 class="d-inline-block css-marginL20">
                                18 <span class="slight"><i class="fa fa-play fa-rotate-270 text-warning"> </i></span>
                            </h2>
	
										<button class="btn btn-default float-right css-paddingL20 css-paddingR20" type="button"> ver </button>

                        </div>
                    </div>
                </div>
				  <!--END PANEL PTE DE COBRO-->
				  
				  <!---PANEL RENOVACIONES-->
				  	 <div class="col-md-6 col-lg-4">
                    <div class="panel panel-filled">
						<div class="panel-heading css-text18">Renovaciones <br><small>proximamente</small></div>
                        <div class="panel-body">
							
										<div class="d-inline-block">
											<i class="fa  fa-clock-o text-info css-text34"> </i>
										</div>
                            <h2 class="d-inline-block css-marginL20">
                                24 <span class="slight"><i class="fa fa-play fa-rotate-270 text-warning"> </i></span>
                            </h2>
	
										<button class="btn btn-default float-right css-paddingL20 css-paddingR20" type="button"> ver </button>

                        </div>
                    </div>
                </div>
				  <!--END PANEL RENOVACIONES-->
				  
				  
				   <!---PANEL TAREAS-->
				  	 <div class="col-md-6 col-lg-4">
                    <div class="panel panel-filled">
						<div class="panel-heading css-text18">Tareas <br><small>pendientes </small></div>
                        <div class="panel-body">
							
										<div class="d-inline-block">
											<i class="fa fa-calendar text-primary css-text34"> </i>
										</div>
                            <h2 class="d-inline-block css-marginL20">
                                68 <span class="slight"><i class="fa fa-play fa-rotate-270 text-warning"> </i></span>
                            </h2>
	
										<button class="btn btn-default float-right css-paddingL20 css-paddingR20" type="button"> ver </button>

                        </div>
                    </div>
                </div>
				  <!--END PANEL TAREAS-->
				  
				  <!---PANAEL ALERTAS-->
				  	 <div class="col-md-6 col-lg-4">
                    <div class="panel panel-filled">
						<div class="panel-heading css-text18">Alertas <br><small>avisos importantes</small></div>
                        <div class="panel-body">
							
										<div class="d-inline-block">
											<i class="fa fa-exclamation-triangle text-alert css-text34"> </i>
										</div>
                            <h2 class="d-inline-block css-marginL20">
                                10 <span class="slight"><i class="fa fa-play fa-rotate-270 text-warning"> </i></span>
                            </h2>
	
										<button class="btn btn-default float-right css-paddingL20 css-paddingR20" type="button"> ver </button>

                        </div>
                    </div>
                </div>
				  <!--END PANEL ALERTAS-->
				  

				  
				  <!---PANEL CLIENTES-->
				  	 <div class="col-md-6 col-lg-4">
                    <div class="panel panel-filled">
						<div class="panel-heading css-text18">Clientes <br><small>nuevos usuarios</small></div>
                        <div class="panel-body">
							
										<div class="d-inline-block">
											<i class="fa fa-users text-success css-text34"> </i>
										</div>
                            <h2 class="d-inline-block css-marginL20">
                                06 <span class="slight"><i class="fa fa-play fa-rotate-270 text-warning"> </i></span>
                            </h2>
	
										<button class="btn btn-default float-right css-paddingL20 css-paddingR20" type="button"> ver </button>

                        </div>
                    </div>
                </div>
				  <!--END PANEL CLIENTES-->
				  
					</div>
			
			<hr>
			
			<div class="row">

                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading paddingTopAb">
											 <i class="pe page-header-icon pe-7s-timer css-text34 text-danger ico-absolute"></i> PRÓXIMA SALIDA : <code>23/04/20-17:30</code> <code>sencilla</code>
							<div class="cnt_plazas"> <small>REF: 02347 - Plazas </small><div class="css-circulo-avisos label-avisos d-inline-block">10</div> <a class="btn btn-default"><i class="fa fa-eye"></i></a></div>
                        </div>
						
                        <div class="panel-body">
                            <div class="tipo_salidas css-marginB10">
												
												<span class="label label-default">inmersión</span>
												<span class="label label-default">curso</span>
												<span class="label label-default">bautismo</span>
										</div>

                            <table class="table table-hover  table-responsive-sm">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Tipo</th>
                                    <th>Equipo</th>
													  <th><i class="fa fa-user-md css-text24" aria-hidden="true"></i></th>
													  <th><i class="fa fa-life-ring css-text24" aria-hidden="true"></i></th>
                                    <th><i class="fa fa-exclamation-circle css-text24" aria-hidden="true"></i></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
													  <td><small>ID:02345</small><br>Nombre Apellido1 Apellido 2</td>
                                    <td class="css-lineheigt38">Inmersión</td>
                                    <td class="css-lineheigt38">Propio</td>
													  <td class="css-lineheigt38"><i class="fa fa-times text-danger css-text20" aria-hidden="true"></i></td>
													  <td class="css-lineheigt38"><i class="fa fa-check text-success css-text20" aria-hidden="true"></i></td>
                                    <td class="css-lineheigt38"><i class="fa fa-hourglass-end text-success css-text20" aria-hidden="true"></i></td>
                                    
                                </tr>
									        <tr>
													  
                                    <td><small>ID:02345</small><br>Nombre Apellido1 Apellido 2</td>
                                    <td class="css-lineheigt38">Curso</td>
                                    <td class="css-lineheigt38">Incluido <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalTallas"> tallas </button></td>
													<td class="css-lineheigt38"><i class="fa fa-times text-danger css-text20" aria-hidden="true"></i></td>
													  <td class="css-lineheigt38"><i class="fa fa-check text-success css-text20" aria-hidden="true"></i></td>
                                    <td class="css-lineheigt38"><i class="fa fa-hourglass-start text-danger css-text20" aria-hidden="true"></i></td>
                                    
                                </tr>
												<tr>
													  
                                    <td><small>ID:02345</small><br>Nombre Apellido1 Apellido 2</td>
                                    <td class="css-lineheigt38">Bautizo</td>
                                    <td class="css-lineheigt38">Parcial <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalTallas"> tallas </button></td>
														<td class="css-lineheigt38"><i class="fa fa-times text-danger css-text20" aria-hidden="true"></i></td>
													  <td class="css-lineheigt38"><i class="fa fa-check text-success css-text20" aria-hidden="true"></i></td>
                                    <td class="css-lineheigt38"><i class="fa fa-hourglass-start text-danger css-text20" aria-hidden="true"></i></td>
                                    
                                </tr>
												<tr>
													  
                                    <td><small>ID:02345</small><br>Nombre Apellido1 Apellido 2</td>
                                    <td class="css-lineheigt38">Bautizo</td>
                                    <td class="css-lineheigt38">Completo <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalTallas"> tallas </button></td>
													 <td class="css-lineheigt38"><i class="fa fa-times text-danger css-text20" aria-hidden="true"></i></td>
													  <td class="css-lineheigt38"><i class="fa fa-check text-success css-text20" aria-hidden="true"></i></td>
                                    <td class="css-lineheigt38"><i class="fa fa-hourglass-half text-warning css-text20" aria-hidden="true"></i></td>
                                    
                                </tr>
												<tr>
													  
                                    <td><small>ID:02345</small><br>Nombre Apellido1 Apellido 2</td>
                                    <td class="css-lineheigt38">Snorkel</td>
                                    <td class="css-lineheigt38">Parcial <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalTallas"> tallas </button></td>
													 <td class="css-lineheigt38"><i class="fa fa-times text-danger css-text20" aria-hidden="true"></i></td>
													  <td class="css-lineheigt38"><i class="fa fa-check text-success css-text20" aria-hidden="true"></i></td>
                                    <td class="css-lineheigt38"><i class="fa fa-hourglass-end text-success css-text20" aria-hidden="true"></i></td>
                                    
                                </tr>
												<tr>
													  
													  <td><small>ID:02345</small><br>Nombre Apellido1 Apellido 2</td>
                                    <td class="css-lineheigt38">Inmersión</td>
                                    <td class="css-lineheigt38">Propio</td>
														<td class="css-lineheigt38"><i class="fa fa-times text-danger css-text20" aria-hidden="true"></i></td>
													  <td class="css-lineheigt38"><i class="fa fa-check text-success css-text20" aria-hidden="true"></i></td>
                                    <td class="css-lineheigt38"><i class="fa fa-hourglass-end text-success css-text20" aria-hidden="true"></i></td>
                                    
                                </tr>
									        <tr>
													  
                                    <td><small>ID:02345</small><br>Nombre Apellido1 Apellido 2</td>
                                    <td class="css-lineheigt38">Curso</td>
                                    <td class="css-lineheigt38">Incluido <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalTallas"> tallas </button></td>
													 <td class="css-lineheigt38"><i class="fa fa-times text-danger css-text20" aria-hidden="true"></i></td>
													  <td class="css-lineheigt38"><i class="fa fa-check text-success css-text20" aria-hidden="true"></i></td>
                                    <td class="css-lineheigt38"><i class="fa fa-hourglass-start text-danger css-text20" aria-hidden="true"></i></td>
                                    
                                </tr>
												<tr>
													  
                                    <td><small>ID:02345</small><br>Nombre Apellido1 Apellido 2</td>
                                    <td class="css-lineheigt38">Bautizo</td>
                                    <td class="css-lineheigt38">Parcial <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalTallas"> tallas </button></td>
														<td class="css-lineheigt38"><i class="fa fa-times text-danger css-text20" aria-hidden="true"></i></td>
													  <td class="css-lineheigt38"><i class="fa fa-check text-success css-text20" aria-hidden="true"></i></td>
                                    <td class="css-lineheigt38"><i class="fa fa-hourglass-start text-danger css-text20" aria-hidden="true"></i></td>
                                    
                                </tr>
												<tr>
													  
                                    <td><small>ID:02345</small><br>Nombre Apellido1 Apellido 2</td>
                                    <td class="css-lineheigt38">Bautizo</td>
                                    <td class="css-lineheigt38">Completo <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalTallas"> tallas </button></td>
														<td class="css-lineheigt38"><i class="fa fa-times text-danger css-text20" aria-hidden="true"></i></td>
													  <td class="css-lineheigt38"><i class="fa fa-check text-success css-text20" aria-hidden="true"></i></td>
                                    <td class="css-lineheigt38"><i class="fa fa-hourglass-half text-warning css-text20" aria-hidden="true"></i></td>
                                    
                                </tr>
												<tr>
													  
                                    <td><small>ID:02345</small><br>Nombre Apellido1 Apellido 2</td>
                                    <td class="css-lineheigt38">Snorkel</td>
                                    <td class="css-lineheigt38">Parcial <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalTallas"> tallas </button></td>
														<td class="css-lineheigt38"><i class="fa fa-times text-danger css-text20" aria-hidden="true"></i></td>
													  <td class="css-lineheigt38"><i class="fa fa-check text-success css-text20" aria-hidden="true"></i></td>
                                    <td class="css-lineheigt38"><i class="fa fa-hourglass-end text-success css-text20" aria-hidden="true"></i></td>
                                    
                                </tr>
                               
                                </tbody>
                            </table>

                        </div>
						
									<hr>
									<div class="fa-pull-right">
									<i class="fa fa-hourglass-start text-danger css-text16" aria-hidden="true"> <smal>Pendiente </smal></i>
									<i class="fa fa-hourglass-half text-warning css-text16" aria-hidden="true"> Con Instructor </i>
									<i class="fa fa-hourglass-end text-success css-text16" aria-hidden="true"> Preparado </i>
									<i class="fa fa-user-md text-default css-text16" aria-hidden="true"> Revisión médica </i>
									<i class="fa fa-life-ring text-default css-text16" aria-hidden="true"> Seguro </i>
									</div>
						
                    </div>
                </div>
                

            </div>
			


        </div>
    </section>
    <!-- End main content-->

</div>
<!-- End wrapper-->
	
	
<?php include('include/modal.php'); ?>
	
<?php include('include/js.php'); ?>
</body>

</html>