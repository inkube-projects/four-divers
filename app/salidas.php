			  <?php
			  $contenido1='
			  		<div class="col-lg-2 col-6">
						<div class="panel panel-filled">
							<div class="panel-body">
								<div class="pull-right">
								<a href="#" class="btn btn-default btn-xs">+ ver</a>
								</div>
								<div class="small text-accent">REF:123456</div>
								<h2 class="m-b-none">
									08 <span class="css-text18"><i class="fa fa-minus-circle text-warning"> </i> 04</span>
								</h2>
								<div class="small">PLAZAS</div>
								<div class="slight m-t-sm css-txt-13"><span class="text-warning">12/06/20 - 10:30</span> DOBLE</div>
							</div>
						</div>
					</div>
			  
			  ';
			  $contenido2='
			  		<div class="col-lg-2 col-6">
						<div class="panel panel-filled">
							<div class="panel-body">
								<div class="pull-right">
								<a href="#" class="btn btn-default btn-xs">+ ver</a>
								</div>
								<div class="small text-accent">REF:123456</div>
								<h2 class="m-b-none">
									12 <span class="css-text18"><i class="fa fa-check-circle text-success"> </i> 00</span>
								</h2>
								<div class="small">PLAZAS</div>
								<div class="slight m-t-sm css-txt-13"><span class="text-warning">04/05/20 - 10:30</span> SENCILLA</div>
							</div>
						</div>
					</div>
			  
			  ';
			  $contenido3='
			  		<div class="col-lg-2 col-6">
						<div class="panel panel-filled">
							<div class="panel-body">
								<div class="pull-right">
								<a href="#" class="btn btn-default btn-xs">+ ver</a>
								</div>
								<div class="small text-accent">REF:123456</div>
								<h2 class="m-b-none">
									00 <span class="css-text18"><i class="fa fa-times-circle  text-danger"> </i> 12</span>
								</h2>
								<div class="small">PLAZAS</div>
								<div class="slight m-t-sm css-txt-13"><span class="text-warning">24/04/20 - 10:30</span> DOBLE</div>
							</div>
						</div>
					</div>
			  
			  ';
			  ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link  rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" type="text/css" />

    <!-- Page title -->
    <title>SALIDAS - FORDIVERS</title>
		
    <!-- Vendor styles -->
    <link rel="stylesheet" href="js/fontawesome/css/font-awesome.css"/>
    <link rel="stylesheet" href="js/animate.css/animate.css"/>
    <link rel="stylesheet" href="js/bootstrap/css/bootstrap.css"/>
	
	  <!--DATA TABLE-->
    <link rel="stylesheet" href="js/datatables/datatables.min.css"/>
	
		<!--MULTISELECT-->
	  <link rel="stylesheet" href="js/select2/dist/css/select2.min.css"/>

    <!-- App styles -->
    <link rel="stylesheet" href="css/pe-icons/pe-icon-7-stroke.css"/>
    <link rel="stylesheet" href="css/pe-icons/helper.css"/>
    <link rel="stylesheet" href="css/stroke-icons/style.css"/>
    <link rel="stylesheet" href="css/style.css">
	 <link rel="stylesheet" href="css/responsive.css">
	 <link rel="stylesheet" href="css/glyphicon.css">
	
</head>
<body>

<!-- Wrapper-->
<div class="wrapper">

    <!-- Header-->
    <nav class="navbar navbar-expand-md navbar-default fixed-top">
        <div class="navbar-header">
            <div id="mobile-menu">
                <div class="left-nav-toggle">
                    <a href="#">
                        <i class="stroke-hamburgermenu"></i>
                    </a>
                </div>
            </div>
            <a class="navbar-brand" href="index.html">
                <img src="images/logo_nav.jpg" alt=""/>
            </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <div class="left-nav-toggle">
                <a href="">
                    <i class="stroke-hamburgermenu"></i>
                </a>
            </div>
            <form class="navbar-form mr-auto">
                
            </form>
            <ul class="nav navbar-nav">
              
                <li class="nav-item profil-link">
                    <a href="login.html">
                        <span class="profile-address">Rafa Pastrana</span>
                        <img src="images/profile.jpg" class="rounded-circle" alt="">
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- End header-->

    <!-- Navigation-->
    <aside class="navigation">
        <nav>
            <ul class="nav luna-nav">

                <li>
                    <a href="#">Escritorio</a>
                </li>
				
						<li class="nav-category">
                    GESTIÓN
                </li>
				
						<li>
                    <a href="#">Agenda/Tareas</a>
                </li>
				
                <li class="active">
                    <a href="#reservas" data-toggle="collapse" aria-expanded="false">
                        Reservas<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="reservas" class="nav nav-second collapse">
                        <li><a href="#"> Listado</a></li>
									<li><a href="#"> Salidas</a></li>
                        <li><a href="#"> Cancelaciones</a></li>
                    </ul>
                </li>

				
                <li>
                    <a href="#ventas" data-toggle="collapse" aria-expanded="false">
                        Ventas<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="ventas" class="nav nav-second collapse">
                        <li><a href="#"> Listado</a></li>
                        <li><a href="#l"> Abonos</a></li>
									<li><a href="#l"> Envios</a></li>
                    </ul>
                </li>
				
					    <li>
                    <a href="#usuarios" data-toggle="collapse" aria-expanded="false">
                        Usuarios<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="usuarios" class="nav nav-second collapse">
                        <li><a href="#"> Clientes</a></li>
                        <li><a href="#"> Usuarios</a></li>
                    </ul>
                </li>
				
				
						<li class="nav-category">
                    COMUNICACIÓN
                </li>
                <li>
                    <a href="#notificaciones" data-toggle="collapse" aria-expanded="false">
                        Notificaciones<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="notificaciones" class="nav nav-second collapse">
                        <li><a href="#">Alertas</a></li>
                        <li><a href="##">Renovaciones</a></li>
                        <li><a href="#">Avisos</a></li>
                    </ul>
                </li>
						
						 <li>
                    <a href="#blog" data-toggle="collapse" aria-expanded="false">
                        Blog<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="blog" class="nav nav-second collapse">
                        <li><a href="#"> Listado</a></li>
                        <li><a href="#"> Comentarios</a></li>
                    </ul>
                </li>
				
						<li>
              			<a href="#">Slider Home</a>
                </li>
						
						<li>
              			<a href="#">Vales regalo</a>
                </li>
				
				
				
						<li class="nav-category">
                    CATÁLOGO
                </li>
                <li>
                    <a href="#cursos" data-toggle="collapse" aria-expanded="false">
                        Servicios<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="cursos" class="nav nav-second collapse">
                        <li><a href="#"> Cursos</a></li>
                        <li><a href="#l">Actividades</a></li>
                        <li><a href="#"> Alojamiento</a></li>
									<li><a href="#"> Cruceros</a></li>
									<li><a href="#"> Seguros</a></li>
                    </ul>
                </li>
						
						 <li>
                    <a href="#material" data-toggle="collapse" aria-expanded="false">
                        Material<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="material" class="nav nav-second collapse">
                        <li><a href="#"> Productos</a></li>
                        <li><a href="#"> Equipamiento</a></li>
                    </ul>
                </li>
				
						<li>
              			<a href="#">Barco</a>
                </li>
				
				
				
						<li class="nav-category">
                   FOURDIVERS
                </li>
                <li>
                    <a href="#empresa" data-toggle="collapse" aria-expanded="false">
                        Empresa<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="empresa" class="nav nav-second collapse">
                        <li><a href="#"> Perfil</a></li>
                        <li><a href="#l"> Presentación</a></li>
                        <li><a href="#"> Certificaciones</a></li>
                    </ul>
                </li>
						
						 <li>
                    <a href="#equipo" data-toggle="collapse" aria-expanded="false">
                        Equipo<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="equipo" class="nav nav-second collapse">
                        <li><a href="#"> Administradores</a></li>
                        <li><a href="#"> Equipo humano</a></li>
                    </ul>
                </li>


       
                <li class="nav-info">
                    <i class="pe pe-7s-shield text-accent"></i>

                    <div class="m-t-xs">
                        <span class="c-white">FOURDIVERS</span><br> admin BUILDING PRO V.1.
                    </div>
                </li>
            </ul>
        </nav>
    </aside>
    <!-- End navigation-->


    <!-- Main content-->
    <section class="content">
        <div class="container-fluid">

			
					<!--TITULAR  SECCION-->
				  	<div class="row">
                <div class="col-lg-12">
                    <div class="view-header">
						
						
							<div class="pull-right text-right" style="line-height: 14px">
                           <a href="#" class="btn btn-w-md btn-info">nueva salida</a>
                        </div>
 
                        <div class="header-icon">
                            <i class="pe page-header-icon pe-7s-clock"></i>
                        </div>
                        <div class="header-title">
                            <h3 class="m-b-xs">Salidas barco</h3>
                            <small>
                                Gestión salidas
                            </small>
										
                        </div>
                    </div>
                    <hr>
                </div>
            </div>
				<!--END. TITULAR  SECCION-->
			
			
 			<div class="row css-paddingB20-M">
			<div class="btn-group cnt-center css-inlineBlock">
				<div class="col-12 col-lg-6">
					<div class="form-group">
						<label>Desde</label>
						<div class="input-group date" id="datetimepicker6" data-target-input="nearest">
						<input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker6"/>
							<div class="input-group-append" data-target="#datetimepicker6" data-toggle="datetimepicker">
							<div class="input-group-text"><i class="fa fa-calendar"></i></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-6">
					<div class="form-group">
						<label>Hasta</label>
						<div class="input-group date" id="datetimepicker7" data-target-input="nearest">
						<input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker7"/>
							<div class="input-group-append" data-target="#datetimepicker7" data-toggle="datetimepicker">
							<div class="input-group-text"><i class="fa fa-calendar"></i></div>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
			
			<div class="row css-paddingB30 css-paddingL10 css-paddingR10">
				<div class="btn-group cnt-center">
				<button class="btn btn-default css-txt-M" type="button">Todas<span class="ocultaM"> las salidas</span></button>
				<button class="btn btn-default css-txt-M" type="button"><span class="ocultaM">Salidas </span>diarias</button>
				<button class="btn btn-default css-txt-M" type="button"><span class="ocultaM">Salidas </span>semanales</button>
				</div>
			</div>
			
		  <div class="row">
		  <?=$contenido1;?>
		  <?=$contenido3;?>
		  <?=$contenido1;?>
		  <?=$contenido2;?>
		  <?=$contenido1;?>
		  <?=$contenido3;?>
		  <?=$contenido2;?>
		  <?=$contenido1;?>
		  <?=$contenido2;?>
		  <?=$contenido3;?>
		  <?=$contenido1;?>
		  <?=$contenido3;?>
		  <?=$contenido1;?>
		  <?=$contenido2;?>
		  <?=$contenido1;?>
		  <?=$contenido3;?>
		  <?=$contenido2;?>
		  <?=$contenido1;?>
		  <?=$contenido2;?>
		  <?=$contenido3;?>
		</div>
			

			


        </div>
    </section>
    <!-- End main content-->

</div>
<!-- End wrapper-->
	
	

	
<!-- Vendor scripts -->
<script src="js/pacejs/pace.min.js"></script>
<script src="js/jquery/dist/jquery.min.js"></script>
<script src="js/jquery-ui/jquery-ui.min.js"></script>
<script src="js/bootstrap/js/bootstrap.min.js"></script>
	
<!--DATA TABLE-->	
<script src="js/datatables/datatables.min.js"></script>
<script src="js/datatables/dataTables.bootstrap4.min.js"></script>
	
<!--SELECT-->
<script src="js/select2/dist/js/select2.js"></script>

<!-- App scripts -->
<script src="js/luna.js"></script>
	
<!-- Datetimepicker-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script type="text/javascript" src="js/tempusdominus-bootstrap-4.js"></script>
	
<script>
    $(document).ready(function () {
		
	
$('#datetimepicker6').datetimepicker();
        $('#datetimepicker7').datetimepicker({
            useCurrent: false
        });
        $("#datetimepicker6").on("change.datetimepicker", function (e) {
            $('#datetimepicker7').datetimepicker('minDate', e.date);
        });
        $("#datetimepicker7").on("change.datetimepicker", function (e) {
            $('#datetimepicker6').datetimepicker('maxDate', e.date);
        });
			
    
		
		 $('#datetimepicker12').datetimepicker({
                inline: true,
                sideBySide: true
            });



        $('#tableExample3').DataTable({
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            buttons: [
                {extend: 'copy', className: 'btn-sm'},
                {extend: 'csv', title: 'ExampleFile', className: 'btn-sm'},
                {extend: 'pdf', title: 'ExampleFile', className: 'btn-sm'},
                {extend: 'print', className: 'btn-sm'}
            ]
        });
		
		 $(".select2_demo_1").select2();
        $(".select2_demo_2").select2({
            placeholder: "Select a state",
            allowClear: true
        });
        $(".select2_demo_3").select2();
		 })
	

</script>

</body>

</html>