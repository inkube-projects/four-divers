<div class="css-navigation">
    <div class="container" id="web-menu">
        <nav class="navbar navbar-light float-right css-subnav">
            <span class="navbar-text css-marginR35">Usuarios | <i class="fas fa-lock"></i></span>
            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Idiomas | ES
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Español</a>
                    <a class="dropdown-item" href="#">Ingles</a>
                </div>
            </div>
        </nav>

        <div class="clearfix"></div>

        <a class="navbar-brand mx-autoauto d-block text-center" href="index.php">
            <img class="css-logo" src="public/images/logos/logo-white.png" alt="Four Divers">
        </a>
        
        <nav class="navbar navbar-expand-lg navbar-light" id="js-main-navigation">
            <div class="container">
                <a class="navbar-brand css-fixed-logo" href="#"><img class="css-logo" src="public/images/logos/logo-water.png" alt="Four Divers"></a>
                <a href="#" class="css-nav-mobile-icon"><i class="far fa-calendar-check"></i></a>
                <a href="#" class="css-nav-mobile-icon"><i class="fas fa-phone-alt"></i></a>
                <a href="#" class="css-nav-mobile-icon"><i class="fas fa-cart-arrow-down"></i></a>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav" aria-controls="main-nav" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-bars"></i>
                </button>
                
                <div class="collapse navbar-collapse" id="main-nav">
                    <div class="navbar-nav mx-auto">
                        <a class="nav-item nav-link" href="courses.php">Cursos</a>
                        <a class="nav-item nav-link" href="#">Actividades</a>
                        <a class="nav-item nav-link" href="destinations.php">Reservas</a>
                        <a class="nav-item nav-link" href="#">Servicios</a>
                        <a class="nav-item nav-link" href="team.php">El centro</a>
                        <a class="nav-item nav-link" href="team.php">Contacto</a>
                        <a class="nav-item nav-link" href="#">Blog</a>
                    </div>
                </div>
            </div>
        </nav>
    </div>

    <!-- MOBILE -->
    <div class="container-fluid" id="mobile-menu">
        <img src="public/images/logos/logo-white.png" alt="Four Divers">

        <div class="css-submenu-responsive">
            <a class="navbar-brand css-fixed-logo" href="#"><img class="css-logo" src="public/images/logos/logo-water.png" alt="Four Divers"></a>
            <a href="#" class="css-nav-mobile-icon"><i class="far fa-calendar-check"></i></a>
            <a href="#" class="css-nav-mobile-icon"><i class="fas fa-phone-alt"></i></a>
            <a href="#" class="css-nav-mobile-icon"><i class="fas fa-cart-arrow-down"></i></a>
            <a href="#sidebar" data-toggle="collapse"><i class="fas fa-bars"></i></a>
        </div>

        <div class="row">
            <div class="col-md-3 col-xs-1 p-l-0 p-r-0 collapse in" id="sidebar">
                <div class="list-group panel">
                    <div class="css-sidebar-option">
                        <a href="#" class="css-nav-mobile-icon"><i class="far fa-calendar-check"></i></a>
                        <a href="#" class="css-nav-mobile-icon"><i class="fas fa-phone-alt"></i></a>
                        <a href="#" class="css-nav-mobile-icon"><i class="fas fa-cart-arrow-down"></i></a>
                        <a href="#sidebar" data-toggle="collapse"><i class="fas fa-times"></i></a>
                    </div>
                    
                    <a href="#" class="list-group-item collapsed" data-parent="#sidebar"><span class="hidden-sm-down">USUARIOS | <i class="fas fa-lock"></i></span></a>
                    <a href="courses.php" class="list-group-item collapsed" data-parent="#sidebar"><span class="hidden-sm-down">CURSOS</span></a>
                    <a href="#" class="list-group-item collapsed" data-parent="#sidebar"><span class="hidden-sm-down">ACTIVIDADES</span></a>
                    <a href="destinations.php" class="list-group-item collapsed" data-parent="#sidebar"><span class="hidden-sm-down">RESERVAS</span></a>
                    <a href="#" class="list-group-item collapsed" data-parent="#sidebar"><span class="hidden-sm-down">SERVICIOS</span></a>
                    <a href="team.php" class="list-group-item collapsed" data-parent="#sidebar"><span class="hidden-sm-down">EL CENTRO</span></a>
                    <a href="team.php" class="list-group-item collapsed" data-parent="#sidebar"><span class="hidden-sm-down">CONTACTO</span></a>
                    <a href="#" class="list-group-item collapsed" data-parent="#sidebar"><span class="hidden-sm-down">BLOG</span></a>
                </div>
            </div>
        </div>
    </div>
</div>