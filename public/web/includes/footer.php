<footer>
    <div class="css-subfooter">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h2>Four Divers</h2>
                    <p>
                        Av. de la Fontana, 5. 03730 Jávea, Alicante <br>
                        685 072 762 <br>
                        hola@fourdivers.com
                    </p>
                </div>

                <div class="col-md-4">
                    <h2>Buceo</h2>
                    <p>
                        Bautizos / Discover Scuba Diving / Imersiones
                    </p>
                </div>

                <div class="col-md-4">
                    <h2>Actividades</h2>
                    <p>
                        Snorkel <br>
                        Excursiones en Kayaks en Jávea <br>
                        Paddle SUP en Jávea <br>
                        Alquiler de bicicletas en Jávea <br>
                        Hoverboard <br>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="css-footer">
        <div class="container">
            <img src="public/images/logos/logo-footer.png" class="d-block mx-auto" alt="" width="50">

            <div class="css-footer-links">
                <a href="#">Política de privacidad |</a>
                <a href="#">Aviso legal |</a>
                <a href="#">Política de cookies |</a>
                <a href="#">Copyright 2020</a>
            </div>

            <div class="css-social text-center">
                <a href="#">
                    <div>
                        <i class="fab fa-facebook-f"></i>
                    </div>
                </a>

                <a href="#">
                    <div>
                        <i class="fab fa-instagram"></i>
                    </div>
                </a>

                <a href="#">
                    <div>
                        <i class="fab fa-twitter"></i>
                    </div>
                </a>
                
                <a href="#">
                    <div>
                        <i class="fab fa-youtube"></i>
                    </div>
                </a>
            </div>
        </div>
    </div>
</footer>