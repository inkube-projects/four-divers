<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Four Divers | Cursos</title>
        <!-- CSS -->
        <!-- Bootstrap -->
        <link rel="stylesheet" href="public/assets/node_modules/bootstrap/dist/css/bootstrap.min.css">
        <!-- Fontawesome -->
        <link rel="stylesheet" href="public/assets/node_modules/@fortawesome/fontawesome-free/css/all.min.css">
        <!-- wow animate -->
        <link rel="stylesheet" href="public/assets/node_modules/wow.js/css/libs/animate.css">
        <!-- select2 -->
        <link rel="stylesheet" href="public/assets/node_modules/select2/dist/css/select2.min.css">
        <!-- Custom -->
        <link rel="stylesheet" href="public/assets/web/css/css-vars.css">
        <link rel="stylesheet" href="public/assets/web/css/css-styles.css">
        <link rel="stylesheet" href="public/assets/web/css/css-responsive.css">
    </head>
    
    <body>
        <header class="css-header css-courses-header">
            <?php include('includes/navigation.php'); ?>
            
            <div id="index-slider" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#index-slider" data-slide-to="0" class="active"></li>
                </ol>

                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="carousel-caption d-md-block css-courses-caption">
                            <h1>
                                Descubre tu lado <br> más profundo
                            </h1>
                            <p>
                                Cursos adaptados <br> a tus inquietudes
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section id="search-secion">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-12 d-block mx-auto">
                        <div class="row wow fadeInUp">
                            <div class="col-md-4 css-noPadding">
                                <select class="form-control select2" name="sel-search" id="sel-search">
                                    <option value="">¿Qué buscas?</option>
                                    <option value="1">Option 1</option>
                                    <option value="2">Option 2</option>
                                    <option value="3">Option 3</option>
                                    <option value="4">Option 4</option>
                                    <option value="5">Option 5</option>
                                    <option value="6">Option 6</option>
                                </select>
                            </div>
        
                            <div class="col-md-4 css-noPadding">
                                <select class="form-control select2" name="sel-date" id="sel-date">
                                    <option value="">¿En que fechas?</option>
                                    <option value="1">Option 1</option>
                                    <option value="2">Option 2</option>
                                    <option value="3">Option 3</option>
                                    <option value="4">Option 4</option>
                                    <option value="5">Option 5</option>
                                    <option value="6">Option 6</option>
                                </select>
                            </div>
        
                            <div class="col-md-4 css-noPadding">
                                <button type="button" class="btn btn-success btn-flat">Buscar</button>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="text-center css-marginT100">
                    <h2>Tenemos el curso de buceo <br> que estas esperando</h2>
                    <p class="css-marginT15">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Alias aliquam, quisquam repudiandae hic illo unde tempora dolorem, dicta officia fuga possimus est facilis accusamus repellendus consequuntur ipsam, necessitatibus vel voluptatum.</p>
                </div>
                
                <div class="row css-marginT80 css-courses-panels-pc">
                    <div class="col-lg-4 col-md-6 wow fadeInDown css-marginB20" data-wow-delay="300ms">
                        <div class="css-panel-courses">
                            <div class="css-tag">Principiante</div>
                            <img src="public/images/courses/c-1.jpg" alt="Nombre del curso">
                            <div class="css-panel-text">
                                <h4>Open Water Diver (OWD)</h4>
                                <p>Este curso representa el primer paso en el programa de enseñanza de buceo.</p>
                                <a href="#">Ver más <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>

                        <div class="css-panel-courses">
                            <div class="css-tag">Avanzado</div>
                            <img src="public/images/courses/c-5.jpg" alt="Nombre del curso">
                            <div class="css-panel-text css-advanced">
                                <h4>Rescue Diver</h4>
                                <p>Saca tu lado más valiente!  Un curso exigente y gratificante. Este curso busca ampliar tus conocimientos.</p>
                                <a href="#">Ver más <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 wow fadeInDown css-marginB20" data-wow-delay="450ms">
                        <div class="css-panel-courses">
                            <div class="css-tag">Avanzado</div>
                            <img src="public/images/courses/c-2.jpg" alt="Nombre del curso">
                            <div class="css-panel-text">
                                <h4>Open Water Diver (AOWD)</h4>
                                <p>Te permitirá de inmediato desarrollar y mejorar los conocimientos adquiridos en el curso anterior, así como aprender nuevas técnicas que ampliarán tu experiencia para bucear en condiciones diversas..</p>
                                <a href="#">Ver más <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>

                        <div class="css-panel-courses">
                            <div class="css-tag">Avanzado</div>
                            <img src="public/images/courses/c-3.jpg" alt="Nombre del curso">
                            <div class="css-panel-text css-advanced">
                                <h4>Divemaster</h4>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
                                <a href="#">Ver más <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 wow fadeInDown css-marginB20" data-wow-delay="550ms">
                        <div class="css-panel-courses">
                            <div class="css-tag">Avanzado</div>
                            <img src="public/images/courses/c-4.jpg" alt="Nombre del curso">
                            <div class="css-panel-text css-advanced">
                                <h4>Emergency FirstResponse (EFR)</h4>
                                <p>El curso Emergency First Response, está especializado en la enseñanza de las técnicas de primeros auxilios..</p>
                                <a href="#">Ver más <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>

                        <div class="css-panel-courses">
                            <div class="css-tag">Avanzado</div>
                            <img src="public/images/courses/c-6.jpg" alt="Nombre del curso">
                            <div class="css-panel-text css-professional">
                                <h4>Instructor de Buceo</h4>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
                                <a href="#">Ver más <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- MOBILE -->
            <div id="carousel-courses" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <!-- Curso 1 -->
                    <div class="carousel-item active">
                        <div class="css-panel-courses">
                            <img src="public/images/courses/c-1.jpg" alt="Nombre del curso">
                            <div class="css-panel-text">
                                <div class="css-tag">Principiante</div>

                                <h4>Open Water Diver (OWD)</h4>
                                <p>Este curso representa el primer paso en el programa de enseñanza de buceo.</p>
                                <a href="#">Ver más <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <!-- Curso 2 -->
                    <div class="carousel-item">
                        <div class="css-panel-courses">
                            <img src="public/images/courses/c-2.jpg" alt="Nombre del curso">
                            <div class="css-panel-text">
                                <div class="css-tag">Avanzado</div>

                                <h4>Open Water Diver (AOWD)</h4>
                                <p>Te permitirá de inmediato desarrollar y mejorar los conocimientos adquiridos en el curso anterior, así como aprender nuevas técnicas que ampliarán tu experiencia para bucear en condiciones diversas..</p>
                                <a href="#">Ver más <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <!-- Curso 3 -->
                    <div class="carousel-item">
                        <div class="css-panel-courses">
                            <img src="public/images/courses/c-4.jpg" alt="Nombre del curso">
                            <div class="css-panel-text css-advanced">
                                <div class="css-tag">Avanzado</div>

                                <h4>Emergency FirstResponse (EFR)</h4>
                                <p>El curso Emergency First Response, está especializado en la enseñanza de las técnicas de primeros auxilios..</p>
                                <a href="#">Ver más <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <!-- Curso 4 -->
                    <div class="carousel-item">
                        <div class="css-panel-courses">
                            <img src="public/images/courses/c-5.jpg" alt="Nombre del curso">
                            <div class="css-panel-text css-advanced">
                                <div class="css-tag">Avanzado</div>

                                <h4>Rescue Diver</h4>
                                <p>Saca tu lado más valiente!  Un curso exigente y gratificante. Este curso busca ampliar tus conocimientos.</p>
                                <a href="#">Ver más <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <!-- Curso 5 -->
                    <div class="carousel-item">
                        <div class="css-panel-courses">
                            <img src="public/images/courses/c-3.jpg" alt="Nombre del curso">
                            <div class="css-panel-text css-advanced">
                                <div class="css-tag">Avanzado</div>

                                <h4>Divemaster</h4>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
                                <a href="#">Ver más <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <!-- Curso 6 -->
                    <div class="carousel-item">
                        <div class="css-panel-courses">
                            <img src="public/images/courses/c-6.jpg" alt="Nombre del curso">
                            <div class="css-panel-text css-professional">
                                <div class="css-tag">Avanzado</div>
                                
                                <h4>Instructor de Buceo</h4>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
                                <a href="#">Ver más <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <a class="carousel-control-prev" href="#carousel-courses" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true">
                        <i class="fas fa-chevron-circle-left"></i>
                    </span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel-courses" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true">
                        <i class="fas fa-chevron-circle-right"></i>
                    </span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </section>

        <!-- Parallax 3 -->
        <section class="css-parallax-sea-3"></section>

        <?php include("includes/footer.php") ?>
    </body>
    
    <!-- Javascript -->
    <!-- jquery -->
    <script src="public/assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="public/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- WOW animate -->
    <script src="public/assets/node_modules/wow.js/dist/wow.js"></script>
    <!-- Select2 -->
    <script src="public/assets/node_modules/select2/dist/js/select2.min.js"></script>
    <!-- Custom -->
    <script src="public/assets/web/js/main.js"></script>
</html>