<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Four Divers | Destinos</title>
        <!-- CSS -->
        <!-- Bootstrap -->
        <link rel="stylesheet" href="public/assets/node_modules/bootstrap/dist/css/bootstrap.min.css">
        <!-- Fontawesome -->
        <link rel="stylesheet" href="public/assets/node_modules/@fortawesome/fontawesome-free/css/all.min.css">
        <!-- wow animate -->
        <link rel="stylesheet" href="public/assets/node_modules/wow.js/css/libs/animate.css">
        <!-- select2 -->
        <link rel="stylesheet" href="public/assets/node_modules/select2/dist/css/select2.min.css">
        <!-- Custom -->
        <link rel="stylesheet" href="public/assets/web/css/css-vars.css">
        <link rel="stylesheet" href="public/assets/web/css/css-styles.css">
        <link rel="stylesheet" href="public/assets/web/css/css-responsive.css">
    </head>
    
    <body>
        <header class="css-header css-destinations-header">
            <?php include('includes/navigation.php'); ?>
            
            <div id="index-slider" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#index-slider" data-slide-to="0" class="active"></li>
                </ol>

                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="carousel-caption d-md-block">
                            <p>Del 10 al 20 de abril de 2020</p>
                            <h1>
                                Descubre todo <br> el misterio del <br> Mar Rojo
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section id="search-secion">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-12 d-block mx-auto">
                        <div class="row wow fadeInUp">
                            <div class="col-md-4 css-noPadding">
                                <select class="form-control select2" name="sel-search" id="sel-search">
                                    <option value="">¿Qué buscas?</option>
                                    <option value="1">Option 1</option>
                                    <option value="2">Option 2</option>
                                    <option value="3">Option 3</option>
                                    <option value="4">Option 4</option>
                                    <option value="5">Option 5</option>
                                    <option value="6">Option 6</option>
                                </select>
                            </div>
        
                            <div class="col-md-4 css-noPadding">
                                <select class="form-control select2" name="sel-date" id="sel-date">
                                    <option value="">¿En que fechas?</option>
                                    <option value="1">Option 1</option>
                                    <option value="2">Option 2</option>
                                    <option value="3">Option 3</option>
                                    <option value="4">Option 4</option>
                                    <option value="5">Option 5</option>
                                    <option value="6">Option 6</option>
                                </select>
                            </div>
        
                            <div class="col-md-4 css-noPadding">
                                <button type="button" class="btn btn-success btn-flat">Buscar</button>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="css-marginT100 css-description">
                    <h2>Destino: aventura y ocio en la profundidades del Mar Rojo</h2>
                    <h3>Abril 15</h3>

                    <div class="row">
                        <div class="col-lg-6">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc porta fringilla ullamcorper. Morbi felis orci, lacinia a velit et, sodales condimentum metus. Nulla non fermentum nisl. Maecenas id molestie turpis, sit amet feugiat lorem. Curabitur sed erat vel tellus hendrerit tincidunt. Sed arcu tortor, sollicitudin ac lectus sed, rhoncus iaculis lectus. Ut efficitur feugiat enim a euismod. Mauris suscipit vehicula imperdiet.</p>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut tristique pretium tellus, sed fermentum est vestibulum id. Aenean semper, odio sed fringillestas. Fusce gravida, ligula non molestie tristique, justo elit blandit risus, blandit maximus augue magna accumsan ante. Aliquam bibendum lacus quis nulla dignissim faucibus. Sed mauris enim, bibendum at purus aliquet, maximus molestie tortor. Sed faucibus et tellus eu sollicitudin. Sed fringilla malesuada luctus.</p>
                        </div>

                        <div class="col-lg-6">
                            <img src="public/images/web/destination-1.jpg" class="img-fluid" alt="Destino: aventura y ocio en la profundidades del Mar Rojo">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="destination-info-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h2>Detalles del viaje</h2>
                        <p>
                            <b>Fechas:</b> <br>
                            Abril 18 al 31
                        </p>

                        <p>
                            <b>Hora de salida:</b> <br> 
                            9:00 am - 3:00 pm
                        </p>

                        <p>
                            <b>Lugar de salida:</b> <br> 
                            Centro 4Divers
                        </p>
                    </div>

                    <div class="col-md-4">
                        <h2>Organizador</h2>
                        <p>
                            <b>Nombre y apellidos</b>
                        </p>

                        <p>
                            <b>Teléfono:</b> <br> 
                            600 000 000
                        </p>

                        <p>
                            <b>Email:</b> <br> 
                            info@yoursitename.com
                        </p>
                    </div>

                    <div class="col-md-4">
                        <h2>Precios</h2>
                        <p class="css-fontSize25">
                            <b>Desde 0,0€</b>
                        </p>

                        <p>Informate de todas las modalidades y opciones del viaje.</p>

                        <p><a href="#" class="btn btn-link">Más info <i class="fas fa-chevron-right"></i></a></p>
                    </div>
                </div>
            </div>
            </div>
        </section>

        <section class="css-gallery-section">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        <img src="public/images/web/destination-2.jpg" alt="">
                        <img src="public/images/web/destination-7.jpg" alt="">
                    </div>

                    <div class="col-lg-4">
                        <div class="css-img-100">
                            <img src="public/images/web/destination-4.jpg" alt="">
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <img src="public/images/web/destination-6.jpg" alt="">
                        <img src="public/images/web/destination-5.jpg" alt="">
                    </div>
                </div>
            </div>
        </section>

        <?php include("includes/footer.php") ?>
    </body>
    
    <!-- Javascript -->
    <!-- jquery -->
    <script src="public/assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="public/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- WOW animate -->
    <script src="public/assets/node_modules/wow.js/dist/wow.js"></script>
    <!-- Select2 -->
    <script src="public/assets/node_modules/select2/dist/js/select2.min.js"></script>
    <!-- Custom -->
    <script src="public/assets/web/js/main.js"></script>
</html>