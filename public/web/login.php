<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Four Divers | Inicio</title>
        <!-- CSS -->
        <!-- Bootstrap -->
        <link rel="stylesheet" href="public/assets/node_modules/bootstrap/dist/css/bootstrap.min.css">
        <!-- Fontawesome -->
        <link rel="stylesheet" href="public/assets/node_modules/@fortawesome/fontawesome-free/css/all.min.css">
        <!-- wow animate -->
        <link rel="stylesheet" href="public/assets/node_modules/wow.js/css/libs/animate.css">
        <!-- select2 -->
        <link rel="stylesheet" href="public/assets/node_modules/select2/dist/css/select2.min.css">
        <!-- Custom -->
        <link rel="stylesheet" href="public/assets/web/css/css-vars.css">
        <link rel="stylesheet" href="public/assets/web/css/css-styles.css">
        <link rel="stylesheet" href="public/assets/web/css/css-responsive.css">
    </head>
    
    <body>
        <header class="css-header css-index-header">
            <?php include('includes/navigation.php'); ?>
            
            <div id="index-slider" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#index-slider" data-slide-to="0" class="active"></li>
                </ol>

                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="carousel-caption d-md-block">
                            <div class="css-login">
                                <form>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email / Usuario</label>
                                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Contraseña</label>
                                        <input type="password" class="form-control" id="exampleInputPassword1">
                                    </div>
                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                        <label class="form-check-label" for="exampleCheck1">Recordarme</label>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <?php include("includes/footer.php") ?>
    </body>
    
    <!-- Javascript -->
    <!-- jquery -->
    <script src="public/assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="public/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- WOW animate -->
    <script src="public/assets/node_modules/wow.js/dist/wow.js"></script>
    <!-- Select2 -->
    <script src="public/assets/node_modules/select2/dist/js/select2.min.js"></script>
    <!-- Custom -->
    <script src="public/assets/web/js/main.js"></script>
</html>