<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Four Divers | Equipo</title>
        <!-- CSS -->
        <!-- Bootstrap -->
        <link rel="stylesheet" href="public/assets/node_modules/bootstrap/dist/css/bootstrap.min.css">
        <!-- Fontawesome -->
        <link rel="stylesheet" href="public/assets/node_modules/@fortawesome/fontawesome-free/css/all.min.css">
        <!-- wow animate -->
        <link rel="stylesheet" href="public/assets/node_modules/wow.js/css/libs/animate.css">
        <!-- select2 -->
        <link rel="stylesheet" href="public/assets/node_modules/select2/dist/css/select2.min.css">
        <!-- Custom -->
        <link rel="stylesheet" href="public/assets/web/css/css-vars.css">
        <link rel="stylesheet" href="public/assets/web/css/css-styles.css">
        <link rel="stylesheet" href="public/assets/web/css/css-responsive.css">
    </head>
    
    <body class="css-header css-team-header">
        <header>
            <?php include('includes/navigation.php'); ?>
            
            <div id="index-slider" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#index-slider" data-slide-to="0" class="active"></li>
                    <li data-target="#index-slider" data-slide-to="1"></li>
                </ol>

                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="carousel-caption d-md-block">
                            <h1>
                                Sumérgete <br>
                                con nosotros <br>
                                en un mundo <br>
                                de aventuras
                            </h1>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="carousel-caption d-md-block">
                            <h1>
                                Segundo Slide
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section id="team-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <h2 class="wow fadeInDown css-marginB20" data-wow-delay="300ms">
                            Four Divers. <br>
                            Tu centro de buceo<br>
                            en Jávea.
                        </h2>

                        <p class="wow fadeInUp css-marginB20" data-wow-delay="350ms">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                        </p>

                        <p class="wow fadeInDown css-marginB20" data-wow-delay="400ms">
                            Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit.
                        </p>

                        <h4 class="wow fadeInUp css-marginB20" data-wow-delay="450ms">
                            Queremos que te sientas especial y que disfrutes tanto como nosotros de las posibilidades que nos ofrece el mar.
                        </h4>
                    </div>
                </div>

                <h2 class="css-marginT70">Nuestro equipo</h2>
                <div class="row css-marginT40">
                    <div class="col-lg-6">
                        <div class="row css-pc-view">
                            <div class="col-lg-6 col-md-6 wow fadeInLeft css-marginB20" data-wow-delay="300ms">
                                <a href="team-detail.php">
                                    <div class="css-team-panel">
                                        <img src="public/images/team/team-1.jpg" alt="Nombre apellido">
                                        <div class="css-tag">Nombre Apellido</div>
                                    </div>
                                </a>
                            </div>
    
                            <div class="col-lg-6 col-md-6 wow fadeInRight css-marginB20" data-wow-delay="300ms">
                                <a href="team-detail.php">
                                    <div class="css-team-panel">
                                        <img src="public/images/team/team-2.jpg" alt="Nombre apellido">
                                        <div class="css-tag">Nombre Apellido</div>
                                    </div>
                                </a>
                            </div>
    
                            <div class="col-lg-6 col-md-6 wow fadeInLeft css-marginB20" data-wow-delay="450ms">
                                <a href="team-detail.php">
                                    <div class="css-team-panel">
                                        <img src="public/images/team/team-3.jpg" alt="Nombre apellido">
                                        <div class="css-tag">Nombre Apellido</div>
                                    </div>
                                </a>
                            </div>
    
                            <div class="col-lg-6 col-md-6 wow fadeInRight css-marginB20" data-wow-delay="450ms">
                                <a href="team-detail">
                                    <div class="css-team-panel">
                                        <img src="public/images/team/team-4.jpg" alt="Nombre apellido">
                                        <div class="css-tag">Nombre Apellido</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 wow fadeInDown css-marginB20" data-wow-delay="450ms">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat</p>
                        <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit.</p>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam</p>
                        <h4>Si lo tuyo es la pasión por el mundo subacuático, ven a disfrutar con nosotros!!</h4>
                        <a href="#" class="btn btn-link">Contacta</a>
                    </div>
                </div>

                <!-- MOBILE -->
                <div id="carousel-team" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="css-team-panel">
                                <img src="public/images/team/team-1.jpg" alt="Nombre apellido">
                                <div class="css-tag">Nombre Apellido</div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="css-team-panel">
                                <img src="public/images/team/team-2.jpg" alt="Nombre apellido">
                                <div class="css-tag">Nombre Apellido</div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="css-team-panel">
                                <img src="public/images/team/team-3.jpg" alt="Nombre apellido">
                                <div class="css-tag">Nombre Apellido</div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="css-team-panel">
                                <img src="public/images/team/team-4.jpg" alt="Nombre apellido">
                                <div class="css-tag">Nombre Apellido</div>
                            </div>
                        </div>
                    </div>

                    <a class="carousel-control-prev" href="#carousel-team" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true">
                            <i class="fas fa-chevron-circle-left"></i>
                        </span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carousel-team" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true">
                            <i class="fas fa-chevron-circle-right"></i>
                        </span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>

                <h2 class="css-marginT50 wow fadeInUp css-marginB20" data-wow-delay="300ms">
                    Four Divers. <br> Un lugar diferente y especial, <br> un lugar para compartir, para <br> vivir, para soñar...
                </h2>
            </div>
        </section>

        <!-- Instalaciones -->
        <section id="installation-section">
            <div class="container-fluid">
                <div class="row css-pc-view">
                    <div class="col-sm-4 col-lg-4 col-md-6">
                        <div class="folio-item wow fadeInRightBig" data-wow-duration="1000ms" data-wow-delay="300ms">
                            <div class="folio-image">
                                <img class="img-responsive" src="public/images/web/installation-1.jpg" alt="">
                            </div>
                            <div class="overlay">
                                <div class="overlay-content">
                                    <div class="overlay-text">
                                        <div class="folio-info">
                                            <h3>Four divers</h3>
                                        </div>
                                        <div class="folio-overview">
                                            <span class="folio-expand"><a href="#" data-img="public/images/web/installation-1.jpg"><i class="fa fa-search-plus"></i></a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-6 css-installation-panel">
                        <div class="wow fadeInLeftBig" data-wow-duration="1000ms" data-wow-delay="350ms">
                            <h3>Cómodos vestuarios exteriores e interiores</h3>

                            <p>En 4Divers podrás disfrutar de unos vestuarios exteriores donde maniobrar sin problemas para manipular los equipos. Así como unos vestuarios interiores donde poder tomar una merecida ducha tras haber disfrutado con nosotros de una magnífica experiencia</p>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-6">
                        <div class="folio-item wow fadeInRightBig" data-wow-duration="1000ms" data-wow-delay="400ms">
                            <div class="folio-image">
                                <img class="img-responsive" src="public/images/web/installation-2.jpg" alt="">
                            </div>
                            <div class="overlay">
                                <div class="overlay-content">
                                    <div class="overlay-text">
                                        <div class="folio-info">
                                            <h3>Four divers</h3>
                                        </div>
                                        <div class="folio-overview">
                                            <span class="folio-expand"><a href="#" data-img="public/images/web/installation-2.jpg"><i class="fa fa-search-plus"></i></a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-6 css-installation-panel">
                        <div class="wow fadeInLeftBig" data-wow-duration="1000ms" data-wow-delay="450ms">
                            <h3>¿Mucho frío o calor? ¡No hay problema!</h3>

                            <p>En 4Divers  contamos con el suficiente espacio interior en nuestras instalaciones para que puedas disfrutar dentro en un entorno más privado y acondicionado.</p>
                            <p>¡Tú eliges!</p>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-6">
                        <div class="folio-item wow fadeInRightBig" data-wow-duration="1000ms" data-wow-delay="500ms">
                            <div class="folio-image">
                                <img class="img-responsive" src="public/images/web/installation-3.jpg" alt="">
                            </div>
                            <div class="overlay">
                                <div class="overlay-content">
                                    <div class="overlay-text">
                                        <div class="folio-info">
                                            <h3>Four divers</h3>
                                        </div>
                                        <div class="folio-overview">
                                            <span class="folio-expand"><a href="#" data-img="public/images/web/installation-3.jpg"><i class="fa fa-search-plus"></i></a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-6">
                        <div class="folio-item wow fadeInLeftBig" data-wow-duration="1000ms" data-wow-delay="550ms">
                            <div class="folio-image">
                                <img class="img-responsive" src="public/images/web/installation-4.jpg" alt="">
                            </div>
                            <div class="overlay">
                                <div class="overlay-content">
                                    <div class="overlay-text">
                                        <div class="folio-info">
                                            <h3>Four divers</h3>
                                        </div>
                                        <div class="folio-overview">
                                            <span class="folio-expand"><a href="#" data-img="public/images/web/installation-4.jpg"><i class="fa fa-search-plus"></i></a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-6">
                        <div class="folio-item wow fadeInRightBig" data-wow-duration="1000ms" data-wow-delay="600ms">
                            <div class="folio-image">
                                <img class="img-responsive" src="public/images/web/installation-5.jpg" alt="">
                            </div>
                            <div class="overlay">
                                <div class="overlay-content">
                                    <div class="overlay-text">
                                        <div class="folio-info">
                                            <h3>Four divers</h3>
                                        </div>
                                        <div class="folio-overview">
                                            <span class="folio-expand"><a href="#" data-img="public/images/web/installation-5.jpg"><i class="fa fa-search-plus"></i></a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-6">
                        <div class="folio-item wow fadeInLeftBig" data-wow-duration="1000ms" data-wow-delay="650ms">
                            <div class="folio-image">
                                <img class="img-responsive" src="public/images/web/installation-6.jpg" alt="">
                            </div>
                            <div class="overlay">
                                <div class="overlay-content">
                                    <div class="overlay-text">
                                        <div class="folio-info">
                                            <h3>Four divers</h3>
                                        </div>
                                        <div class="folio-overview">
                                            <span class="folio-expand"><a href="#" data-img="public/images/web/installation-6.jpg"><i class="fa fa-search-plus"></i></a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-6 css-installation-panel">
                        <div class="wow fadeInRightBig" data-wow-duration="1000ms" data-wow-delay="700ms">
                            <h3>Cómodos vestuarios exteriores e interiores</h3>

                            <p>En 4Divers podrás disfrutar de unos vestuarios exteriores donde maniobrar sin problemas para manipular los equipos. Así como unos vestuarios interiores donde poder tomar una merecida ducha tras haber disfrutado con nosotros de una magnífica experiencia</p>
                        </div>
                    </div>
                </div>
            </div>

            <!-- MOBILE -->
            <div id="carousel-installation" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="img-responsive" src="public/images/web/installation-1.jpg" alt="">
                    </div>

                    <div class="carousel-item">
                        <img class="img-responsive" src="public/images/web/installation-1.jpg" alt="">
                    </div>

                    <div class="carousel-item">
                        <img class="img-responsive" src="public/images/web/installation-1.jpg" alt="">
                    </div>

                    <div class="carousel-item">
                        <img class="img-responsive" src="public/images/web/installation-1.jpg" alt="">
                    </div>
                </div>

                <a class="carousel-control-prev" href="#carousel-installation" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true">
                        <i class="fas fa-chevron-circle-left"></i>
                    </span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel-installation" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true">
                        <i class="fas fa-chevron-circle-right"></i>
                    </span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </section>

        <?php include("includes/footer.php") ?>

        <!-- Modals -->
        <div class="modal" tabindex="-1" role="dialog" id="mod-image">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body text-center"></div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <!-- jquery -->
    <script src="public/assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="public/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- WOW animate -->
    <script src="public/assets/node_modules/wow.js/dist/wow.js"></script>
    <!-- Select2 -->
    <script src="public/assets/node_modules/select2/dist/js/select2.min.js"></script>
    <!-- Custom -->
    <script src="public/assets/web/js/main.js"></script>
</html>