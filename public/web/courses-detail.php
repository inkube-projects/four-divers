<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Four Divers | Inicio</title>
        <!-- CSS -->
        <!-- Bootstrap -->
        <link rel="stylesheet" href="public/assets/node_modules/bootstrap/dist/css/bootstrap.min.css">
        <!-- Fontawesome -->
        <link rel="stylesheet" href="public/assets/node_modules/@fortawesome/fontawesome-free/css/all.min.css">
        <!-- wow animate -->
        <link rel="stylesheet" href="public/assets/node_modules/wow.js/css/libs/animate.css">
        <!-- select2 -->
        <link rel="stylesheet" href="public/assets/node_modules/select2/dist/css/select2.min.css">
        <!-- Custom -->
        <link rel="stylesheet" href="public/assets/web/css/css-vars.css">
        <link rel="stylesheet" href="public/assets/web/css/css-styles.css">
        <link rel="stylesheet" href="public/assets/web/css/css-responsive.css">
    </head>
    
    <body>
        <header class="css-header css-courses-detail-header" style="background-image: linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url('public/images/sliders/courses-detail.jpg');">
            <?php include('includes/navigation.php'); ?>
            
            <div id="index-slider" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="carousel-caption d-md-block css-course-detail-captation">
                            <h2>Curso avanzado</h2>
                            
                            <h1>
                                EmergencyFirst Response (EFR)
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section id="search-secion">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-12 d-block mx-auto">
                        <div class="row wow fadeInUp">
                            <div class="col-md-4 css-noPadding">
                                <select class="form-control select2" name="sel-search" id="sel-search">
                                    <option value="">¿Qué buscas?</option>
                                    <option value="1">Option 1</option>
                                    <option value="2">Option 2</option>
                                    <option value="3">Option 3</option>
                                    <option value="4">Option 4</option>
                                    <option value="5">Option 5</option>
                                    <option value="6">Option 6</option>
                                </select>
                            </div>
        
                            <div class="col-md-4 css-noPadding">
                                <select class="form-control select2" name="sel-date" id="sel-date">
                                    <option value="">¿En que fechas?</option>
                                    <option value="1">Option 1</option>
                                    <option value="2">Option 2</option>
                                    <option value="3">Option 3</option>
                                    <option value="4">Option 4</option>
                                    <option value="5">Option 5</option>
                                    <option value="6">Option 6</option>
                                </select>
                            </div>
        
                            <div class="col-md-4 css-noPadding">
                                <button type="button" class="btn btn-success btn-flat">Buscar</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="css-course-detail-panel">
                    <div class="css-tag">
                        <h2>Emergency First Response (EFR)</h2>
                    </div>

                    <div class="row">
                        <div class="col-lg-9">
                            <div class="css-text">
                                <p class="css-title">El curso Emergency First Response, está especializado en la enseñanza de las técnicas de RCP y primeros auxilios.</p>

                                <p>Durante el curso aprenderás cuidados básicos de primeros auxilios muy útiles en situaciones cotidianas. También te enseñaremos cómo realizar una reanimación cardio pulmonar (RCP). Y como administrar oxigeno medicinal, para los casos de accidentes descompresivos.</p>
                            </div>
                        </div>

                        <div class="col-lg-3 css-image" style="background-image: url('public/images/team/team-1-detail.jpg')">
                            <!-- <img src="public/images/team/team-1-detail.jpg" class="img-fluid" alt=""> -->
                        </div>
                    </div>
                </div>

                <div class="css-course-description">
                    <h2>Es un curso necesario para poder participar <br> en el curso de buceador de rescate.</h2>

                    <div class="row css-marginT80">
                        <div class="col-lg-3">
                            <h3>Duración:</h3>

                            <p>Un día</p>
                        </div>

                        <div class="col-lg-3">
                            <h3>Objetivos:</h3>

                            <p>Adquirir los conocimientos básicos de primeros auxilios.</p>
                            <p>Manejar de situaciones de accidentes.</p>
                            <p>Suministro de oxígeno.</p>
                            <p>Aprender como desenvolvernos en este tipo de situaciones con los medios que dispongamos en ese momento. No está demás estar bien preparado para cualquier incidencia.</p>
                        </div>

                        <div class="col-lg-3">
                            <h3>¿Qué aprenderás?</h3>

                            <p>SVB (Soporte Vital Básico) RCP y respiraciones de rescate a nivel no profesional.</p>
                            <p>Uso del DEA (desfibrilador externo automático) (opcional).</p>
                            <p>Prevención y tratamiento de un shock.</p>
                            <p>Manejo de lesiones de la columna vertebral.</p>
                            <p>Uso de barreras para reducir el riesgo de transmisión de enfermedades.</p>
                            <p>Consideraciones sobre primeros auxilios básicos y el equipo de primeros auxilios.</p>
                        </div>

                        <div class="col-lg-3">
                            <h3>Precio:</h3>

                            <h3 class="css-price">110 €</h3>
                            <p class="css-font-museo">Disponibilidad >> </p>
                            <a href="#" class="btn btn-link">Reservar ahora</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Parallax 4 -->
        <section class="css-parallax-sea-4"></section>

        <?php include("includes/footer.php") ?>
    </body>
    
    <!-- Javascript -->
    <!-- jquery -->
    <script src="public/assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="public/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- WOW animate -->
    <script src="public/assets/node_modules/wow.js/dist/wow.js"></script>
    <!-- Select2 -->
    <script src="public/assets/node_modules/select2/dist/js/select2.min.js"></script>
    <!-- Custom -->
    <script src="public/assets/web/js/main.js"></script>
</html>