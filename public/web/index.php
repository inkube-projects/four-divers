<?php
    require_once '../../sys/core/init.inc.php';
    $arr_courses = WebProducts::getCourses($dbo);


?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Four Divers | Inicio</title>
        <!-- CSS -->
        <!-- Bootstrap -->
        <link rel="stylesheet" href="public/assets/node_modules/bootstrap/dist/css/bootstrap.min.css">
        <!-- Fontawesome -->
        <link rel="stylesheet" href="public/assets/node_modules/@fortawesome/fontawesome-free/css/all.min.css">
        <!-- wow animate -->
        <link rel="stylesheet" href="public/assets/node_modules/wow.js/css/libs/animate.css">
        <!-- select2 -->
        <link rel="stylesheet" href="public/assets/node_modules/select2/dist/css/select2.min.css">
        <!-- Custom -->
        <link rel="stylesheet" href="public/assets/web/css/css-vars.css">
        <link rel="stylesheet" href="public/assets/web/css/css-styles.css">
        <link rel="stylesheet" href="public/assets/web/css/css-responsive.css">
		<style>
			.carousel-indicators {bottom: 115px;}
			.separador_slider{ position: absolute; bottom: 0; display: flex; right: 0; left: 0; z-index: 14; width: 100%; height: 100px;  background: url('https://www.fourdivers.com/web/public/images/sliders/separador_slider.png');}
			.txt12{ font-size: 12px;}
			.txt18{ font-size: 18px;}
			.padding0{padding: 0}
			.margin0{margin: 0}
			.txtWhite{color: #fff;}
			.css-marginR15{ margin-right: 15px;}
			.tripadvisor{ position: absolute; left: 100px; top: 100px;}
			.select2-container--default .select2-selection--single, .select2-selection .select2-selection--single {border: 1px solid #d2d6de !important;  font-size: 14px;}
			.txtVerde{color: #377789;}
			.css-bold{ font-weight: 900;}
			.cnt_centrar{
                display: flex;
                justify-content: center;
                align-items: center;
                height: 370px;
			}
			.centrarTxt{
                display: table-cell;
                text-align: center;
                vertical-align: middle;
			}
			.btn-link:hover {
                color: #fff;
                text-decoration: none;
                background: #96d3e0;
			}
			#search-secion{
			    padding: 40px 0;
			}
			#search-secion h3 {
				font-size: 24px;
			}
			.input-group {
				height: 42px;
			}
			.form-control {
                height: auto;
                border-radius: 0px;
			}
			.input-group-text { border-radius: 0px; }
			#sponsor-section h3{ color: #377789;font-family: "museo_slab700"; font-size: 22px; }
	    </style>
    </head>
    
    <body>
        <header class="css-header css-index-header">
			<div class="separador_slider"></div>
			<div class="tripadvisor"><img src="public/images/logos/tripadvisor-white.png"  alt="tripadvisor"/></div>
            <?php include('includes/navigation.php'); ?>
            
            <div id="index-slider" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#index-slider" data-slide-to="0" class="active"></li>
                    <li data-target="#index-slider" data-slide-to="1"></li>
                </ol>

                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="carousel-caption d-md-block">
                            <h1>
                                Esta Semana Santa <br>
                                ven con nosotros <br>
                                al Mar Rojo
                            </h1>
                            <a href="#" class="btn btn-link">Ver oferta <i class="fas fa-chevron-right"></i></a>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="carousel-caption d-md-block">
                            <h1>
                                Segundo Slide
                            </h1>
                            <a href="#" class="btn btn-link">Ver oferta <i class="fas fa-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section id="search-secion">
            <div class="container">
                <div class="row">
					<div class="col-md-12">
                        <div class="text-center txtVerde css-font-museo">
                        <h3>¿Buscas aventura?</h3>
                        </div>
                    </div>
				
                    <div class="col-lg-7 col-md-12 d-block mx-auto">
                        <div class="row wow fadeInUp">
						
                            <div class="col-md-4 css-noPadding">
                                <select class="form-control select2" name="sel-search" id="sel-search">
                                    <option value="">Cursos/actividades</option>
                                    <option value="1">Option 1</option>
                                    <option value="2">Option 2</option>
                                    <option value="3">Option 3</option>
                                    <option value="4">Option 4</option>
                                    <option value="5">Option 5</option>
                                    <option value="6">Option 6</option>
                                </select>
                            </div>
                            
                            <div class="col-md-4 css-noPadding">
                                <div class="form-group">
                                    <div class="input-group date" id="datetimepicker2" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker2"/>
                                        <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
						    
                            <div class="col-md-4 css-noPadding">
                                <button type="button" class="btn btn-success btn-flat">Buscar</button>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="text-center css-marginT100">
                    <h2>Descubre nuestros cursos y actividades</h2>
                    <p class="css-marginT15">No es solo bucear, es vivir experiencias única e inolvidables, formarte con profesionales experimentados y vivir aventuras en una de las zonas más exóticas del meditarraneo (Jávea). Tabién disponemos de cursos insitu para Madrid y Sevilla. </p>
                </div>
                
                <div class="row css-marginT50 css-courses-panels-pc">
                    <?php foreach ($arr_courses as $key => $course): ?>
                        <div class="col-lg-3 col-md-6 wow fadeInDown css-marginB20" data-wow-delay="300ms">
                            <div class="css-panel-courses">
                                <div class="css-tag">Principiante</div>
                                <img src="public/images/courses/c-1.jpg" alt="Nombre del curso">
                                <div class="css-panel-text">
                                    <h4>Open Water Diver</h4>
                                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Beatae, cumque. Voluptatem soluta expedita enim laboriosam numquam alias cum consequatur natus, vitae dolorem.</p>
                                    <a href="#">Ver más <i class="fas fa-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    
                    <div class="col-lg-3 col-md-6 wow fadeInDown css-marginB20" data-wow-delay="450ms">
                        <div class="css-panel-courses">
                            <div class="css-tag">Avanzado</div>
                            <img src="public/images/courses/c-2.jpg" alt="Nombre del curso">
                            <div class="css-panel-text">
                                <h4>Open Water Diver</h4>
                                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Beatae, cumque. Voluptatem soluta expedita enim laboriosam numquam alias cum consequatur natus, vitae dolorem.</p>
                                <a href="#">Ver más <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6 wow fadeInDown css-marginB20" data-wow-delay="550ms">
                        <div class="css-panel-courses">
                            <div class="css-tag">Avanzado</div>
                            <img src="public/images/courses/c-3.jpg" alt="Nombre del curso">
                            <div class="css-panel-text">
                                <h4>Open Water Diver</h4>
                                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Beatae, cumque. Voluptatem soluta expedita enim laboriosam numquam alias cum consequatur natus, vitae dolorem.</p>
                                <a href="#">Ver más <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6 wow fadeInDown css-marginB20" data-wow-delay="650ms">
                        <div class="css-panel-courses">
                            <div class="css-tag">Profesional</div>
                            <img src="public/images/courses/c-4.jpg" alt="Nombre del curso">
                            <div class="css-panel-text">
                                <h4>Open Water Diver</h4>
                                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Beatae, cumque. Voluptatem soluta expedita enim laboriosam numquam alias cum consequatur natus, vitae dolorem.</p>
                                <a href="#">Ver más <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- MOBILE -->
            <div id="carousel-courses" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="css-panel-courses">
                            <img src="public/images/courses/c-1.jpg" alt="Nombre del curso">
                            
                            <div class="css-panel-text">
                                <div class="css-tag">Principiante</div>

                                <h4>Open Water Diver</h4>
                                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Beatae, cumque. Voluptatem soluta expedita enim laboriosam numquam alias cum consequatur natus, vitae dolorem.</p>
                                <a href="#">Ver más <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="css-panel-courses">
                            <img src="public/images/courses/c-2.jpg" alt="Nombre del curso">
                            
                            <div class="css-panel-text">
                                <div class="css-tag">Avanzado</div>

                                <h4>Open Water Diver</h4>
                                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Beatae, cumque. Voluptatem soluta expedita enim laboriosam numquam alias cum consequatur natus, vitae dolorem.</p>
                                <a href="#">Ver más <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="css-panel-courses">
                            <img src="public/images/courses/c-3.jpg" alt="Nombre del curso">
                            
                            <div class="css-panel-text">
                                <div class="css-tag">Avanzado</div>

                                <h4>Open Water Diver</h4>
                                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Beatae, cumque. Voluptatem soluta expedita enim laboriosam numquam alias cum consequatur natus, vitae dolorem.</p>
                                <a href="#">Ver más <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="css-panel-courses">
                            <img src="public/images/courses/c-1.jpg" alt="Nombre del curso">
                            
                            <div class="css-panel-text">
                                <div class="css-tag">Profesional</div>

                                <h4>Open Water Diver</h4>
                                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Beatae, cumque. Voluptatem soluta expedita enim laboriosam numquam alias cum consequatur natus, vitae dolorem.</p>
                                <a href="#">Ver más <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <a class="carousel-control-prev" href="#carousel-courses" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true">
                        <i class="fas fa-chevron-circle-left"></i>
                    </span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel-courses" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true">
                        <i class="fas fa-chevron-circle-right"></i>
                    </span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </section>

			<!-- Parallax 1 -->
			<section class="css-parallax-sea-1 center-block text-center">
				<div class="container cnt_centrar">
					<div class="row">
						<div class="col-md-12 col-lg-8 offset-lg-2 centrarTxt">
							<h2 class="txtWhite">¿Por qué elegir Four Divers?</h2>
							<p class="txtWhite css-bold txt18">Somos un equipo cuyo principal objetivo es que descubras nuevos destinos, adentrándote en el mundo del buceo, conociendo gente nueva con las mismas aficiones que tú.</p>
							<a href="#" class="btn btn-link">Descúbrelo <i class="fas fa-chevron-right"></i></a>
						</div>
					</div>
				</div>
			</section>

        <section id="promo-section">
            <div class="container">
                <h2 class="text-center">Otras aventuras interesantes</h2>

                <div class="css-promo-panels-pc">
                    <div class="row css-marginT100 wow fadeInDown" data-wow-delay="300ms">
                        <div class="col-lg-10 col-md-12 css-panel-img-right mx-auto d-block">
                            <img src="public/images/promo/promo-1.jpg" alt="Descubre el mundo Snorkel">
    
                            <div>
                                <h2>Descubre el mundo Snorkel</h2>
        
                                <p>
                                    El Snorkel es la versión más sencilla para acercarse a la vida submarina.
                                    Esta experiencia te brindará la posibilidad de conocer los fondos marinos sin tener que realizar un curso de buceo.
                                </p>
                                <a href="#" class="btn btn-link">Más info <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
    
                    <div class="row css-marginT100 wow fadeInUp" data-wow-delay="450ms">
                        <div class="col-lg-10 col-md-12 css-panel-img-left mx-auto d-block">
                            <img src="public/images/promo/promo-2.jpg" alt="Descubre el mundo Snorkel">
    
                            <div>
                                <h2>Un regalo diferente</h2>
        
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.sectetuer adipiscing elit, sed diam nonummy nibh euismod</p>
                                <a href="#" class="btn btn-link">Más info <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
    
                    <div class="row css-marginT100 wow fadeInDown" data-wow-delay="550ms">
                        <div class="col-lg-10 col-md-12 css-panel-img-right mx-auto d-block">
                            <img src="public/images/promo/promo-3.jpg" alt="Descubre el mundo Snorkel">
    
                            <div>
                                <h2>Excursiones de Kayaks en Jávea</h2>
        
                                <p>Tenemos a tu disposición el alquiler de kayaks de una y dos plazas, para poder utilizarlos no es necesario tener conocimientos técnicos ni ninguna titulación.</p>
                                <a href="#" class="btn btn-link">Más info <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
    
                    <div class="row css-marginT100 wow fadeInUp" data-wow-delay="650ms">
                        <div class="col-lg-10 col-md-12 css-panel-img-left mx-auto d-block">
                            <img src="public/images/promo/promo-4.jpg" alt="Descubre el mundo Snorkel">
    
                            <div>
                                <h2>Mantenimientode tu equipo</h2>
        
                                <p>Ya sabías que lo que peor le sienta a los equipos es la sal, pero...¿y la cal disuelta en el agua? ¿y el polvo de óxido de las botellas?</p>
                                <a href="#" class="btn btn-link">Más info <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- MOBILE -->
                <div id="carousel-promo" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="css-panel-img-left mx-auto d-block">
                                <img src="public/images/promo/promo-4.jpg" alt="Descubre el mundo Snorkel">
        
                                <div>
                                    <h2>Mantenimientode tu equipo</h2>
            
                                    <p>Ya sabías que lo que peor le sienta a los equipos es la sal, pero...¿y la cal disuelta en el agua? ¿y el polvo de óxido de las botellas?</p>
                                    <a href="#" class="btn btn-link">Más info <i class="fas fa-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="css-panel-img-left mx-auto d-block">
                                <img src="public/images/promo/promo-2.jpg" alt="Descubre el mundo Snorkel">
        
                                <div>
                                    <h2>Un regalo diferente</h2>
            
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.sectetuer adipiscing elit, sed diam nonummy nibh euismod</p>
                                    <a href="#" class="btn btn-link">Más info <i class="fas fa-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="css-panel-img-right mx-auto d-block">
                                <img src="public/images/promo/promo-3.jpg" alt="Descubre el mundo Snorkel">
        
                                <div>
                                    <h2>Excursiones de Kayaks en Jávea</h2>
            
                                    <p>Tenemos a tu disposición el alquiler de kayaks de una y dos plazas, para poder utilizarlos no es necesario tener conocimientos técnicos ni ninguna titulación.</p>
                                    <a href="#" class="btn btn-link">Más info <i class="fas fa-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="css-panel-img-left mx-auto d-block">
                                <img src="public/images/promo/promo-4.jpg" alt="Descubre el mundo Snorkel">
        
                                <div>
                                    <h2>Mantenimientode tu equipo</h2>
            
                                    <p>Ya sabías que lo que peor le sienta a los equipos es la sal, pero...¿y la cal disuelta en el agua? ¿y el polvo de óxido de las botellas?</p>
                                    <a href="#" class="btn btn-link">Más info <i class="fas fa-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <a class="carousel-control-prev" href="#carousel-promo" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true">
                            <i class="fas fa-chevron-circle-left"></i>
                        </span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carousel-promo" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true">
                            <i class="fas fa-chevron-circle-right"></i>
                        </span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </section>

        <!-- Parallax 2 -->
        <section class="css-parallax-sea-2 text-center">
            <img src="public/images/logos/logo-footer.png" class="img-responsive" alt="">
        </section>

        <section id="sponsor-section">
            <div class="container">
                <h3 class="text-center">Centro de buceo certificado PADI en Jávea, Sevilla y Madrid</h3>

                <div class="css-sponsor">
                    <img src="public/images/logos/padi-logo.png" alt="PADI" width="100">
                    <img src="public/images/logos/dan-logo.png" alt="PADI" width="200">
                </div>
            </div>
        </section>

        <?php include("includes/footer.php") ?>
		        <script type="text/javascript">
            $(function () {
                $('#datetimepicker2').datetimepicker({
                    locale: 'es'
                });
            });
        </script>
    </body>
    
    <!-- Javascript -->
    <!-- jquery -->
    <script src="public/assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="public/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- WOW animate -->
    <script src="public/assets/node_modules/wow.js/dist/wow.js"></script>
    <!-- Select2 -->
    <script src="public/assets/node_modules/select2/dist/js/select2.min.js"></script>
    <!-- Custom -->
    <script src="public/assets/web/js/main.js"></script>
	
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
		<script type="text/javascript" src="public/assets/web/js/tempusdominus-bootstrap-4.js"></script>
  		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" />
</head>
</html>