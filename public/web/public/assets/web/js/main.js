$(function(e){
	// Menu fixed
	$(window).on('scroll', function(){
		if( $(window).scrollTop()>slideHeight ){
			$('#js-main-navigation').addClass('css-navbar-fixed-top');
		} else {
			$('#js-main-navigation').removeClass('css-navbar-fixed-top');
		}
	});

	// Menu mobile
	$('[data-toggle=collapse]').on('click',function(){
		$(this).trigger('click');
	});

	// header al mismo tamaño de la pantalla
    var slideHeight = $(window).height();
	$('#index-slider .carousel-item').css('height',slideHeight);

	$(window).resize(function(){'use strict',
		$('#index-slider .carousel-item').css('height',slideHeight);
	});

	// select2
	$('.select2').select2();
	
	// WOW animate
	new WOW().init();

	// Intalaiones
	$('.folio-expand a').on('click', function(e) {
		e.preventDefault();
		var img = $(this).data('img');

		$('#mod-image').modal('show');
		$('#mod-image .modal-body').html(' ').html('<img src="' + img + '" class="img-fluid" alt="Four Divers">');
	})
});