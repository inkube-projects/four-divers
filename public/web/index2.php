<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Four Divers | Inicio</title>
        <!-- CSS -->
        <!-- Bootstrap -->
        <link rel="stylesheet" href="public/assets/node_modules/bootstrap/dist/css/bootstrap.min.css">
        <!-- Fontawesome -->
        <link rel="stylesheet" href="public/assets/node_modules/@fortawesome/fontawesome-free/css/all.min.css">
        <!-- wow animate -->
        <link rel="stylesheet" href="public/assets/node_modules/wow.js/css/libs/animate.css">
        <!-- select2 -->
        <link rel="stylesheet" href="public/assets/node_modules/select2/dist/css/select2.min.css">
        <!-- Custom -->
        <link rel="stylesheet" href="public/assets/web/css/css-vars.css">
        <link rel="stylesheet" href="public/assets/web/css/css-styles.css">
        <link rel="stylesheet" href="public/assets/web/css/css-responsive.css">
    </head>
    
    <body>
        <header class="css-header css-index-header">
            <?php include('includes/navigation.php'); ?>
            
            <div id="index-slider" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#index-slider" data-slide-to="0" class="active"></li>
                    <li data-target="#index-slider" data-slide-to="1"></li>
                </ol>

                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="carousel-caption d-md-block">
                            <h1>
                                Esta Semana Santa <br>
                                ven con nosotros <br>
                                al Mar Rojo
                            </h1>
                            <a href="#" class="btn btn-link">Ver oferta <i class="fas fa-chevron-right"></i></a>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="carousel-caption d-md-block">
                            <h1>
                                Segundo Slide
                            </h1>
                            <a href="#" class="btn btn-link">Ver oferta <i class="fas fa-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section id="search-secion">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-12 d-block mx-auto">
                        <div class="row wow fadeInUp">
                            <div class="col-md-4 css-noPadding">
                                <select class="form-control select2" name="sel-search" id="sel-search">
                                    <option value="">¿Qué buscas?</option>
                                    <option value="1">Option 1</option>
                                    <option value="2">Option 2</option>
                                    <option value="3">Option 3</option>
                                    <option value="4">Option 4</option>
                                    <option value="5">Option 5</option>
                                    <option value="6">Option 6</option>
                                </select>
                            </div>
        
                            <div class="col-md-4 css-noPadding">
                                <select class="form-control select2" name="sel-date" id="sel-date">
                                    <option value="">¿En que fechas?</option>
                                    <option value="1">Option 1</option>
                                    <option value="2">Option 2</option>
                                    <option value="3">Option 3</option>
                                    <option value="4">Option 4</option>
                                    <option value="5">Option 5</option>
                                    <option value="6">Option 6</option>
                                </select>
                            </div>
        
                            <div class="col-md-4 css-noPadding">
                                <button type="button" class="btn btn-success btn-flat">Buscar</button>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="text-center css-marginT100">
                    <h2>Descubre nuestros cursos <br> y actividades</h2>
                    <p class="css-marginT15">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Alias aliquam, quisquam repudiandae hic illo unde tempora dolorem, dicta officia fuga possimus est facilis accusamus repellendus consequuntur ipsam, necessitatibus vel voluptatum.</p>
                </div>
                
                <div class="row css-marginT80">
                    <div class="col-lg-3 col-md-6 wow fadeInDown css-marginB20" data-wow-delay="300ms">
                        <div class="css-panel-courses">
                            <div class="css-tag">Principiante</div>
                            <img src="public/images/courses/c-1.jpg" alt="Nombre del curso">
                            <div class="css-panel-text">
                                <h4>Open Water Diver</h4>
                                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Beatae, cumque. Voluptatem soluta expedita enim laboriosam numquam alias cum consequatur natus, vitae dolorem.</p>
                                <a href="#">Ver más <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6 wow fadeInDown css-marginB20" data-wow-delay="450ms">
                        <div class="css-panel-courses">
                            <div class="css-tag">Avanzado</div>
                            <img src="public/images/courses/c-2.jpg" alt="Nombre del curso">
                            <div class="css-panel-text">
                                <h4>Open Water Diver</h4>
                                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Beatae, cumque. Voluptatem soluta expedita enim laboriosam numquam alias cum consequatur natus, vitae dolorem.</p>
                                <a href="#">Ver más <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6 wow fadeInDown css-marginB20" data-wow-delay="550ms">
                        <div class="css-panel-courses">
                            <div class="css-tag">Avanzado</div>
                            <img src="public/images/courses/c-3.jpg" alt="Nombre del curso">
                            <div class="css-panel-text">
                                <h4>Open Water Diver</h4>
                                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Beatae, cumque. Voluptatem soluta expedita enim laboriosam numquam alias cum consequatur natus, vitae dolorem.</p>
                                <a href="#">Ver más <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6 wow fadeInDown css-marginB20" data-wow-delay="650ms">
                        <div class="css-panel-courses">
                            <div class="css-tag">Profesional</div>
                            <img src="public/images/courses/c-1.jpg" alt="Nombre del curso">
                            <div class="css-panel-text">
                                <h4>Open Water Diver</h4>
                                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Beatae, cumque. Voluptatem soluta expedita enim laboriosam numquam alias cum consequatur natus, vitae dolorem.</p>
                                <a href="#">Ver más <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Parallax 1 -->
        <section class="css-parallax-sea-1">

        </section>

        <section id="promo-section">
            <div class="container">
                <h2 class="text-center">Tambien te puede interesar</h2>

                <div class="row css-marginT100 wow fadeInDown" data-wow-delay="300ms">
                    <div class="col-lg-10 col-md-12 css-panel-img-right mx-auto d-block">
                        <img src="public/images/promo/promo-1.jpg" alt="Descubre el mundo Snorkel">

                        <div>
                            <h2>Descubre el mundo Snorkel</h2>
    
                            <p>
                                El Snorkel es la versión más sencilla para acercarse a la vida submarina.
                                Esta experiencia te brindará la posibilidad de conocer los fondos marinos sin tener que realizar un curso de buceo.
                            </p>
                            <a href="#" class="btn btn-link">Más info <i class="fas fa-chevron-right"></i></a>
                        </div>
                    </div>
                </div>

                <div class="row css-marginT100 wow fadeInUp" data-wow-delay="450ms">
                    <div class="col-lg-10 col-md-12 css-panel-img-left mx-auto d-block">
                        <img src="public/images/promo/promo-2.jpg" alt="Descubre el mundo Snorkel">

                        <div>
                            <h2>Un regalo diferente</h2>
    
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.sectetuer adipiscing elit, sed diam nonummy nibh euismod</p>
                            <a href="#" class="btn btn-link">Más info <i class="fas fa-chevron-right"></i></a>
                        </div>
                    </div>
                </div>

                <div class="row css-marginT100 wow fadeInDown" data-wow-delay="550ms">
                    <div class="col-lg-10 col-md-12 css-panel-img-right mx-auto d-block">
                        <img src="public/images/promo/promo-3.jpg" alt="Descubre el mundo Snorkel">

                        <div>
                            <h2>Excursiones de Kayaks en Jávea</h2>
    
                            <p>Tenemos a tu disposición el alquiler de kayaks de una y dos plazas, para poder utilizarlos no es necesario tener conocimientos técnicos ni ninguna titulación.</p>
                            <a href="#" class="btn btn-link">Más info <i class="fas fa-chevron-right"></i></a>
                        </div>
                    </div>
                </div>

                <div class="row css-marginT100 wow fadeInUp" data-wow-delay="650ms">
                    <div class="col-lg-10 col-md-12 css-panel-img-left mx-auto d-block">
                        <img src="public/images/promo/promo-4.jpg" alt="Descubre el mundo Snorkel">

                        <div>
                            <h2>Mantenimientode tu equipo</h2>
    
                            <p>Ya sabías que lo que peor le sienta a los equipos es la sal, pero...¿y la cal disuelta en el agua? ¿y el polvo de óxido de las botellas?</p>
                            <a href="#" class="btn btn-link">Más info <i class="fas fa-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Parallax 2 -->
        <section class="css-parallax-sea-2 text-center">
            <img src="public/images/logos/logo-footer.png" class="img-responsive" alt="">
        </section>

        <section id="sponsor-section">
            <div class="container">
                <h2 class="text-center">Centro de buceo PASI <br> en Jávea certificado</h2>

                <div class="css-sponsor">
                    <img src="public/images/logos/padi-logo.png" alt="PADI" width="100">
                    <img src="public/images/logos/dan-logo.png" alt="PADI" width="200">
                </div>
            </div>
        </section>

        <?php include("includes/footer.php") ?>
    </body>
    <!-- Javascript -->
    <!-- jquery -->
    <script src="public/assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="public/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- WOW animate -->
    <script src="public/assets/node_modules/wow.js/dist/wow.js"></script>
    <!-- Select2 -->
    <script src="public/assets/node_modules/select2/dist/js/select2.min.js"></script>
    <!-- Custom -->
    <script src="public/assets/web/js/main.js"></script>
</html>