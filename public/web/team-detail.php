<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Four Divers | Equipo</title>
        <!-- CSS -->
        <!-- Bootstrap -->
        <link rel="stylesheet" href="public/assets/node_modules/bootstrap/dist/css/bootstrap.min.css">
        <!-- Fontawesome -->
        <link rel="stylesheet" href="public/assets/node_modules/@fortawesome/fontawesome-free/css/all.min.css">
        <!-- wow animate -->
        <link rel="stylesheet" href="public/assets/node_modules/wow.js/css/libs/animate.css">
        <!-- select2 -->
        <link rel="stylesheet" href="public/assets/node_modules/select2/dist/css/select2.min.css">
        <!-- Custom -->
        <link rel="stylesheet" href="public/assets/web/css/css-vars.css">
        <link rel="stylesheet" href="public/assets/web/css/css-styles.css">
        <link rel="stylesheet" href="public/assets/web/css/css-responsive.css">
    </head>

    <body class="css-background-default">
        <header class="css-header css-team-detail-header">
            <?php include('includes/navigation.php'); ?>
            <div class="clearfix"></div>
        </header>

        <section id="team-detail-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <img src="public/images/team/team-1-detail.jpg" class="img-fluid" alt="Nombre Apellido">
                    </div>

                    <div class="col-lg-6">
                        <h2>Nombre Apellido</h2>

                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
                        <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit.</p>

                        <h3>Contacto</h3>
                        <p class="css-contact-info">Email: nombre@fourdivers.com</p>
                        <p class="css-contact-info">Tel. 600 000 000</p>

                        <div class="css-social">
                            <a href="#"><div><i class="fab fa-facebook-f"></i></div></a>
                            <a href="#"><div><i class="fab fa-instagram"></i></div></a>
                            <a href="#"><div><i class="fab fa-twitter"></i></div></a>
                            <a href="#"><div><i class="fab fa-youtube"></i></div></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php include("includes/footer.php") ?>
    </body>

    <!-- jquery -->
    <script src="public/assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="public/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- WOW animate -->
    <script src="public/assets/node_modules/wow.js/dist/wow.js"></script>
    <!-- Select2 -->
    <script src="public/assets/node_modules/select2/dist/js/select2.min.js"></script>
    <!-- Custom -->
    <script src="public/assets/web/js/main.js"></script>
</html>