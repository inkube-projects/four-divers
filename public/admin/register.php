<?php
$adminArea=true;
$autoInitLoginSystem=false;
require_once '../../sys/core/init.inc.php';
// configuración Login System lo deberán tener todos loas archivos que requieran al usuario logueado
include_once '../../sys/config/LS-admin-cred.inc.php';


echo "AA" ;  print_r($LS);

?>
<!DOCTYPE html>
<html>
  <head></head>
  <body>
    <div class="content">
      <h1>Register</h1>
      <form action="register.php" method="POST">
        <label>
          <input name="username" placeholder="Username" />
        </label>
       
          <input name="pass" type="password" placeholder="Password" />
        </label>
        <label>
          <input name="retyped_password" type="password" placeholder="Retype Password" />
        </label>
        <label>
          <button name="submit">Register</button>
        </label>
      </form>
      <?php
      if( isset($_POST['submit']) ){
        $username = $_POST['username'];
        $email="dd@pp.com";
        $password = $_POST['pass'];
        $retyped_password = $_POST['retyped_password'];
        
        if( $username == "" ||  $password == '' || $retyped_password == ''  ){
            echo "<h2>Fields Left Blank</h2>", "<p>Some Fields were left blank. Please fill up all fields.</p>";
        }elseif( !ctype_alnum($username) ){
            echo "<h2>Invalid Username</h2>", "<p>The Username is not valid. Only ALPHANUMERIC characters are allowed and shouldn't exceed 10 characters.</p>";
        }elseif($password != $retyped_password){
            echo "<h2>Passwords Don't Match</h2>", "<p>The Passwords you entered didn't match</p>";
        }else{
          $createAccount = $LS->register($username, $password,
            array(
              "email" => $email,
              "created" => date("Y-m-d H:i:s") // Just for testing
            )
          );
          if($createAccount === "exists"){
            echo "<label>User Exists.</label>";
          }elseif($createAccount === true){
            echo "<label>Success. Created account. <a href='login.php'>Log In</a></label>";
          }
        }
      }
      ?>
      <style>
        label{
          display: block;
          margin-bottom: 5px;
        }
      </style>
    </div>
  </body>
</html>
