<?php

include('include/app-init-login.php');
$useCustomCheck=true; $useCustomSelect=true; $useDateTimePicker=true; $useSweetAlert=true;
$plusTitle="";
if( isset($_REQUEST['action']) && $_REQUEST['action']=="add" ){
 $action='add'; $rowDetail=array('id'=>0,'date_created'=>date('Y-m-d')); 
}
else if( isset($_REQUEST['idObj']) && intval($_REQUEST['idObj'])>0 ) { 
 $action='update';
 $rowDetail = AdminReservations::reservationDetail($dbo,$_REQUEST['idObj']);
 $plusTitle=" - ".$rowDetail['name']." ".$rowDetail['surname'];
} else {
  echo "Incorrect access";
  exit();
}

$rowDetail['action']=$action;

?>
<!DOCTYPE html>
<head>
	<?php include('include/estructure/head.php'); ?>
	<link rel="stylesheet" href="<?=URLADMIN?>assets/css/user_detail.css"/>
</head>
<body>
  <!-- Wrapper-->
  <div class="wrapper">
    <?php include('include/estructure/header.php'); ?>
    <!-- Main content-->
    <section class="content">
      <div class="container-fluid" style="opacity: 1;">

        <!--FORMULARIO RESERVA-->
        <form action="#" id="reservationDetail">
         <input id="formToken"  type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
         <input type="hidden" name="action" id="action" value="reservation-<?=$action?>" />
         <input type="hidden" name="reservationId" id="reservationId" value="<?=$rowDetail['id']?>" />


         <!--TITULAR  SECCION-->
         <div class="row">
          <div class="col-lg-12">
            <div class="view-header">
             <div class="pull-right text-right" style="line-height: 14px">
               <a style="display: none;" href="#" class="btn btn-w-md btn-info"> facturar</a>
               <a id="createReservationBtn" href="#" class="btn btn-w-md btn-info"> crear</a>
             </div>
             <div class="header-icon">
              <i class="pe page-header-icon pe-7s-cart"></i>
            </div>
            <div class="header-title">
              <h3 class="m-b-xs">Reservas</h3>
              <small>
                Gestión de reservas 
              </small>

            </div>
          </div>
          <hr>
        </div>
      </div>
      <!--END. TITULAR  SECCION-->

      <div class="row">

        <!--DATOS RESERVA-->
        <div class="col-md-12">
          <div class="panel panel-filled">
            <div class="panel-heading">
              <h4> Datos Reserva</h4>

            </div>
            <div class="panel-body">

              <div class="row">
                <div class="form-group  col-md-6 ">
                  <label>Cliente</label>
                  <select style="width: 100%" class="form-control client-select" ></select>
                </div>

                <div class="form-group  col-md-3 ">
                  <label>Fecha</label>
                  <input readonly   type="text" value="<?=date('d-m-Y')?>" style="width: 100%" class="form-control " />
                </div>

                <div class="form-group col-md-3">
                  <label>Estado</label>
                   <input readonly   type="text" value="Activa" style="width: 100%" class="form-control " />
                </div>

              </div>

            </div>
          </div>
        </div>

        <div class="btnFixed">
         <div class="cnt_btnFixed">
          <a class="btn btn-accent save-reservation-btn" type="button" data-toggle="tooltip"  title="guardar"><i class="fa fa-repeat"></i></a>
          <a href="<?=URL?>LISTADO-RESERVAS" class="btn btn-primary"  data-toggle="tooltip"  title="volver"><i class="fa fa-step-backward"></i></a>
        </div>
      </div>

      <div class="form-group col-md-12">
       <a style="display:none" href="#" class="btn btn-danger delete-reservation-btn"> x borrar reserva</a>
       <a href="<?=URL?>LISTADO-RESERVAS" class="btn btn-primary"> < volver al listado </a>
     </div>

   </div>

 </form>
 <!--END FORMULARIO USUARIO-->

 <?php include_once("include/modals/common-modals.php"); ?>
</div>
</section>
<!-- End main content-->

</div>
<!-- End wrapper-->

<?php 
include('include/js.php'); 
?>
<script>
  var userDetail=<?=json_encode($rowDetail)?>;
  console.log(userDetail);
</script>

<script type="text/javascript" src="<?=URLADMIN?>assets/js/admin/reservations/reservation-detail.js"></script>

</body>

</html>