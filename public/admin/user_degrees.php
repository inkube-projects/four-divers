<?php

include('include/app-init-login.php');
$useCustomSelect=true; $useSweetAlert=true; $useDataTables=true;  $useDateTimePicker=true;
$plusTitle="";
$avatarImage=URLADMIN.'images/icoUser.png';
if( isset($_REQUEST['idObj']) && intval($_REQUEST['idObj'])>0 ) { 
 $rowDetail = AdminClients::clientDetail($dbo,$_REQUEST['idObj']);
 $plusTitle=" - ".$rowDetail['name']." ".$rowDetail['surname'];
 if(strlen($rowDetail['avatar'])>0) $avatarImage = URLPUBLIC."assets/images/users/u-".$rowDetail['id']."/".$rowDetail['avatar'];
} else {
  echo "Incorrect access";
  exit();
}

?>
<!DOCTYPE html>
<head>
	<?php include('include/estructure/head.php'); ?>
	<link rel="stylesheet" href="<?=URLADMIN?>assets/css/user_detail.css"/>
</head>
<body>
  <!-- Wrapper-->
  <div class="wrapper">
    <?php include('include/estructure/header.php'); ?>
    <!-- Main content-->
    <section class="content">
      <div class="container-fluid" style="opacity: 1;">

      
         <!--TITULAR  SECCION-->
         <div class="row">
          <div class="col-lg-12">
            <div class="view-header">
              <div  class="header-icon2"> 
                <img data-filename="<?=$rowDetail['avatar']?>" id="avatarImage" src="<?=$avatarImage?>" class="avatar rounded-circle" alt="">
              </div>
              <div class="header-title">
                <h3 class="m-b-xs"><?=$rowDetail['name']." ".$rowDetail['surname']?></h3>
                <small>
                 <span class="text-accent"> ID: <?=$rowDetail['nif']?></span>
               </small>

               <div class="m-xs pull-right">
                 <div class="" style="width: 100px; height: 25px; display: inline;"><button type="button"  onclick="window.location='<?=URL?>DETALLE-CLIENTE/<?=$rowDetail['id']?>'" class="btn btn-primary pull-right css-marginR10">
                  <i class="fa fa-chevron-left"></i> <span class="bold">  volver </span>
                </button></div>

              </div>

            </div>
          </div>
          <hr>
        </div>
      </div>
      <!--END. TITULAR  SECCION-->
      <div class="row" style="margin-top: 20px">

        <!--DATOS DE HISTÓRICO  CURSOS -->
        <div class="col-md-12">
         <div class="panel panel-filled">
          <div class="panel-heading">
            <h4> Histórico de titulaciones (Fourdivers)</h4>
          </div>

          <div class="panel-body">

         <!--FORMULARIO USUARIO-->
        <form action="#" id="userDegreeForm">
         <input id="formToken"  type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
         <input type="hidden" name="action" id="action" value="clientDegree-add" />
         <input type="hidden" name="user_id" id="userId" value="<?=$rowDetail['id']?>" />

           <div class="row">

             <div class="form-group col-xs-12 col-md-6 col-lg-6">
              <label>Titulación</label>
              <select class="select2_demo_1 form-control" id="userCourse"  name="course_id"  style="width: 100%">
                <option value=""></option>
                <?php $courses = AdminProducts::getBasicInfoByFamily($dbo,1);
                foreach ($courses as $key => $value) { echo ' <option value="'.$value['id'].'">'.$value['name'].'</option>' ; }
                ?>
              </select>
            </div>


            <div class="form-group col-xs-12 col-md-6 col-lg-3">
              <label>CODE</label>
              <input type="text" value="" id="degreeCode" class="form-control" name="code">
            </div>

            <div class="form-group col-xs-12 col-md-6 col-lg-3">
              <label>DIVER NO.</label>
              <input type="text" value="" id="degreeDiver_no" class="form-control" name="diver_no">
            </div>

            <div class="form-group col-xs-12 col-md-6 col-lg-3">
              <label>Fecha </label>
                   <div class="input-group date" id="datetimepicker-date" data-target-input="nearest">
                     <input name="date" value="" type="text" class="form-control datetimepicker-input" data-target="#datetimepicker-date"/>
                     <div class="input-group-append" data-target="#datetimepicker-date" data-toggle="datetimepicker">
                      <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                  </div>
              </div>
            </div>

            <div class="form-group col-xs-12 col-md-6 col-lg-3">
              <label>Comentario </label><br>
              <input type="text" value="" id="degreeComment" class="form-control for" name="comment">
            </div>


            <div class="form-group col-lg-3">
              <label>Estado</label>
              <select class="select2_demo_1 form-control" id="degreeStatus"  name="status"  style="width: 100%">
               <option value="1">En proceso</option>
               <option value="2">Aprobado</option>
             </select>
           </div>

           <div class="form-group col-md-3">
             <label>&nbsp;</label>
             <button id="clientDegreeBtn" type="button" class="btn btn-accent btn-block"> añadir curso </button>			
           </div>


         </div>
       </form>

       </div>



       <div class="panel-body">

        <div class="row">

         <div class="form-group col-lg-3 text-muted css-text18 " style="padding-left: 40px;">
          <span class="pe-7s-info css-text34 text-warning" style="position: absolute; left: 0; top: -5px"></span> NÚMERO IDENTIFICACIÓN <strong>PADI: </strong>
        </div>
        <div class="form-group col-lg-2 text-muted css-text18">
          <input id="padi_number" style="background: none; border: none;"  value="<?=$rowDetail['padi_number']?>" type="text" class="text-warning">
        </div>

        <div class="form-group col-lg-1 ">

         <button type="button" class="btn btn-default btn-xs btn-block" id="save-padi-number">Actualizar</button>
       </div>

     </div>


        <div class="row">

         <table id="userDegreesTable" class="table table-hover table-responsive-sm">
          <thead>
            <tr>
             <th>ID</th>
             <th>Curso ID</th>
             <th>Estado ID</th>
             <th>Curso</th>
             <th>CODE</th>
             <th>DIVER NO</th>
             <th>Fecha</th>
             <th>Coment</th>
             <th>Estado</th>
             <th>Eliminar</th>
           </tr>
         </thead>
         <tbody>
       </tbody>
     </table>
   </div>
 </div>
</div>
</div>

</div>


<?php include_once("include/modals/common-modals.php"); ?>
</div>
</section>
<!-- End main content-->

</div>
<!-- End wrapper-->

<?php 
include('include/js.php'); 
?>
<script>
  var userDetail=<?=json_encode($rowDetail)?>;
  console.log(userDetail);
</script>

<script type="text/javascript" src="<?=URLADMIN?>assets/js/admin/users/user-degrees.js"></script>

</body>

</html>