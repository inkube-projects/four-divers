<?php
//declare(strict_types=1);
/*
* Enable sessions if needed.
* Avoid pesky warning if session already active.
*/
/*$status = session_status();
if ($status == PHP_SESSION_NONE){
//There is no active session
session_start();
}*/

if(!isset($_SESSION))session_start();


// seguridad
if( !isset($_REQUEST['action']) || ( $_REQUEST['token'] != $_SESSION['token'] ) ){
	exit;
}
	

	
	/*
	* Include necessary files
	*/
	require '../../../../sys/config/db-cred.inc.php';
	/*
	* Define constants for config info
	*/
	foreach ( $C as $name => $val ){
	define($name, $val);
	}
	
		

	/*
	* Create a Meekrodb object */
	$dbo = new MeekroDB(DB_HOST, DB_USER, DB_PASS,  DB_NAME, DB_PORT, DB_ENCODING);
	
	/*
	* Include necessary files
	*/
	$autoInitLoginSystem=false; // evitamos que la clase se ejecute con verificaciones
	require '../../../../sys/config/LS-admin-cred.inc.php';
	/*
	* Define constants for config info
	*/
	if( isset($_GET['action']) && ( $_GET['action']=="images_list" ||  $_GET['action']=="files_list") ) {
		foreach ($_GET as $key => $value) { $_POST[$key]=$value; }
		} 
	
	/*
	* Create a lookup array for form actions
	*/
	$ACTIONS=array(
	  // users-admin
		'user_login' => array('object' => $LS,'method' => 'processLoginForm'),
		'user_change_password' => array('object' => $LS,'method' => 'processChangePasswordForm'),
		'user-reset-password' => array('object' => $LS,'method' => 'processResetPasswordForm'),
     	'change_companyFilter' => array('object' => 'AdminData','method' => 'changeCompanyFilter','param1' => $dbo),
		// images
		'images_list' => array('object' => 'AdminImages','method' => 'listImages','param1' => $dbo ),
		'images_update' => array('object' => 'AdminImages','method' => 'updateImage','param1' => $dbo),
		'images_delete' => array('object' => 'AdminImages', 'method' => 'deleteImage','param1' => $dbo),
		'images_delete_multiple' => array('object' => 'AdminImages', 'method' => 'deleteImageMultiple','param1' => $dbo),
		'images_rotate' => array('object' => 'AdminImages', 'method' => 'rotateImage','param1' => $dbo),
		// files
		'files_list' => array('object' => 'AdminFiles','method' => 'listFiles','param1' => $dbo ),
		// videos
		'video_add' => array('object' => 'AdminVideos','method' => 'addVideo','param1' => $dbo ),
		'video_update' => array('object' => 'AdminVideos','method' => 'updateVideo','param1' => $dbo ),
		// clients
		'list_clients' => array('object' => 'AdminClients','method' => 'listClients','param1' => $dbo ),
		'client-add' => array('object' => 'AdminClients','method' => 'addClient','param1' => $dbo ),
		'client-update' => array('object' => 'AdminClients','method' => 'updateClient','param1' => $dbo ),
		'client-delete' => array('object' => 'AdminClients','method' => 'deleteClient','param1' => $dbo ),
		'client-search' => array('object' => 'AdminClients','method' => 'searchClient','param1' => $dbo ),
		'userPadiNumber' => array('object' => 'AdminClients','method' => 'updatePadiNumber','param1' => $dbo ),
		// clients degrees
		'list_clientDegrees' => array('object' => 'AdminClientDegrees','method' => 'listClientDegrees','param1' => $dbo ),
	  	'clientDegree-update' => array('object' => 'AdminClientDegrees','method' => 'updateDegree','param1' => $dbo ),
	  	'clientDegree-add' => array('object' => 'AdminClientDegrees','method' => 'addDegree','param1' => $dbo ),
	  	'clientDegree-delete' => array('object' => 'AdminClientDegrees','method' => 'deleteDegree','param1' => $dbo ),
	  	// products
		'list_products' => array('object' => 'AdminProducts','method' => 'listProducts','param1' => $dbo ),
		'product-add' => array('object' => 'AdminProducts','method' => 'addProduct','param1' => $dbo ),
		'product-update' => array('object' => 'AdminProducts','method' => 'updateProduct','param1' => $dbo ),
		'product-delete' => array('object' => 'AdminProducts','method' => 'deleteProduct','param1' => $dbo ),
		'product-language' => array('object' => 'AdminProducts','method' => 'languageProduct','param1' => $dbo ),
		'product-categories-providers'  => array('object' => 'AdminProducts','method' => 'getCategoriesAndProviders','param1' => $dbo ),
		 // reservations
		'list_reservations' => array('object' => 'AdminReservations','method' => 'listReservations','param1' => $dbo ),
		'reservation-add' => array('object' => 'AdminReservations','method' => 'addReservation','param1' => $dbo ),
		'reservation-update' => array('object' => 'AdminReservations','method' => 'updateReservation','param1' => $dbo ),
		'reservation-delete' => array('object' => 'AdminReservations','method' => 'deleteReservation','param1' => $dbo ),
	);
	//

     
    
	/*
	* Make sure the anti-CSRF token was passed and that the
	* requested action exists in the lookup array
	*/
	if ( isset($ACTIONS[$_POST['action']]) ){
		
		
		$use_array = $ACTIONS[$_POST['action']];
		
		
		if(is_object($use_array['object'])) $obj=$use_array['object'];
		elseif (!is_null($use_array['param1'])){
			$obj = new $use_array['object']($use_array['param1']); // pasar de forma opcional conexión a bd $dbo
			}
		else {
		$obj = new $use_array['object'](); 
		
		
		
		}
		$method = $use_array['method'];
	
		
		echo $obj->$method();
	}
	
	
	function __autoload($class_name){
	$filename = '../../../../sys/class/class.'.$class_name. '.inc.php';
		if ( file_exists($filename) ){
			include_once $filename;
		}
	}
?>