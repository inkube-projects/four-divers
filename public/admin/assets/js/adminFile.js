// en el formulario principal se debe indicar un hidden con id adminSection y propiedad data-seoname

var processFile = "assets/inc/ajaxAdmin.inc.php";
var myDropZone;
	
  //maxFilesize máximo tamaño upload 5M
  Dropzone.options.myAwesomeDropzone = {
	  
	  acceptedFiles: ".zip,.xls,.xlsx,.rar,.pdf,.doc,.docx",
	  clickable: "#addImgBtn",
	  dictDefaultMessage: '',
	  init: function() {
		  
		  
	  myDropZone = this;
		  
	  $.ajax({
				type: "POST",
					url: processFile,
					data:{
						id: arrDetail['id'],
						idPresu: arrDetail['idPresu'],
						action: 'files_list',
						token: tokenId
					},
					success: function(data){
						
						var data = JSON.parse(data);
	
						$.each(data, function(key,value){
             
						var mockFile = { name: value.name, size: value.size};
						myDropZone.options.addedfile.call(myDropZone, mockFile);
						myDropZone.options.thumbnail.call(myDropZone, mockFile, ruta+"images/"+value.extension+".png");
			
						addButtons(myDropZone, mockFile, value.id, value.alt,value.ruta,value.name);
						myDropZone.emit("complete", mockFile);
							 
						});
						
					},
					error: function(msg){
						
					}
				});	
      
	
	  this.on("sending", function(file, xhr, formData){
						formData.append("carpet", "p"+arrDetail['idPresu']);
						formData.append("gallery_id", arrDetail['id']);	
						formData.append("ppto_id", arrDetail['idPresu']);	
                });  
	
	
     this.on("addedfile", function(file) {
		 
		file.previewElement.addEventListener("click", function() { removeFile(file); });
		});
    this.on('success', function( file, resp ){
      //console.log( file );
	  var response = jQuery.parseJSON(resp);
	  if(response.error==1){
		   this.defaultOptions.error(file, response.errorMessage);  
		  } else {
	  if( typeof reloadAfterAddDeleteImg !== 'undefined' ){location.reload();}
	  
	  
	      var ext = file.name.split('.').pop();

          $(file.previewElement).find(".img-fluid ").attr("src",  ruta+"images/"+ext+".png");
  
	       addButtons(myDropZone,file,response.id,'',response.filelink,response.filename);
		  }

    });
	  
	  
	  
	  },
	
  
};


function addButtons(myDropZone,file,id,alt,ruta,nombre){
	
	
	   var openButton = Dropzone.createElement('<a href="'+ruta+'" style="position:absolute; top:5%; right:20px; color:#949ba2"  class="btn btn-default" ><i alt="Descargar" class="fa fa-download "></i></a>');
	   
	  var title = Dropzone.createElement('<h5 style="position:absolute; top:50%; color:#949ba2">'+nombre+'</h5>');
		
	  var removeButton = Dropzone.createElement('<button  data-id="'+id+'" class="btn btn-default" ><i class="fa fa-trash-o"></i> <span class="bold">Borrar</span></button>');

	  var editButton = Dropzone.createElement('<button  data-ruta="'+ruta+'" data-id="'+id+'" data-alt="'+alt+'"  class="btn btn-default" ><i class="fa fa-paste"></i> Editar</button>');
	  
	  var selectButton = Dropzone.createElement('<button data-id="'+id+'" chk="false" class="btn btn-default selectItem"> <i class="fa fa-square-o"></i> <br> Seleccionar </button>');
	 
	  // eliminar archivo  
     removeButton.addEventListener("click", function(e) {
        e.preventDefault();
        e.stopPropagation();
		var theId=$(this).data('id');
		var theName=$(this).parent().parent().parent().find('.dz-filename-span').text();
		console.log("Borrar",theId,'name',theName);
		$('#eliminar__imagen').modal('show'); 
		$("#eliminarImagen").unbind( "click" );
		$("#eliminarImagen").bind("click", function(e) { 
		
		   $.ajax({
				type: "POST",
					url: processFile,
					data:{
						id: arrDetail['id'],
						idImg:theId,
						nameImg:theName,
						carpet:'p'+arrDetail['idPresu'],
						action: 'images_delete',
						token: tokenId
					},
					success: function(data){
						if(data==1){
						if( typeof reloadAfterAddDeleteImg !== 'undefined' ){ location.reload(); }
						myDropZone.removeFile(file);
						$('#eliminar__imagen').modal('hide'); 
						}
						
					},
					error: function(msg){
						
					}
				});			  
	
		}); // bind click
					
               
      });
	  
	  
	  
	      // botón de editar		
	 editButton.addEventListener("click", function(e) {
       
           e.preventDefault();
           e.stopPropagation();
		   var theId=$(this).data('id');
		   $('#imagenGrandota').attr('src','');
           $('#editar__imagen').modal('show'); 
		   if( $(this).data('ruta')!=''){
			    rutaimgThumb=$(this).data('ruta');
				} else {
                rutaimgThumb = $(this).parent().find('img').attr('src');
				}
				rutaimgGrande = rutaimgThumb.replace(/min\//g,'');
				$('#em_alt').val($(this).data('alt'));
				$('#em_alt').attr('prevalue',$(this).data('alt'));
				var nImg=rutaimgGrande.substr(rutaimgGrande.lastIndexOf('/') + 1); 
				var nImg= nImg.substr( 0,nImg.lastIndexOf('.')); 
				var imgExt = rutaimgGrande.substr(rutaimgGrande.lastIndexOf('.') + 1); 
				$('#em_nombre').val( nImg );
				$('#em_nombre').attr( 'prevalue', nImg );
				$('#em_nombre').attr( 'extvalue', imgExt );
				$('#em_nombre').attr( 'rutavalue', rutaimgThumb );
				$("#editarImagen").unbind( "click" );
				

				
		$("#editarImagen").bind("click", function(e) {
		
			dataO = new Object(); 
		    if( $('#em_alt').val()!=$('#em_alt').attr('prevalue') ) dataO.alt = $('#em_alt').val();
		    if(!jQuery.isEmptyObject(dataO)) {
			   
			 dataO.idImg=theId;
			 dataO.action='images_update';
			 dataO.token=tokenId;
		
		     $.ajax({
				type: "POST",
					url: processFile,
					data:dataO,
					success: function(data){
						
						var data = JSON.parse(data);
	                    if(data==1){
						alert("Modificada correctamente.");
						var url = window.location.href;
						if (url.indexOf("?") < 0 && url.indexOf("zonalMapDetail") < 0){
							window.location.href = url + "?idA="+arrDetail['id'];
							} else {
						top.location.reload();
							}
					}
					
					if(data==666){
						alert("Ya existe un archivo con ese nombre.");
					}
					
						
						
					},
					error: function(msg){
						
					}
				});	
			  
			 } // final params.length>0
	
		}); // bind click
				
			
                
         });
	  
		
	       // botón de seleccionar		
	       selectButton.addEventListener("click", function(e) {
			   
			   e.preventDefault();
               e.stopPropagation();
			   
			   if($(this).attr('chk')=="false"){
				   $(this).attr('chk',"true");
				   $(this).find('.fa-square-o').removeClass('fa-square-o').addClass('fa-check-square-o');
				   } else {
					   $(this).attr('chk',"false");
				       $(this).find('.fa-check-square-o').removeClass('fa-check-square-o').addClass('fa-square-o');
					   
					   }
			   
			   
			  })
	 
	 
	       $(file.previewElement).find('.btn-group').parent().append(title);
		   $(file.previewElement).find('.btn-group').parent().append(openButton);
		   $(file.previewElement).find('.btn-group').append(editButton);
		   $(file.previewElement).find('.btn-group').append(removeButton);
		   $(file.previewElement).find('.btn-group').append(selectButton);
		  
	
	} // end addButtons
	

	
 function checkChanged(ck){ 
	return ( ( $('#'+ck).attr('prevalue')=='1' && !$('#'+ck).is(':checked') ) ||  ( $('#'+ck).attr('prevalue')=='0' && $("#"+ck).is(':checked') )  ) ? 1 : 0;
	 
	 }