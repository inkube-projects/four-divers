// en el formulario principal se debe indicar un hidden con id adminSection y propiedad data-seoname

var processFile = "assets/inc/ajaxAdmin.inc.php";
var myDropZone;
	
  
 $ ( document ).ready(function() {
    console.log( "ready!" );
	
	
	$('#addVideoBtn').click(function(){
		cleanModalVideo();
         $('#editar__video').modal('show');
		 $('#videoAction').val('video_add'); 
	})
	
	
	 //referencia video si pega url detecto / y corto
    $( "#vd_referencia" ).change(function() {
			 var ref=$(this).val();
			 if(ref.indexOf('/')!=-1) ref=ref.split('/').pop();
			 if(ref.indexOf('=')!=-1) ref=ref.split('=').pop();
			 $(this).val(ref);	

          });
		  

	
	$('#videoBtnModal').click(function(){
		 dataO = new Object(); 
		    if( $('#vd_referencia').val()!='' ) dataO.video = $('#vd_referencia').val();
		    if(!jQuery.isEmptyObject(dataO)) {
			   
			 dataO.comment=$('#video_comment').val();
			 dataO.action=$('#videoAction').val();
			 dataO.token=tokenId;
		     dataO.galleryId = arrDetail['id'];
			 dataO.budgetId = arrDetail['idPresu'];
			 if(dataO.action=="video_update")dataO.id=$('#video_id').val();
			 
			 
		     $.ajax({
				type: "POST",
					url: processFile,
					data:dataO,
					success: function(data){
						var data = JSON.parse(data);
	                    if(data['status']==1){
						top.location.reload();
						}
						if(data==666){
						alert("Ya existe un archivo con ese nombre.");
					    }
					},
					error: function(msg){
						
					}
				});	
			  
			 } // final params.length>0
		
		
		})
		
		
		// botón de seleccionar		
	       $('.selectItem').click(function(e) {
			 
			   e.preventDefault();
               e.stopPropagation();
			   
			   if($(this).attr('chk')=="false"){
				   $(this).attr('chk',"true");
				   $(this).find('.fa-square-o').removeClass('fa-square-o').addClass('fa-check-square-o');
				   } else {
					   $(this).attr('chk',"false");
				       $(this).find('.fa-check-square-o').removeClass('fa-check-square-o').addClass('fa-square-o');
					   
					   }
			   
			  })
			  
			  
				  // eliminar archivo  
      $('.deleteItem').click(function(e) {
        e.preventDefault();
        e.stopPropagation();
		var theId=$(this).data('id');
		$('#eliminar__imagen').modal('show'); 
		$("#eliminarImagen").unbind( "click" );
		$("#eliminarImagen").bind("click", function(e) { 
		   $.ajax({
				type: "POST",
					url: processFile,
					data:{
						id: arrDetail['id'],
						idImg:theId,
						nameImg:'',
						carpet:'p'+arrDetail['idPresu'],
						action: 'images_delete',
						token: tokenId
					},
					success: function(data){
						if(data==1){
						 location.reload(true); 
						$('#eliminar__imagen').modal('hide'); 
						}
						
					},
					error: function(msg){
						
					}
				});			  
	
		}); // bind click
		
	  });
	  
	  
	  //**************EDIT
	        // botón de editar		
	  $('.editItem').click(function(e) {
           e.preventDefault();
           e.stopPropagation();
           $('#editar__video').modal('show');
		   $('#video_comment').val($(this).data('alt'));
		   $('#videoAction').val('video_update');	
           $( "#vd_referencia" ).val($(this).data('nombre'));
		   $('#video_id').val($(this).data('id'));
				
	  });
	  
	  
	  
	  
	  
	  
	
	
});  //ready






function cleanModalVideo(){
	$("#vd_referencia, #video_comment, #video_id").val('');
	}



	
 function checkChanged(ck){ 
	return ( ( $('#'+ck).attr('prevalue')=='1' && !$('#'+ck).is(':checked') ) ||  ( $('#'+ck).attr('prevalue')=='0' && $("#"+ck).is(':checked') )  ) ? 1 : 0;
	 
	 }