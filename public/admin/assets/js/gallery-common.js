 
   var deleteItemsIds = [];
   var deleteItemsNames = [];
   
 
 
 $(document).ready(function () {
		
		$('body').addClass('pace-done'); 
		
		$('#deleteSelection').click( collectItems ); // 
		
		// modal btn deleteSelection
		$('#eliminarSeleccion').click( deleteSelected )
			
		$('.editGallery').click( modalEditGallery );
		
		$('#modGaleriaBtn').click( updateGallery );
		// show and fill modal 
		$('#sendEmailBtn').click( modalShowSendEmail );
		// modal send email 
		$('#modalSendEmailBtn').click( modalSendEmail );
		

    }); // /ready
	

     /*
	 * Show modal email
	 */
	 function modalShowSendEmail(){
		 
		 var asunto="Galería: "+arrDetail['nombre']+"; Ppto:  "+budgetDetail['referencia'];
		 $('#emailSubject').val(asunto);
		 var email=(budgetDetail['emailAdministrador'].length>0)?budgetDetail['emailAdministrador']:budgetDetail['emailCliente']
		  $('#emailEmail').val(email);
		  $('#emailGalleryLink').val(arrDetail['galleryLink']);
		  
		  $('#sendEmailModal').modal('show');
		 
		 }
		 
		 
	/*
	 *  send email
	 */	 
	function modalSendEmail(){
		if($('#emailSubject').val()=='') { toastr.clear(); toastr.warning('Indique un asunto'); return false; }
		if(!validateEmail($('#emailEmail').val())) { toastr.clear(); toastr.warning('Indique un email válido'); return false; }
		
		data=$('#sendGalleryEmailForm').serialize()+"&action=gallery_email&gallery_id="+arrDetail['id']+"&ppto_ref="+budgetDetail['referencia']+"&name="+arrDetail['nombre']+"&description="+arrDetail['descripcion']+"&token="+tokenId;
		
		 $.ajax({
                url: processFile,
				dataType: "json",
                data:data,
                type: "POST",
                success: function (res){ 
				     if(res.status){ 
					 	toastr.clear(); toastr.success("Email enviado.");
						$('#sendEmailModal').modal('hide');
						}
					 else {toastr.clear(); toastr.warning(res.error);}
				
				 },
                error: onError
            })
		
		
		 }
	
    

	/*
	* Collect items for delete
	*/
	function collectItems(){
		 deleteItemsIds = [];
		 deleteItemsNames = [];
			var itemsSelected = $('.selectItem[chk="true"]');
			if(itemsSelected.length==0){
				toastr.clear(); toastr.warning("Ningún imagen/archivo seleccionad");
				} else {
					itemsSelected.each(function () {
                      deleteItemsIds.push( $(this).data("id") );
					  deleteItemsNames.push($(this).parent().parent().parent().find('.dz-filename-span').text());
                     });
					$('#eliminar__seleccion').modal('show');
					
			}
			
		}	
	/*
	* Delete Selected Items
	*/
	function deleteSelected(){
		 $.ajax({
				type: "POST",
					url: processFile,
					data:{
						id: arrDetail['id'],
						idsImg:deleteItemsIds,
						namesImg:deleteItemsNames,
						carpet:'p'+arrDetail['idPresu'],
						action: 'images_delete_multiple',
						token: tokenId
					},
					success: function(data){
						if(data==1)location.reload(true); 
						
					},
					error: function(msg){
						
					}
				});			  
		}
	
	
	
	function  modalEditGallery(){
		$('#modGalleryName').val(arrDetail['nombre']);
		$('#modGalleryDes').val(arrDetail['descripcion']);
		$('#modGallleryModal').modal('show');
		}
		
		
	function updateGallery(){
		
		console.log(processFile);
		
		var galleryName=$('#modGalleryName').val();
		var galleryDescription=$('#modGalleryDes').val();
		if( galleryName==arrDetail['nombre'] && galleryDescription==arrDetail['descripcion']  ) return false;
		if(galleryName.length<1){toastr.clear(); toastr.warning('Indique un nombre para la galería'); return false; }
	 
	    $.ajax({
                url: processFile,
				dataType: "json",
                data:"&action=gallery_update&id="+arrDetail['id']+"&ppto_id="+arrDetail['idPresu']+"&name="+galleryName+"&description="+galleryDescription+"&token="+tokenId,
                type: "POST",
                success: function (res){ 
				     if(res.status){ 
					 	arrDetail['nombre']=$('#modGalleryName').val();
						$('#gName').text($('#modGalleryName').val());
						arrDetail['descripcion']=$('#modGalleryDes').val();
						$('#gDescription').text($('#modGalleryDes').val());
						$('#modGallleryModal').modal('hide');
						}
					 else {toastr.clear(); toastr.warning(res.error);}
				
				 },
                error: onError
            })
		
		}
	
onError = function (){ toastr.clear(); toastr.warning('Error');  }	

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}// JavaScript Document