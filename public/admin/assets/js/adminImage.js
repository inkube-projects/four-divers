// en el formulario principal se debe indicar un hidden con id adminSection y propiedad data-seoname

var myDropZone;


if( !$( "#webSel" ).length ) $('#porta_webSelImage').css('display','none');


  //uploader
  Dropzone.options.uploadWidget = {
  	paramName: 'file',
  	maxFilesize: 4,
  	maxFiles: 15,
  	thumbnailWidth:200,
  	thumbnailHeight:200,
  	clickable: "#addImgBtn",
  	dictDefaultMessage: '',
  	headers: {
  		'x-csrf-token': document.querySelectorAll('meta[name=csrf-token]')[0].getAttributeNode('content').value,
  	},
  	acceptedFiles: 'image/*',
  	init: function() {
	   //obtengo imágenes previas
	   myDropZone = this;
	   $.ajax({
	   	type: "POST",
	   	url: processFile,
	   	data:{
	   		id: arrDetail['id'],
	   		sectionName:$('#adminSection').data('seoname'),
	   		sectionId:$('#adminSection').val(),
	   		action: 'images_list',
	   		token: $('#formToken').val()
	   	},
	   	success: function(data){

	   		var data = JSON.parse(data);

	   		$.each(data, function(key,value){

	   			var mockFile = { name: value.name, size: value.size};
	   			myDropZone.options.addedfile.call(myDropZone, mockFile);
	   			var seoName=$('#adminSection').data('seoname');
	   			var carpetId="id-"+arrDetail['id'];
	   			myDropZone.options.thumbnail.call(myDropZone, mockFile, mainUrl+"public/assets/images/"+seoName+"/"+carpetId+"/min/"+value.name);

	   			addButtons(myDropZone, mockFile, value.id, value.alt,'',value.portada,value.banner,value.colorId,value.finishId,value.webId);
	   			myDropZone.emit("complete", mockFile);

	   		});

	   	},
	   	error: function(msg){
	   		console.log(msg);
	   	}
	   });	


	   this.on("sending", function(file, xhr, formData){
	   	formData.append("id", arrDetail['id']);
	   	formData.append("carpet", $('#adminSection').data('seoname'));
	   	formData.append("sectionId", $('#adminSection').val());
	   	if( $( "#webSel" ).length && $( "#webSel" ).val()>0 ){
	   		formData.append("webId", $('#webSel').val());
	   	}
	   });
	   this.on("addedfile", function(file) {

	   	file.previewElement.addEventListener("click", function() { removeFile(file); });
	   });
	   this.on('success', function( file, resp ){
      //console.log( file );
      var response = jQuery.parseJSON(resp);
      if(response.error==1){
      	this.defaultOptions.error(file, response.errorMessage);  
      } else {
      	if( typeof reloadAfterAddDeleteImg !== 'undefined' ){location.reload();}
      	addButtons(myDropZone,file,response.id,'',response.ruta,0,0,0,response.webId)
      }

    });
	   this.on('thumbnail', function(file) {
	   	if ( file.width < 130 || file.height < 130 ) {
	   		file.rejectDimensions();
	   	}
	   	else {
	   		file.acceptDimensions();
	   	}
	   });
	 },
	 accept: function(file, done) {
	 	file.acceptDimensions = done;
	 	file.rejectDimensions = function() {
	 		done('The image must be at least 130 x 130px')
	 	};
	 	
	 }
	};



	function addButtons(myDropZone,file,id,alt,ruta,portada,banner,color_id,finish_id,web_id){
		var removeButton = Dropzone.createElement('<button  data-id="'+id+'"  style="width:33%;" class="btn btn-danger" ><i class="fa fa-trash fa-lg" aria-hidden="true"></i></button>');
		var editButton = Dropzone.createElement('<button data-color_id="'+color_id+'" data-finish_id="'+finish_id+'"  data-banner="'+banner+'" data-portada="'+portada+'" data-web_id="'+web_id+'"  data-ruta="'+ruta+'" data-id="'+id+'" data-alt="'+alt+'" style="position:absolute; right:33%; width:33%;" class="btn  btn-warning" ><i class="fa fa-edit fa-lg" aria-hidden="true"></i></button>');
		var rotateButton = Dropzone.createElement('<button style="position:absolute; right:0px; width:33%;"   data-id="'+id+'" class="btn btn-default" ><i class="fa fa-rotate-left"></i></button>');
		
		// eliminar imagen  
		removeButton.addEventListener("click", function(e) {
			e.preventDefault();
			e.stopPropagation();
			var imgId=$(this).data('id');
			$('#eliminar__imagen').modal('show'); 
			$("#eliminarImagen").unbind( "click" );
			$("#eliminarImagen").bind("click", function(e) { 

				$.ajax({
					type: "POST",
					url: processFile,
					data:{
						ownerId: arrDetail['id'],
						id:imgId,
						action: 'images_delete',
						token: $('#formToken').val(),
						sectionName: $('#adminSection').data('seoname')
					},
					success: function(data){
						if(data==1){
							if( typeof reloadAfterAddDeleteImg !== 'undefined' ){ location.reload(); }
							myDropZone.removeFile(file);
							$('#eliminar__imagen').modal('hide'); 
						}
						
					},
					error: function(msg){
						
					}
				});			  

		}); // bind click

		});


		// botón de rotar		
   rotateButton.addEventListener("click", function(e) {
   	e.preventDefault();
   	e.stopPropagation();

   	console.log("rotate");
   	var img = $(this).parents('.dz-preview').find('img');
   	var imgName = img.attr('src');
   	dataO = new Object(); 
   	dataO.id=$(this).data('id');
   	dataO.ownerId=arrDetail['id'];
   	dataO.sectionName=$('#adminSection').data('seoname');
		dataO.action='images_rotate';
		dataO.token=tokenId;
   	$.ajax({
							type: "POST",
							url: processFile,
							data:dataO,
							success: function(data){
								var data = JSON.parse(data);
								if(data==1){
									d = new Date();
   							  img.attr("src", imgName+"?"+d.getTime());
								}
							},
							error: function(msg){

							}
						});	

   })
		
	 // botón de editar		
	 editButton.addEventListener("click", function(e) {
	 	fillEditImageSelects($(this).data('color_id'),$(this).data('finish_id'));
	 	var rutaimgGrande;
	 	var rutaimgThumb;
	 	e.preventDefault();
	 	e.stopPropagation();
	 	var theId=$(this).data('id');
	 	$('#imagenGrandota').attr('src','');
	 	$('#editar__imagen').modal('show'); 
	 	if( $(this).data('ruta')!=''){
	 		rutaimgThumb=$(this).data('ruta');
	 	} else {
	 		rutaimgThumb = $(this).parent().find('img').attr('src');
	 	}
	 	rutaimgGrande = rutaimgThumb.replace(/min\//g,'');
				//alert(rutaimgGrande);
				$('#imagenGrandota').attr('src',rutaimgGrande);
				$('#em_alt').val($(this).data('alt'));
				$('#em_alt').attr('prevalue',$(this).data('alt'));
				var nImg=rutaimgGrande.substr(rutaimgGrande.lastIndexOf('/') + 1); 
				var nImg= nImg.substr( 0,nImg.lastIndexOf('.')); 
				var imgExt = rutaimgGrande.substr(rutaimgGrande.lastIndexOf('.') + 1); 
				$('#em_portada').attr( 'prevalue', $(this).data('portada') ); 
				$("#em_portada").prop('checked',$(this).data('portada'));
				$('#em_logoEmail').attr( 'prevalue', $(this).data('logoemail') ); 
				$("#em_logoEmail").prop('checked',$(this).data('logoemail'));
				$('#em_banner').attr( 'prevalue', $(this).data('banner') ); 
				$("#em_banner").prop('checked',$(this).data('banner'));
				$('#em_nombre').val( nImg );
				$('#em_nombre').attr( 'prevalue', nImg );
				$('#em_nombre').attr( 'extvalue', imgExt );
				$('#em_nombre').attr( 'rutavalue', rutaimgThumb );
				$("#editarImagen").unbind( "click" );
				
				if( $( "#webSel" ).length ){
					$('#webSelImage').val( $(this).data('web_id') );
					$('#webSelImage').attr( 'prevalue',$(this).data('webid') );
				}
				
				$("#editarImagen").bind("click", function(e) {

					dataO = new Object(); 

					if( $('#em_alt').val()!=$('#em_alt').attr('prevalue') ) dataO.alt = $('#em_alt').val();
					if( $( "#webSel" ).length && $('#webSelImage').val()!=$('#webSelImage').attr( 'prevalue') ) dataO.webId=$('#webSelImage').val();

		// si alguno de los tres checks ha sido modificado
		if( checkChanged('em_portada') ||  checkChanged('em_banner') ||  checkChanged('em_logoEmail')  ) {
			
			var valPortada=( $("#em_portada").is(':checked') ) ? '1' : '0';
			var valBanner=( $("#em_banner").is(':checked') ) ? '1' : '0';
			
			dataO.banner=valBanner;
			dataO.portada=valPortada;
			dataO.id=arrDetail['id'];

		}

		if( $('#em_nombre').val()!=$('#em_nombre').attr('prevalue') ) {

			var regex = /^[a-zA-Z0-9_-]+$/;
			if( !regex.test($('#em_nombre').val()) ) {
				alert("El nombre posee caracteres no permitidos. No emplee ñ o acentos.");
				return false;
			}

			dataO.nombre=$('#em_nombre').val();
			dataO.extnombre=$('#em_nombre').attr('extvalue');
			dataO.nombreprevio=$('#em_nombre').attr('prevalue');
			dataO.rutanombre=$('#em_nombre').attr('rutavalue').substr(0,$('#em_nombre').attr('rutavalue').lastIndexOf('/') ); 
			dataO.rutanombre=dataO.rutanombre.replace(/https?:\/\/[^\/]+/i, "");

		}
		
		if(!jQuery.isEmptyObject(dataO)) {

			dataO.idImg=theId;
			dataO.action='images_update';
			dataO.token=$('#formToken').val();
			if ( $('#imageColor').val()>=0 ) dataO.colorId=$('#imageColor').val();
			if ( $('#imageFinish').val()>=0 ) dataO.finishId=$('#imageFinish').val();

			$.ajax({
				type: "POST",
				url: processFile,
				data:dataO,
				success: function(data){

					var data = JSON.parse(data);
					if(data==1){
						alert("Modificada correctamente.");
						var url = window.location.href;
						if (url.indexOf("?") < 0 && url.indexOf("zonalMapDetail") < 0){
							window.location.href = url + "?idA="+arrDetail['id'];
						} else {
							top.location.reload();
						}
					}
					
					if(data==666){
						alert("Ya existe una imagen con ese nombre.");
					}
					


				},
				error: function(msg){

				}
			});	

			 } // final params.length>0

		}); // bind click
				


			});


	 file.previewElement.appendChild(removeButton);
	 file.previewElement.appendChild(editButton);
	 file.previewElement.appendChild(rotateButton);


	}
	
	
	function fillEditImageSelects(colorId,finishId){
		var colors = $('#colors option:selected');
		$('#imageColor, #imageFinish').find('option').remove().end();
		if(colors.length>0){
			$('#imageColor').append($("<option />").val('0').text('Seleccione'));
			$(colors).each(function(index, color){
				var selected = (colorId==color.value) ? 'selected' : '';
				$('#imageColor').append($("<option "+selected+" />").val(color.value).text(color.text));
			});
		}
		
		var finishes = $('#finishes option:selected');
		if(finishes.length>0){
			$('#imageFinish').append($("<option />").val('0').text('Seleccione'));
			$(finishes).each(function(index, finish){
				var selected = (finishId==finish.value) ? 'selected' : '';
				$('#imageFinish').append($("<option "+selected+" />").val(finish.value).text(finish.text));
			});
		}


	}



	
	function checkChanged(ck){ 
		return ( ( $('#'+ck).attr('prevalue')=='1' && !$('#'+ck).is(':checked') ) ||  ( $('#'+ck).attr('prevalue')=='0' && $("#"+ck).is(':checked') )  ) ? 1 : 0;

	}