// JavaScript Document
$(document).ready(function () {

        $('body').addClass('pace-done'); 

       
        //NAVIGATION
        $('#reservationsSectionSideMenu').trigger('click');
        $('#reservationsSectionSideMenu').parent().addClass('active');
        $('#liReservationsSideBar').addClass('active');

        $('.select2_demo_1').select2();

      
        //CLIENT SELECT
        $('.client-select').select2({
          ajax: {
            type:"POST",
            delay: 250,
            url: processFile,
            dataType: 'json',
            data: function (params) {
                    return {
                       q: params.term, // search term
                       page: params.page,
                       action: 'client-search',
                       token: tokenId
                       };
            }
     
            },
            placeholder: 'Buscar cliente',
            minimumInputLength: 3,
        });


        $('#createReservationBtn').click(function(){
             console.log($('.client-select').val());
             if(!$('.client-select').val())sweetAlert( "Reservas","Debe de seleccionar un cliente", "error");
        })
        

    }) // end ready




