// JavaScript Document
$(document).ready(function () {

      // DATETIMEPICKERS
      dateFrom='';
      dateTo='';
      asignPeriod = false;
      
      createPickers();

        //BUTTONS PERIOD
        // must destroy datePickers and recreate (clear only and pick again generate error) 
        $('#allReservationsBtn').click(function(){
            $('#datetimepicker1, #datetimepicker2').datetimepicker('clear');
            $('#datetimepicker1, #datetimepicker2').datetimepicker('destroy');
            createPickers();
            dateFrom='';
            dateTo='';
            table.ajax.reload();
        })

        $('#todayReservationsBtn').click(function(){
            asignPeriod=true;
            $('#datetimepicker1').datetimepicker('date',moment());
            $('#datetimepicker2').datetimepicker('date',moment().add( 1, 'seconds'));
        })

         $('#weekReservationsBtn').click(function(){
            asignPeriod=true;
            $('#datetimepicker1').datetimepicker('date',moment());
            $('#datetimepicker2').datetimepicker('date',moment().add( 7, 'days'));
        })

        $("#datetimepicker1").on("change.datetimepicker", function (e) {
            if(e.date!=false){
            $('#datetimepicker2').datetimepicker('minDate', e.date.add( 1, 'seconds'));
            dateFrom=e.date.format('YYYY-MM-DD');
            if(asignPeriod==false) table.ajax.reload();
            } else dateFrom='';
           
        });
        $("#datetimepicker2").on("change.datetimepicker", function (e) {
           if(e.date!=false){
            $('#datetimepicker1').datetimepicker('maxDate', e.date);
            dateTo=e.date.format('YYYY-MM-DD');
            } else dateTo='';
            asignPeriod=false;
            table.ajax.reload();
        });



        $('body').addClass('pace-done'); 

        // Datatables   
        table = $('#tableReservations').DataTable({
         dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
         "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
         "processing": true,
         "searching": true,
         "serverSide": true,
         "order": [[ 2, "asc" ]],
         "pageLength": 50,
         "buttons": [
         {extend: 'copy', className: 'btn-sm'},
         {extend: 'csv', title: 'Reservas-Four-Divers', className: 'btn-sm'},
         {extend: 'pdf', title: 'Reservas-Four-Divers', className: 'btn-sm'},
         {extend: 'print', className: 'btn-sm'}
         ],
         "ajax": {
          "type": "POST",
          "url": processFile,
          "data": function ( d ) {
            d.action = "list_reservations";
            d.isJs=1;
            d.token=tokenId;
            d.from=dateFrom;
            d.to=dateTo;
          }
        },
        "columnDefs": [ // oculto columna Id
        {
          "targets": [ 0 ],
          "visible": true,
          "searchable": true
        }
        ],
        "columns": [
        { "data": "DT_RowId" },
        { "data": "date" },
        { "data": "client" },
        { "data": "type" },
        { "data": "status" }
        ],
        "drawCallback": function(settings) {
          //afterTableDraw();
        }


      } );


        $('#tableReservations tbody').on('click', 'tr', function () {
          var data = table.row( this ).data();
          top.location.href=mainUrl+'DETALLE-RESERVA/'+data['DT_RowId']
        });

        //NAVIGATION
        $('#reservationsSectionSideMenu').trigger('click');
        $('#reservationsSectionSideMenu').parent().addClass('active');
        $('#liReservationsSideBar').addClass('active');

      


        

    }) // end ready




function createPickers(){

$('#datetimepicker1, #datetimepicker2').datetimepicker({
            locale: 'es',
            timePicker: false,
            format: 'DD-MM-YYYY',
            useCurrent: false,
            todayHighlight: false
        });
        

}