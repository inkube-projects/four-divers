$(document).ready(function () {

/* COMBOS CATEGORY AND PROVIDER  */

$('#productsFamilies').change(function(event) {
  if(asignedDBValues){
    console.log('productFamily',$(this).val());
   // loadFamilyCategories()
   loadCategoriesAndProviders($(this).val());


   //$('#productsCategories option').remove();
   //var newOption = new Option("Ejemplo de adición opción", 1, false, false);
   //$('#productsCategories').append(newOption).trigger('change');

  }
});

  
}); // end ready


function loadCategoriesAndProviders(family_id,setValues){
   //clean categories and providers 
   $('#productsCategories option, #productsProviders option').remove();

   // add select first option
   var newOption = new Option("Seleccione", '', false, false);
   $('#productsCategories, #productsProviders').append(newOption);

   $.ajax({
      type:"POST",
      dataType:"html",
      url: processFile,
      data:"&action=product-categories-providers&family_id="+family_id+"&token="+tokenId,
      success:function(msg){
            var res=JSON.parse(msg);
            //fill categories
            res['categories'].forEach(function(cat) {
               var newOption = new Option(cat['name'], cat['id'], false, false);
               $('#productsCategories').append(newOption);
            });
            //fill providers
            res['providers'].forEach(function(cat) {
               var newOption = new Option(cat['name'], cat['id'], false, false);
               $('#productsProviders').append(newOption);
            });
             
            if(setValues==true){ // updating
               $('#productsProviders').val(arrDetail['provider_id']);
               $('#productsCategories').val(arrDetail['category_id']);
            }

            $('#productsProviders').trigger('change');
            $('#productsCategories').trigger('change');

          } // final success
        });  


}

