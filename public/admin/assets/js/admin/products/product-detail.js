$(document).ready(function () {
    
    data={};

   if(arrDetail['action']=="add") { 
      $('#addImgBtn').prop( "disabled", true );
      }

  productLanguageId=0;
  var btn_Fixed = $('.btnFixed');
  asignedDBValues=(arrDetail['action']=='update')?false:true;

  //WINDOW SCROLL
  $(window).scroll(function () {
   if ( $(this).scrollTop() > 250 )  btn_Fixed.addClass("btnFixed-jq").fadeIn("slow");
   if( ($(window).scrollTop() + $(window).height() == $(document).height()) || $(this).scrollTop() < 250 ) {
    btn_Fixed.removeClass("btnFixed-jq").fadeOut("slow");
  }
  });

  
  // SHOW CATEGORY SPECS
  $('#productsCategories').change(function(){
    $( "#specs-container" ).empty();
    if($(this).val()>0){
    $( "#specs-container" ).load( adminUrl+"include/product-specs/specs-cat_"+$(this).val()+".php" , function() {
        //SET VALUES
        setSpecsValues();
    });
  }
  })

 
  //SAVE USER
  $('.save-product-btn').click(function(e){
    e.preventDefault();
    productDB();
  })

  //DELETE USER
  $('.delete-product-btn').click(function(e){
    e.preventDefault();
    $('#eliminar__entrada').modal('show');
  })
  $('#eliminarEntrada').click(function() { deleteDB(); });

  //SIDEBAR
  $('#productsSectionSideMenu').trigger('click');
  $('#productsSectionSideMenu').parent().addClass('active');
  $('#liProductsListSideBar').addClass('active');

  //SWITCH
  var elem1 = document.querySelector('.js-switch1');
  var elem1 = new Switchery(elem1, { size: 'small'});

  var elem2 = document.querySelector('.js-switch2');
  var elem2 = new Switchery(elem2, { color: '#d64842', size: 'small'});




  // Detalle de usuario asignar valores mediante JS
  if(arrDetail['action']=='update'){setJsValues();}

  // Deben de instanciarse tras haber asignado valores de bd, de lo contrario no muestran el valor asignado
  $(".select2_demo_1").select2();

  //Redactor
  //$('#descripcion').redactor('code.get')
  $('#productDescription, #productRequirements, #productIncluded, #productObjectives').redactor({   
      minHeight: 150, // pixels
      lang: 'es',
      imageUpload: '/',
      imageManagerJson: processFile+"?action=images_list&id="+arrDetail['id']+"&sectionName="+$('#adminSection').data('seoname')+"&sectionId="+$('#adminSection').val()+"&token="+$('#formToken').val()+"&origen=redactor",
              plugins: ['table','imagemanager','fontcolor','fullscreen']
        }
    );

})  // end ready

function setJsValues(){
      console.log("set js values");
      //setSeletcMultiValues(selectId,arrdetaIl['']);
       //cursos
      $('#productsFamilies').val(arrDetail['family_id']);
      loadCategoriesAndProviders(arrDetail['family_id'],true);
      asignedDBValues=true;
    }

    function setSpecsValues(){
      
      specsArray=$.parseJSON(arrDetail['specs']);
      console.log(specsArray);
      jQuery.each( specsArray, function( field, val ) {
        var $myDiv = $('#specs-'+field);
        if($myDiv.length){
          var type = $myDiv.attr('type');
          if(type=="text" || type==undefined)  $myDiv.val(val);
          if(type=="checkbox")  $myDiv.prop('checked', true);
        }

      });


       //SPECS SWITCHS
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch-specs'));
        elems.forEach(function(html) {
        var switchery = new Switchery(html, { size: 'small'});
        });

      //SPECS SELECTS
      $(".js-specs-select2").select2();
    }

  

    function setSeletcMultiValues(sel,data){

      if(data!=null && data!=""){
        var data = JSON.parse(data);
        data.forEach(function (value, index) {
          console.log(value);
          $('#'+sel+' option[value="'+value+'"]').attr('selected', 'selected');

        });
      }


    }

    //UPDATE/SAVE PRODUCT
    function productDB() {
      if( $('#productsFamilies').val()=="" || $('#productsCategories').val()=="" || $('#productsProviders').val()==""  ) { 
        sweetAlert( "Catálogo","indique una familia, categoría y proveedor", "error");
         $('html,body').animate({ scrollTop:$('#productsFamilies').offset().top-100 }, 'slow');
        return false;
      }

      if( $('#productReference').val().length<1 ) { 
        sweetAlert( "Catálogo","indique una referencia", "error");
        return false;
      }

      data=$('#detalleProducto').serializeArray().reduce(function(m,o){ m[o.name] = o.value; return m;}, {}) 
      data['id']=arrDetail['id'];
      getSpecsValues();
      //return false;
     
      $.ajax({
        type:"POST",
        dataType:"html",
        url: processFile,
        data:data,
        success:function(msg){
          var res=JSON.parse(msg);
          if( typeof res['error'] != 'undefined'){
            if(res['error']=='duplicated-reference'){
              sweetAlert( "Catálogo","Referencia ya en uso, emplee otra", "error");
              return false;
            }

          }  
       // en caso de adición correcta recargamos, abreviamos
       else if(   $('#action').val()=="product-add" && res['id']>0 ){
              arrDetail['action']="update";
              arrDetail['id']=res['id'];
              $("#languageSel option:disabled").removeAttr('disabled');
              $("#languageSel").select2();
              $('#eliminarBtn').css('display','inline');
              $('#addImgBtn').prop( "disabled", false );
              $('#action').val('product-update');
         //top.location.href=mainUrl+"DETALLE-PRODUCTO/"+res['id'];
       } 

       sweetAlert( "Catálogo","", "success");

       } // final success
     });  

    };

  //getSpecsValues
  function getSpecsValues(){
    var specs = {};
    $( ".product-specs" ).each(function( index ) {
       //console.log( index + ": " + $( this ).text() );
       var type = $(this).attr('type');

          console.log(type,$(this).val());
          // type==undefined  = selects
          if( ( type=="text" || type==undefined ) && $(this).val()!='')  specs[$(this).attr('name')]=$(this).val();
          if(type=="checkbox" && $(this).prop('checked')) {
             specs[$(this).attr('name')]="1"; 
             }
          delete data[$(this).attr('name')];

    });
    
    data["specs"]=JSON.stringify(specs);
   
    console.log(data);


  }


  // SELECTOR DE IDIOMA
  $( "#languageSel" ).change(function() {
    languageSel=$(this).val();
    $.ajax({
      type:"POST",
      dataType:"html",
      url: processFile,
      data:"&action=product-language&productId="+arrDetail['id']+"&languageId="+languageSel+"&token="+tokenId,
      success:function(msg){
        var res=JSON.parse(msg);
        if(res.resultado){
          fillData(res);
          arrDetail['action']="update";
          $('#action').val('product-update');
        } else {
          $('#productName, #metaTitle, #metaKeywords, #metaDescription').val('');
          $('#productDescription, #productRequirements, #productIncluded, #productObjectives').redactor('code.set','');
          arrDetail['action']="add";
          $('#action').val('product-add');
        }
        
      } // final success
   });  
    
    
  });

  function fillData(res){
      $('#productName').val(res.data['name']);
      $('#metaTitle').val(res.data['metaTitle']);
      $('#metaKeywords').val(res.data['metaKeywords']);
      $('#metaDescription').val(res.data['metaDescription']);
      $('#productDescription').redactor('code.set',res.data['description']);
      $('#productRequirements').redactor('code.set',res.data['requirements']);
      $('#productIncluded').redactor('code.set',res.data['included']);
      $('#productObjectives').redactor('code.set',res.data['objectives']);
    }

  //DELETE USER
  function deleteDB() {

    $.ajax({
      type:"POST",
      dataType:"html",
      url: processFile,
      data:"&action=product-delete&id="+$('#productId').val()+"&token="+tokenId,
      success:function(msg){
            var res=JSON.parse(msg);
            if(res['resultado']==-1){
              sweetAlert( "Cliente","El producto no ha podido ser eliminado.", "error");
              $('#eliminar__entrada').modal('hide');
            }
            else top.location=mainUrl+"LISTADO-PRODUCTOS";
          } // final success
        });  

  };
