// JavaScript Document
$(document).ready(function () {


  $('body').addClass('pace-done'); 

        // Datatables   
        table = $('#tableProducts').DataTable({
         dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
         "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
         "processing": true,
         "searching": true,
         "serverSide": true,
         "order": [[ 2, "asc" ]],
         "pageLength": 50,
         "buttons": [
         {extend: 'copy', className: 'btn-sm'},
         {extend: 'csv', title: 'Catálogo-Four-Divers', className: 'btn-sm'},
         {extend: 'pdf', title: 'Catálogo-Four-Divers', className: 'btn-sm'},
         {extend: 'print', className: 'btn-sm'}
         ],
         "ajax": {
          "type": "POST",
          "url": processFile,
          "data": function ( d ) {
            d.action = "list_products";
            d.isJs=1;
            d.token=tokenId;
            d.family_id=$("#productsFamilies").val();
          }
        },
        "columnDefs": [ // oculto columna Id
        {
          "targets": [ 0 ],
          "visible": false,
          "searchable": false
        },
        {
          "targets": [ 5 ],
          "searchable": false,
          "orderable": false
        }
        ],
        "columns": [
        { "data": "DT_RowId" },
        { "data": "reference" },
        { "data": "family" },
        { "data": "category" },
        { "data": "name" },
        { "data": "price" },
        { "data": "stock" },
        { "data": "status" }
        ],
        "drawCallback": function(settings) {
          //afterTableDraw();
        }


      } );


        $('#tableProducts tbody').on('click', 'tr', function () {
          var data = table.row( this ).data();
          top.location.href=mainUrl+'DETALLE-PRODUCTO/'+data['DT_RowId']
        });


        $('#productsSectionSideMenu').trigger('click');
        $('#productsSectionSideMenu').parent().addClass('active');
        $('#liProductsListSideBar').addClass('active');

        $(".select2_demo_1").select2();

        $('#productsFamilies').change(function(){
          table.ajax.reload();
        })

    }) // end ready




