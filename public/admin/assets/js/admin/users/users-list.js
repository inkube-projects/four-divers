// JavaScript Document
$(document).ready(function () {


        $('body').addClass('pace-done'); 

        //remove not used columns
        $('.only-ship-out').remove();


        // Datatables   
        table = $('#usersTable').DataTable({
         dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
         "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
         "processing": true,
         "searching": true,
         "serverSide": true,
         "order": [[ 2, "asc" ]],
         "pageLength": 50,
         "buttons": [
         {extend: 'copy', className: 'btn-sm'},
         {extend: 'csv', title: 'Clientes-Four-Divers', className: 'btn-sm'},
         {extend: 'pdf', title: 'Clientes-Four-Divers', className: 'btn-sm'},
         {extend: 'print', className: 'btn-sm'}
         ],
         "ajax": {
          "type": "POST",
          "url": processFile,
          "data": function ( d ) {
            d.action = "list_clients";
            d.isJs=1;
            d.token=tokenId;
            d.type=userType;
          }
        },
        "columnDefs": [ // oculto columna Id
        {
          "targets": [ 0 ],
          "visible": false,
          "searchable": false
        },
        {
          "targets": [ 5 ],
          "searchable": false,
          "orderable": false
        }
        ],
        "columns": [
        { "data": "DT_RowId" },
        { "data": "date" },
        { "data": "name" },
        { "data": "mobile_phone" },
        { "data": "email" },
        { "data": "open_sizes" },
        { "data": "status" }
        ],
        "drawCallback": function(settings) {

          afterTableDraw();

        }


      } );

        if(userType==1){
          $('#usersSectionSideMenu').trigger('click');
          $('#usersSectionSideMenu').parent().addClass('active');
          $('#liUsersList').addClass('active');
        } else {
          $('#companyTeamSideMenu').trigger('click');
          $('#companyTeamSideMenu').parent().addClass('active');
          $('#liCompanyHumanTeamSideBar').addClass('active');
        }

    }) // end ready


function afterTableDraw(){

 $('.open-modal-tallas').click(function () {
  var data = table.row( $(this).parents('tr') ).data();
  setSizesData( data, $(this).data('sizes'));
  $('#modal-equipment-sizes-name').html(data['name']);

  $('#modalTallas').modal('show');

} );


}


function setSizesData( data, sizes){
 console.log( sizes );
 $('#modal-equipment-sizes-tbody tr').each(function(){
                //clean all text
                $(this).children('td').not('td:eq(0)').text('');
                //asign value
                $(this).find('td:eq(1)').text( sizes[$(this).data('equipment-id')] );
              })

}