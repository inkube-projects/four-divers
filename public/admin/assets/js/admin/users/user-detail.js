
section = (userDetail['type']==1) ? ['Cliente'] : ['Equipo humano'];

$(document).ready(function () {

  var btn_Fixed = $('.btnFixed');
 

  //WINDOW SCROLL
  $(window).scroll(function () {
   if ( $(this).scrollTop() > 250 )  btn_Fixed.addClass("btnFixed-jq").fadeIn("slow");
   if( ($(window).scrollTop() + $(window).height() == $(document).height()) || $(this).scrollTop() < 250 ) {
    btn_Fixed.removeClass("btnFixed-jq").fadeOut("slow");
  }


});

  Dropzone.autoDiscover = false;
  //File insurance upload
  var insuranceDropzone = new Dropzone("#file-selector2", { 
    url: adminUrl+"archivos/upload_file.php", 
    acceptedFiles: ".zip,.xls,.xlsx,.rar,.pdf,.doc,.docx",
    maxFilesize: 5, // MB
    paramName: 'file',
    init: function () {
      var _this = this;
      this.on("sending", function(file, xhr, data) {
        swal({ title: "Seguro", text: "subiendo...", showConfirmButton: false });
        data.append("previousFile", $('#file-selector2').data('filename'));
        data.append("filetype", "insuranceuploadtype");
        data.append("id", userDetail['id']);
      });
      this.on("error", function(file, message) { 
        alert(message);
        this.removeFile(file); 
      });
      this.on("success", function (file, data) {
        swal.close();
        data=JSON.parse(data);
        $('#file-selector2').data('filename',data['name']);
        var paths = $('#see-file-info2').attr('href').split('/');
        paths[ paths.length-1 ] = data['name']; // new value
        $('#see-file-info2').attr('href',paths.join('/'));
        $('#container-see-file-info2').css('display','block');
      });
      }, // end init
      uploadMultiple: false,
    });


    //File medical upload
  var insuranceDropzone = new Dropzone("#file-selector3", { 
    url: adminUrl+"archivos/upload_file.php", 
    acceptedFiles: ".zip,.xls,.xlsx,.rar,.pdf,.doc,.docx",
    maxFilesize: 5, // MB
    paramName: 'file',
    init: function () {
      var _this = this;
      this.on("sending", function(file, xhr, data) {
        swal({ title: "Certificado médico", text: "subiendo...", showConfirmButton: false });
        data.append("previousFile", $('#file-selector3').data('filename'));
        data.append("filetype", "medicaluploadtype");
        data.append("id", userDetail['id']);
      });
      this.on("error", function(file, message) { 
        alert(message);
        this.removeFile(file); 
      });
      this.on("success", function (file, data) {
        swal.close();
        data=JSON.parse(data);
        $('#file-selector3').data('filename',data['name']);
        var paths = $('#see-file-info3').attr('href').split('/');
        paths[ paths.length-1 ] = data['name']; // new value
        $('#see-file-info3').attr('href',paths.join('/'));
        $('#container-see-file-info3').css('display','block');
      });
      }, // end init
      uploadMultiple: false,
    });

  // Avatar image
  var avatarDropzone = new Dropzone("#avatarImage", { 
    url: adminUrl+"archivos/upload.php", 
    acceptedFiles: 'image/*',
    maxFilesize: 3, // MB
    maxFiles: 5,
    paramName: 'file[]',
    init: function () {
      var _this = this;
      this.on("sending", function(file, xhr, data) {
        swal({ title: "Avatar", text: "subiendo...", showConfirmButton: false });
        data.append("previousImage", $('#avatarImage').data('filename'));
        data.append("filetype", "avataruploadtype");
        data.append("id", userDetail['id']);
      });
      this.on("error", function(file, message) { 
        alert(message);
        this.removeFile(file); 
      });
      this.on("success", function (file, data) {
        swal.close();
        data=JSON.parse(data);
        $('#avatarImage').attr('src',data['ruta']);
        $('#avatarImage').data('filename',data['name']);
      });

      this.on("maxfilesexceeded", function(file) { });
      }, // end init
      headers: { "avatar": "avatarupload" }, //you might be also to piggyback of the headers in serverside
      uploadMultiple: false,
      clickable: true,
      addRemoveLinks: true,
    });


  //SAVE USER
  $('.save-user-btn').click(function(e){
    e.preventDefault();
    userDB();
  })

  //DELETE USER
  $('.delete-user-btn').click(function(e){
    e.preventDefault();
    $('#eliminar__entrada').modal('show');
  })
  $('#eliminarEntrada').click(function() { deleteDB(); });

  //SIDEBAR
   if(userDetail['type']==1){
      $('#usersSectionSideMenu').trigger('click');
      $('#usersSectionSideMenu').parent().addClass('active');
      $('#liUsersList').addClass('active');
  } else {
      $('#companyTeamSideMenu').trigger('click');
       $('#companyTeamSideMenu').parent().addClass('active');
      $('#liCompanyHumanTeamSideBar').addClass('active');
   }

  //SWITCH
  var elem1 = document.querySelector('.js-switch1');
  var elem1 = new Switchery(elem1, { size: 'small'});

  var elem2 = document.querySelector('.js-switch2');
  var elem2 = new Switchery(elem2, { color: '#d64842', size: 'small'});

  //DATETIME PICKERS
  $('#datetimepicker-birthdate, #datetimepicker-insurance, #datetimepicker-medicalCheck').datetimepicker({
    locale: 'es',
    timePicker: false,
    format: 'DD-MM-YYYY',
    viewMode: 'years'
  });


  // Detalle de usuario asignar valores mediante JS
  if(userDetail['action']=='update'){setJsValues();}

  // Deben de instanciarse tras haber asignado valores de bd, de lo contrario no muestran el valor asignado
  $(".select2_demo_1, .select2_demo_3").select2();
  $(".select2_demo_2").select2({placeholder: "Select a state",allowClear: true });

  if(userDetail['action']=="add")$('#user-subactions-buttons').css('display','none');


})  // end ready

function setJsValues(){
      //tallas
      setSizesValues();
      //cursos
      setSeletcMultiValues('userCourses',userDetail['courses']);
       //cursos
      setSeletcMultiValues('userActivities',userDetail['activities']);
      //cuerpo especial
      $('#userCertifiedBy').val(userDetail['certified_by']);
      //cuerpo especial
      $('#userProfessionSpecial').val(userDetail['profession_special']);
      //conocido por
      $('#userKnownFor').val(userDetail['known_for']);
      //deportes
      setSeletcMultiValues('userSports',userDetail['sports']);
      //address
      var address = JSON.parse(userDetail['address']);
      if(address!=null)$('#address_province').val(address['province']);
      //address delivery
      var addressDelivery = JSON.parse(userDetail['address_delivery']);
      if(addressDelivery!=null)$('#address_delivery_province').val(addressDelivery['province']);
    }

    function setSizesValues(){
      var sizes = JSON.parse(userDetail['equipment_sizes']);
      $.each( sizes, function( key, value ) {
        $('#size-equipment-'+key).val(value);
      });
    }


    function setSeletcMultiValues(sel,data){

      if(data!=null && data!=""){
        var data = JSON.parse(data);
        data.forEach(function (value, index) {
          console.log(value);
          $('#'+sel+' option[value="'+value+'"]').attr('selected', 'selected');

        });
      }


    }

    //UPDATE/SAVE USER
    function userDB() {

      if( $('#userName').val().length<1 || $('#userEmail').val().length<1 ) { 
        sweetAlert(  section[0],"indique un nombre y email", "error");
        return false;
      }

      data=$('#detalleUsuario').serialize();
      data+='&avatar='+ $('#avatarImage').data('filename');
      data+='&insurance_file='+ $('#file-selector2').data('filename');
      data+='&medical_file='+ $('#file-selector3').data('filename');

      $.ajax({
        type:"POST",
        dataType:"html",
        url: processFile,
        data:data,
        success:function(msg){
          sweetAlert(  section[0],"", "success");
          var res=JSON.parse(msg);
          if( typeof res['error'] != 'undefined'){
            if(res['error']==1){
              alert('Error');
              sweetAlert(  section[0],"email ya registrado", "error");
              return false;
            }

          }  
       // en caso de adición correcta preparamos valores para actualizar
       else if(   $('#action').val()=="client-add" && res['id']>0 ){
         var area = (userDetail['type']==1) ? 'CLIENTE' : 'EQUIPO-HUMANO';
         top.location.href=mainUrl+"DETALLE-"+area+"/"+res['id'];
       } 


       } // final success
     });  

    };

  //DELETE USER
  function deleteDB() {

    $.ajax({
      type:"POST",
      dataType:"html",
      url: processFile,
      data:"&action=client-delete&id="+$('#userId').val()+"&token="+tokenId,
      success:function(msg){
        var res=JSON.parse(msg);
        if(res['resultado']==-1){
          sweetAlert(  section[0],"No se ha podido ser eliminado.", "error");
          $('#eliminar__entrada').modal('hide');
        }
        else { 
           var area = (userDetail['type']==1) ? 'CLIENTES' : 'EQUIPO-HUMANO';
           top.location=mainUrl+"LISTADO-"+area;
        }

                 } // final success
               });  

  };
