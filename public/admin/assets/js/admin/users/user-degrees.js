$(document).ready(function () {


//SIDEBAR
$('#usersSectionSideMenu').trigger('click');
$('#usersSectionSideMenu').parent().addClass('active');
$('#nav-usersList').addClass('active');


//DATETIME PICKERS
  $('#datetimepicker-date').datetimepicker({
    locale: 'es',
    timePicker: false,
    format: 'DD-MM-YYYY',
    viewMode: 'years'
  });


$(".select2_demo_1").select2();

// Datatables   
table = $('#userDegreesTable').DataTable({
  "processing": true,
  "lengthChange": false,
  "searching": false,
  "serverSide": true,
  "order": [[ 2, "asc" ]],
  "pageLength": 100,
  "ajax": {
    "type": "POST",
    "url": processFile,
    "data": function ( d ) {
      d.action = "list_clientDegrees";
      d.isJs=1;
      d.user_id=userDetail['id'];
      d.token=tokenId;
    }
  },
  "columnDefs": [ // oculto columna Id
    {
    "targets": [ 0, 1, 2],
    "visible": false,
    "searchable": false
   },
],
  "columns": [
    { "data": "DT_RowId" },
    { "data": "course_id" },
    { "data": "status" },
    { "data": "course" },
    { "data": "code" },
    { "data": "diver_no" },
    { "data": "date" },
    { "data": "comment" },
    { "data": "status_txt" },
    { "data": "delete" }
  ],
  "drawCallback": function(settings) {
      $('.deleteCase').click( function (e){
        $('#eliminar__entrada').modal('show');
        $('#eliminarEntrada').data('id',$(this).data('id'));
      });
  }
}); // end DataTable

 $('#userDegreesTable tbody').on('click', 'tr', function () {
        var data = table.row( this ).data();
        $('#userCourse').val(data['course_id']).trigger('change');
    } );

$('#userCourse').change(function(event) {
  console.log('userCourseChange',$(this).val());
  if($(this).val().trim()!=''){
    console.log("Entro");
    findIfCourseExists($(this).val());
  }

 

});


$('#clientDegreeBtn').click(function(){
  if($('#userCourse').val()>0)degreeToDb();
  else sweetAlert( "Curso cliente","seleccione un curso", "error");
})


 $('#save-padi-number').click( function (e){
        savePadiNumber();
    });


}); // end ready


$('#eliminarEntrada').click(function() {
      $.ajax({
            type:"POST",
            dataType:"html",
            url: processFile,
            data:"&action=clientDegree-delete&id="+$(this).data('id')+"&token="+tokenId,
            success:function(res){
                res=jQuery.parseJSON(res);
                if(res['resultado']==1){ 
                      table.ajax.reload();
                      cleanForm();
                }
                $("#eliminar__entrada").modal('hide');
            }
              });
    });


function degreeToDb(){

    console.log($('#userDegreeForm').serialize());

    $.ajax({
        type:"POST",
        dataType:"html",
        url: processFile,
        data:$('#userDegreeForm').serialize(),
        success:function(msg){
          var res=JSON.parse(msg);
          if( typeof res['error'] != 'undefined'){
          }  else if(res['resultado']==1) {
            sweetAlert( "Curso cliente","", "success");
            table.ajax.reload();
            if($('#action').val()=='clientDegree-add'){
             $('#action').val('clientDegree-update'); 
             $('#clientDegreeBtn').text('actualizar curso');
           }

          }
       } // final success
     });  

}


/* Search in table data if course selected exists */
function findIfCourseExists ($idSelected){

  var data = table.data().toArray();
  found=false;
  data.forEach(function(row, i) {
    if($idSelected==row['course_id']){
      fillFormFromTableRow(row);
      found=true;
      return false;
    }

     

  });

     //if not found cleanForm;
    if(found==false)cleanForm(true);

}


function  cleanForm(preserveUserCourse){

  $('#action').val('clientDegree-add');
  $('#degreeDiver_no, #degreeCode, #degreeComment').val(''); 
  $('#degreeStatus').val('').trigger('change');
  $('#datetimepicker-date').datetimepicker('clear');
  $('#clientDegreeBtn').text('añadir curso');
  if(preserveUserCourse!=true)$('#userCourse').val('').trigger('change');

}


function  fillFormFromTableRow(row){
  console.log("eee",row['diver_no']);
  $('#clientDegreeBtn').text('actualizar curso');
  $('#action').val('clientDegree-update');
  $('#degreeDiver_no').val(row['diver_no']);
  $('#degreeCode').val(row['code']);
  $('#degreeStatus').val(row['status']).trigger('change');
  $('#degreeComment').val(row['comment']);
 
  $('#datetimepicker-date').datetimepicker('date', row['date']);

}


function savePadiNumber(){

  $.ajax({
        type:"POST",
        dataType:"html",
        url: processFile,
        data:"&action=userPadiNumber&user_id="+userDetail['id']+"&padi_number="+$('#padi_number').val()+"&token="+tokenId,
        success:function(msg){
        var res=JSON.parse(msg);
        if(res['resultado']==1) sweetAlert( "Número identificación PADI","", "success");
       } // final success
     });  
}