// JavaScript Document
//"use strict"; // enforce variable declarations – safer coding
// Makes sure the document is ready before executing scripts
var processFile = ruta+"assets/inc/ajaxAdmin.inc.php";
var clienteId=0;
var presupuestoId=0;

jQuery(function($){
// A quick check to make sure the script loaded properly
   // File to which AJAX requests should be sent
	
	refPresupuesto();
	clientePresupuesto();
	tituloPresupuesto();
	
	
}); // final ready


function refPresupuesto() {
    var a = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
			cache : false,
            url: processFile ,
			transport: function (opts, onSuccess, onError) {
            $.ajax({
                url: processFile,
				dataType: "json",
                data:"&action=ppto_ref_autocomplete&ref="+jQuery("#presupuesto-ref").val()+"&token="+tokenId,
                type: "POST",
                success: onSuccess,
                error: onError,
            })
        }
			
      },
        limit: 100
    });
    a.initialize();
    jQuery("#presupuesto-ref").typeahead({minLength: 2}, {
        name: "referencia-autocomplete",  display: "referencia", source: a,
        templates: {
            suggestion: function(b) {return "<div>" + b.referencia + "</div>" }
        },
        limit: 100
    });
	
	jQuery("#presupuesto-ref").bind("typeahead:select", function(d, b) {
			    jQuery("#presupuesto-cliente, #presupuesto-titulo").val('');
				clienteId=0; presupuestoId=b.id;
				cargarGalerias()
    });
	
    jQuery("#presupuesto-ref").blur(function() {
    })
	
}// refPresupuesto
	

function clientePresupuesto() {
    var a = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
			cache : false,
            url: processFile ,
			transport: function (opts, onSuccess, onError) {
            $.ajax({
                url: processFile,
				dataType: "json",
                data:"&action=ppto_cliente_autocomplete&cliente="+jQuery("#presupuesto-cliente").val()+"&token="+tokenId,
                type: "POST", success: onSuccess, error: onError,
            })
        }
			
      },
        limit: 100
    });
    a.initialize();
    jQuery("#presupuesto-cliente").typeahead({minLength: 2}, {
        name: "cliente-autocomplete",  display: "cliente", source: a,
        templates: {
            suggestion: function(b) {return "<div>" + b.cliente + "</div>" }
        },
        limit: 100
    });
	
	jQuery("#presupuesto-cliente").bind("typeahead:select", function(d, b) {
				jQuery('#presupuesto-titulo, #presupuesto-ref').typeahead('val', ' ');
				clienteId=b.id;
     			console.log(clienteId);
    });
	
	
}// refPresupuesto

//Título presupuesto
function tituloPresupuesto() {
    var a = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
			cache : false,
            url: processFile ,
			transport: function (opts, onSuccess, onError) {
            $.ajax({
                url: processFile,
				dataType: "json",
                data:"&action=ppto_titulo_autocomplete&clienteId="+clienteId+"&titulo="+jQuery("#presupuesto-titulo").val()+"&token="+tokenId,
                type: "POST", success: onSuccess, error: onError,
            })
        }
			
      },
        limit: 100
    });
    a.initialize();
    jQuery("#presupuesto-titulo").typeahead({minLength: 0}, {
        name: "titulo-autocomplete",  display: "titulo", source: a,
        templates: {
            suggestion: function(b) {return "<div>" + b.titulo + "</div>" }
        },
        limit: 100
    });
	
	jQuery("#presupuesto-titulo").bind("typeahead:select", function(d, b) {
		        jQuery("#presupuesto-ref").val('');
				presupuestoId=b.id;
     			cargarGalerias();
    });
	
	
}// refPresupuesto
	
	
function cargarGalerias(){
	top.location.href="https://www.cargaur.com/LISTADO-GALERIAS/"+presupuestoId;
	console.log("Cargar galerías presupuesto: "+presupuestoId);
	}