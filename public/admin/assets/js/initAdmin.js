// JavaScript Document
//"use strict"; // enforce variable declarations – safer coding
// Makes sure the document is ready before executing scripts
jQuery(function($){
// A quick check to make sure the script loaded properly
   // File to which AJAX requests should be sent
	var processFile = ruta+"assets/inc/ajaxAdmin.inc.php";

   
    $("#submitBtn").prop('disabled', false);
	
	/*************** LOGIN ***********************************************/
		
	$("#login-form").on("submit", function(event){
		// Prevents the default form action from executing
		event.preventDefault();
		// Serializes the form data for use with $.ajax()
				$("#submitBtn").prop('disabled', true);
				// Loads the action for the processing file
				var formData = $(this).serialize();
				//alert( formData ); return false;
				// Loads the editing form and displays it
				$.ajax({
				type: "POST",
					url: processFile,
					data: formData,
					success: function(data){
						var response = JSON.parse(data);
						if(response[0]=="Error") { alert(response[1]); $("#submitBtn").prop('disabled', false); }
						else { top.location.href=response[1]; }
					},
					error: function(msg){
						
					}
				});	
	})		
	
	
		/*************** CHANGE PASSWORD ***********************************************/
			
		$("#change-password-form").on("submit", function(event){
		// Prevents the default form action from executing
		event.preventDefault();
		// Serializes the form data for use with $.ajax()
				
			if( $('#f_new_password').val().length < 6 ) { alert ("Password must have at least 6 characters"); return false; }
				//verificaciones previas
			if( $('#f_new_password').val() != $('#f_retype_password').val() ) {alert ("The passwords you entered didn't match. Try again."); return false; }
				
				
				$("#submitBtn").prop('disabled', true);
				// Loads the action for the processing file
				var formData = $(this).serialize();
				// Loads the editing form and displays it
				$.ajax({
				type: "POST",
					url: processFile,
					data: formData,
					success: function(data){
						var response = JSON.parse(data);
						alert(response[1]);
						if(response[0]=="Error") {  $("#submitBtn").prop('disabled', false); }
						else { top.location.href=response[2]; }
					},
					error: function(msg){
						
					}
				});	
	})		
				
			
		
	
	/*************** RESET PASSWORD ***********************************************/
			
		$("#reset-password-form").on("submit", function(event){
		// Prevents the default form action from executing
		event.preventDefault();
		// Serializes the form data for use with $.ajax()
				
				$("#submitBtn").prop('disabled', true);
				// Loads the action for the processing file
				var formData = $(this).serialize();
				// Loads the editing form and displays it
				$.ajax({
				type: "POST",
					url: processFile,
					data: formData,
					success: function(data){
						//alert(data);
						var response = JSON.parse(data);
						alert(response[1]);
						if(response[0]=="Error") {  $("#submitBtn").prop('disabled', false); }
						else if(response[0]=="Success") { top.location.href=response[2]; }
					},
					error: function(msg){
						
					}
				});	
	})		
				
			
		
	// Collapse plugin

$("[data-toggle='collapse']").click(function(e) {
	e.preventDefault();
});
	
	
   
   
}); // final ready