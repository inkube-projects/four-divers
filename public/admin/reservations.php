<?php include('include/app-init-login.php'); $useDataTables=true; $useDateTimePicker=true;?>
<!DOCTYPE html>
<head>
  <?php include('include/estructure/head.php'); ?>
</head>
<body>

  <!-- Wrapper-->
  <div class="wrapper">
   <?php include('include/estructure/header.php'); ?>
   <!-- Main content-->
   <section class="content">
    <div class="container-fluid" style="opacity: 1">


     <!--TITULAR  SECCION-->
     <div class="row">
      <div class="col-lg-12">
        <div class="view-header">


         <div class="pull-right text-right" style="line-height: 14px">
           <a href="<?=URL?>ALTA-RESERVA" class="btn btn-w-md btn-info">nueva reserva</a>
         </div>

         <div class="header-icon">
          <i class="pe page-header-icon pe-7s-cart"></i>
        </div>
        <div class="header-title">
          <h3 class="m-b-xs">Reservas</h3>
          <small>
            Gestión de reservas y salidas 
          </small>

        </div>
      </div>
      <hr>
    </div>
  </div>
  <!--END. TITULAR  SECCION-->
 <div class="row css-paddingB20-M">
      <div class="btn-group cnt-center css-inlineBlock">
        <div class="col-12 col-lg-6">
          <div class="form-group">
            <label>Desde</label>
            <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
            <input id="dateFrom" type="text" class="form-control datetimepicker-input" data-target="#datetimepicker1"/>
              <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-lg-6">
          <div class="form-group">
            <label>Hasta</label>
            <div class="input-group date" id="datetimepicker2" data-target-input="nearest">
            <input id="dateTo" type="text" class="form-control datetimepicker-input" data-target="#datetimepicker2"/>
              <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>
      
      <div class="row css-paddingB30 css-paddingL10 css-paddingR10">
        <div class="btn-group cnt-center">
        <button id="allReservationsBtn" class="btn btn-default css-txt-M" type="button">Todas<span class="ocultaM"> las reservas</span></button>
        <button id="todayReservationsBtn" class="btn btn-default css-txt-M" type="button"><span class="ocultaM">Reservas </span>hoy</button>
        <button id="weekReservationsBtn" class="btn btn-default css-txt-M" type="button"><span class="ocultaM">Reservas </span>semanales</button>
        </div>
      </div>

<div class="row">

  <div class="col-md-12">
    <div class="panel ">

      <div class="panel-body">

        <table id="tableReservations" class="table table-hover table-responsive-sm">
          <thead>
            <tr>
              <th>Ref.</th>
              <th>Fecha</th>
              <th>Cliente</th>
              <th>Tipo</th>
              <th><i class="fa fa-shopping-cart css-text24" aria-hidden="true"></i></th>
            </tr>
         </thead>
         <tbody>
         </tbody>
       </table>
     </div>
   </div>
 </div>


</div>

</div>
</section>
<!-- End main content-->

</div>
<!-- End wrapper-->
<?php 
include('include/js.php'); 
?>
<script type="text/javascript" src="<?=URLADMIN?>assets/js/admin/reservations/reservations-list.js"></script>

</body>

</html>