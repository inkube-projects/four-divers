<?php include('include/app-init-login.php'); $useDataTables=true; $useCustomSelect=true; ?>
<!DOCTYPE html>
<head>
  <?php include('include/estructure/head.php'); ?>
</head>
<body>

  <!-- Wrapper-->
  <div class="wrapper">
   <?php include('include/estructure/header.php'); ?>
   <!-- Main content-->
   <section class="content">
    <div class="container-fluid" style="opacity: 1">


     <!--TITULAR  SECCION-->
     <div class="row">
      <div class="col-lg-12">
        <div class="view-header">


         <div class="pull-right text-right" style="line-height: 14px">
           <a href="<?=URL?>ALTA-PRODUCTO" class="btn btn-w-md btn-info">nuevo producto</a>
         </div>

         <div class="header-icon">
          <i class="pe page-header-icon pe-7s-albums"></i>
        </div>
        <div class="header-title">
          <h3 class="m-b-xs">Catálogo</h3>
          <small>
            Productos y servicios 
          </small>

        </div>
      </div>
      <hr>
    </div>
  </div>
  <!--END. TITULAR  SECCION-->
  <div class="row css-paddingB30">
    <div class="cnt-center css-inlineBlock">
     <div class="col-md-12">
      <label>Catálogo</label>
      <select id="productsFamilies" class="select2_demo_1 form-control" style="width: 100%">
        <option value="">Todo el catálogo</option>
        <option value="1">Cursos</option>
        <option value="2">Actividades</option>
        <option value="3">Equipamiento</option>
        <option value="4">Seguros</option>
        <option value="5">Alojamiento</option>
      </select>
    </div>
  </div>
</div>

<div class="row">

  <div class="col-md-12">
    <div class="panel ">

      <div class="panel-body">

        <table id="tableProducts" class="table table-hover table-responsive-sm">
          <thead>
            <tr>
             <th>ID</th>
             <th>Ref.</th>
             <th>Familia</th>
             <th>Tipo</th>
             <th>Título</th>
             <th>Precio</th>
             <th>Stock</th>
             <th>Activo</th>
           </tr>
         </thead>
         <tbody>
         </tbody>
       </table>
     </div>
   </div>
 </div>


</div>

</div>
</section>
<!-- End main content-->

</div>
<!-- End wrapper-->
<?php 
include('include/js.php'); 
?>
<script type="text/javascript" src="<?=URLADMIN?>assets/js/admin/products/products-list.js"></script>

</body>

</html>