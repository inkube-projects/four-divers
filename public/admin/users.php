<?php include('include/app-init-login.php'); $useDataTables=true; 
$area=($_REQUEST['type']==1)?array('clientes','CLIENTE'):array('equipo humano','EQUIPO-HUMANO');
?>
<!DOCTYPE html>
<head>
  <?php include('include/estructure/head.php'); ?>
</head>
<body>

    <!-- Wrapper-->
    <div class="wrapper">

     <?php include('include/estructure/header.php'); ?>
     <!-- Main content-->
     <section class="content">
        <div class="container-fluid">


           <!--TITULAR  SECCION-->
           <div class="row">
            <div class="col-lg-12">
                <div class="view-header">

                 <div class="pull-right text-right" style="line-height: 14px">
                     <a href="<?=URL."ALTA-".$area[1]?>" class="btn btn-w-md btn-info">nuevo</a>
                 </div>

                 <div class="header-icon">
                    <i class="pe page-header-icon pe-7s-users"></i>
                </div>
                <div class="header-title">
                    <h3 class="m-b-xs"><?=ucfirst($area[0])?></h3>
                    <small>
                        Gestión de <?=$area[0]?>
                    </small>

                </div>
            </div>
            <hr>
        </div>
    </div>

    <div class="row">


        <div class="col-md-12">
            <div class="panel ">

                <div class="panel-body">

                    <table id="usersTable" class="table table-hover table-responsive-sm">
                        <thead>
                            <tr>
                             <th>ID</th>
                             <th>Fecha</th>
                             <th>Cliente</th>
                             <th>Tel.</th>
                             <th>Email</th>
                             <th>Tallas</th>
                             <th>Estado</th>
                         </tr>
                     </thead>
                     <tbody>
                     </tbody>
               </table>
           </div>
       </div>
   </div>



</div>

</div>
</section>
<!-- End main content-->

</div>
<!-- End wrapper-->

<?php 
include('include/modals/equipment_sizes.html'); 
include('include/js.php'); 
?>
<script>userType=<?=$_REQUEST['type']?>;</script>
<script type="text/javascript" src="<?=URLADMIN?>assets/js/admin/users/users-list.js"></script>

</body>

</html>