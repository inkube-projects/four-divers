<!--modal añadir-->
  <div class="modal fade" id="modGallleryModal" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">
                                                <h4 id="addModGalleryTitle" class="modal-title">Modificar galería</h4>
                                            </div>
                                            <div class="modal-body">                                
                                            
                                             <div class="form-group">
                                                 <input placeholder="Nombre"  class="form-control input-lg" type="text" id="modGalleryName" value=""></div>
                                                  <div class="form-group">
                                                 <textarea id="modGalleryDes"  class="form-control"  placeholder="Descripción"  style="resize:none; margin-top:20px;"></textarea></div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                <button data-budgetid="<?=intval($_REQUEST['idObj'])?>" id="modGaleriaBtn" type="button" class="btn btn-accent">Modificar</button>
                                            </div>
                                        </div>
                         </div>
             
       </div>
    
    <!-- /modal -->
    