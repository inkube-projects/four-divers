<div class="modal fade" id="eliminar__entrada" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">ELIMINAR</h4>
      </div>
      <div class="modal-body">       
     <p>¿Confirma que desea eliminar este registro?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
        <button type="button" id="eliminarEntrada" class="btn btn-danger">SI</button>
      </div>
    </div>
  </div>
</div>