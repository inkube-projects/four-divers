<div class="modal fade" id="editar__imagen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">EDITAR ARCHIVO/IMAGEN</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
      <div class="form-group">
            <label class="col-sm-6 control-label">Comentario</label>
                    <div class="col-sm-12">
                        <textarea style="resize: none;" prevalue="" id="em_alt" name="alt" type="text" class="form-control"></textarea>
                    </div>
             </div>
   
             <div class="form-group">
            <label class="col-sm-6 control-label">Nombre archivo</label>
                    <div class="col-sm-10">
                        <input disabled rutavalue="" extvalue="" prevalue="" id="em_nombre" name="nombre" type="text" class="form-control">
                    </div>
             </div>
              <!--<label style="font-size:10px;" for="em_nombre">No modificar el nombre de la imagen si ya ha sido añadida a la descripci&oacute;n.  </label>-->
           </form>
             
      <img style="width:100%; height:auto"  id="imagenGrandota" src="" alt="" class="img-responsive"/>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" id="editarImagen" class="btn btn-success">Guardar Cambios</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="eliminar__imagen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Eliminar archivo/imagen</h4>
      </div>
      <div class="modal-body">       
     <p>&iquest;Confirma que desea eliminarlo?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" id="eliminarImagen" class="btn btn-danger">Eliminar</button>
      </div>
    </div>
  </div>
</div>