  <!-- Vendor styles -->
  <link rel="stylesheet" href="<?=URLADMIN?>assets/js/fontawesome/css/font-awesome.css"/>
  <link rel="stylesheet" href="<?=URLADMIN?>assets/js/animate.css/animate.css"/>
  <link rel="stylesheet" href="<?=URLADMIN?>assets/js/bootstrap/css/bootstrap.css"/>
  <link rel="stylesheet" href="<?=URLADMIN?>assets/js/toastr/toastr.min.css"/>

  <!-- App styles -->
  <link rel="stylesheet" href="<?=URLADMIN?>assets/css/pe-icons/pe-icon-7-stroke.css"/>
  <link rel="stylesheet" href="<?=URLADMIN?>assets/css/pe-icons/helper.css"/>
  <link rel="stylesheet" href="<?=URLADMIN?>assets/css/stroke-icons/style.css"/>

  <!-- EMPLEA REDACTOR TEXTO -->
  <?php if(isset($useTextRedactor)) { ?>
  <link rel="stylesheet" href="<?=URLADMIN?>assets/js/redactor/redactor.css" />
  <?php } ?>
  <!-- EMPLEA DATA TABLES -->
  <?php if(isset($useDataTables)) { ?>
  <link rel="stylesheet" href="<?=URLADMIN?>assets/css/datatables.min.css">
  <?php } ?>
  <?php if(isset($useCustomSelect)) { ?>
  <!--SELECT-->
  <link rel="stylesheet" href="<?=URLADMIN?>assets/js/select2/dist/css/select2.min.css">
  <?php } ?>
  <?php if(isset($useCustomCheck)) { ?>
  <!--CHECK-->
  <link rel="stylesheet" href="<?=URLADMIN?>assets/js/switchery/switchery.min.css">
  <?php } ?>
  <?php if(isset($useDateTimePicker)) { ?>
  <!-- Datetimepicker-->
  <link rel="stylesheet" href="<?=URLADMIN?>assets/css/bootstrap-datepicker.min.css"/>
  <?php } ?>
  <!-- SweetAlert-->
   <?php if(isset($useSweetAlert)) { ?>
  <link href="<?=URLADMIN?>assets/css/sweetalert.css" rel="stylesheet">
  <?php } ?>
  <?php if(isset($useDropZone)) { ?>
  <link href="<?=URLADMIN?>assets/css/dropzone.min.css" rel="stylesheet">
  <?php } ?>

  <link rel="stylesheet" href="<?=URLADMIN?>assets/css/style.css">
  <link rel="stylesheet" href="<?=URLADMIN?>assets/css/responsive.css">