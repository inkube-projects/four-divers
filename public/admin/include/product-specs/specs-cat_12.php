
<div class="form-group col-4 col-md-2 col-lg-2 ">
  <label>Edad mínima</label>
  <input type="text" value="" id="specs-minimum_age" class="form-control product-specs" name="minimum_age">
</div>
<div class="form-group col-4 col-md-2 col-lg-2 ">
  <label>Duración (días)</label>
  <input type="text" value="" id="specs-day_duration" class="form-control product-specs" name="day_duration">
</div>

<div class="form-group col-4 col-md-2 col-lg-2 ">
  <label>Duración máxima</label>
  <input type="text" value="" id="specs-day_duration_max" class="form-control product-specs" name="day_duration_max">
</div>

<div class="form-group col-4 col-md-2 col-lg-2 ">
  <label>Horas teoría</label>
  <input type="text" value="" id="specs-hours_theory" class="form-control product-specs" name="hours_theory">
</div>

<div class="form-group col-4 col-md-2 col-lg-2 ">
  <label>Horas práctica</label>
  <input type="text" value="" id="specs-hours_practice" class="form-control product-specs" name="hours_practice">
</div>

<div class="form-group col-4 col-md-2 col-lg-2 ">
  <label>Certificado médico </label> <br>
  <input id="specs-require_medical_certificate" name="require_medical_certificate" type="checkbox" class="js-switch-specs product-specs"  />
</div>

<div class="form-group col-4 col-md-2 col-lg-2 ">
  <label>Seguro </label> <br>
  <input id="specs-require_insurance" name="require_insurance" type="checkbox" class="js-switch-specs product-specs" />
</div>


