
<div class="form-group col-4 col-md-2 col-lg-2 ">
  <label>Habitación</label>
   <select id="specs-room_type" class="js-specs-select2 form-control product-specs" name="room_type" >
                  <option value="">Seleccione</option>
                  <option value="single">Sencilla</option>
                  <option value="double">Doble</option>
                </select>
</div>

<div class="form-group col-4 col-md-2 col-lg-2 ">
  <label>Régimen</label>
   <select id="specs-board_type" class="js-specs-select2 form-control product-specs" name="board_type" >
                  <option value="">Seleccione</option>
                  <option value="breakfast">Desayuno</option>
                  <option value="half-board">Media pensión</option>
                  <option value="full-board">Pensión completa</option>
                </select>
</div>






