  <!-- Navigation-->
  <aside class="navigation">
    <nav>

     <ul class="nav luna-nav">

        <li id="nav-dashboard">
            <a href="<?=URL?>ADMIN-FOUR-DIVERS">Escritorio</a>
        </li>

        <li class="nav-category">
            GESTIÓN
        </li>

        <li>
            <a href="#">Agenda/Tareas</a>
        </li>

        <li>
            <a id="reservationsSectionSideMenu" href="#nav-ul-reservation" data-toggle="collapse" aria-expanded="false">
                Reservas<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
            </a>
            <ul id="nav-ul-reservation" class="nav nav-second collapse">
                <li id="liReservationsSideBar" section="1.0"><a href="<?=URL?>LISTADO-RESERVAS"> Listado </a></li>
                <li id="liDeparturesSideBar"  section="1.1"><a href="#"> Salidas</a></li>
                <li id="liCancellationSideBar" section="1.2"><a href="#"> Cancelaciones</a></li>

            </ul>
        </li>
        <!-- VENTAS -->

        <li>
            <a id="salesSectionSideMenu" href="#nav-ul-sales" data-toggle="collapse" aria-expanded="false">
                Ventas<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
            </a>
            <ul id="nav-ul-sales" class="nav nav-second collapse">
                <li id="liSalesSideBar" section="2.0"><a href="#"> Listado</a></li>
                <li id="liPaymentSideBar" section="2.1"><a href="#"> Abonos</a></li>
                <li id="liShippingSideBar" section="2.2"><a href="#"> Envios</a></li>
            </ul>
        </li>
        <!-- USUARIOS -->
        <li>
            <a id="usersSectionSideMenu"  href="#nav-ul-users" data-toggle="collapse" aria-expanded="false">
                Usuarios<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
            </a>
            <ul id="nav-ul-users" class="nav nav-second collapse">
                <li id="liUsersList" section="3.0"><a href="<?=URL?>LISTADO-CLIENTES"> Clientes</a></li>
                <li id="liUsersAdminList" section="3.1"><a href="#"> Usuarios</a></li>
            </ul>
        </li>

        <!-- COMUNICACIÓN -->

        <li class="nav-category">
            COMUNICACIÓN
        </li>
        <li>
            <a id="notificationsSectionSideMenu"  href="#nav-ul-notifications" data-toggle="collapse" aria-expanded="false">
                Notificaciones<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
            </a>
            <ul id="nav-ul-notifications" class="nav nav-second collapse">
                <li id="liAlertsSideBar" section="4.0"><a href="#">Alertas</a></li>
                <li id="liRenewalSideBar" section="4.1"><a href="#l">Renovaciones</a></li>
                <li id="liAdviseSideBar" section="4.2"><a href="#">Avisos</a></li>
            </ul>
        </li>

        <li>
            <a href="#nav-ul-blog" data-toggle="collapse" aria-expanded="false">
                Blog<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
            </a>
            <ul id="nav-ul-blog" class="nav nav-second collapse">
                <li id="liBlogListSideBar" section="5.0"><a href="#"> Listado</a></li>
                <li id="liCommentsListSideBar" section="5.1"><a href="#"> Comentarios</a></li>
            </ul>
        </li>

        <li id="liSliderHomeSideBar" section="6.0">
           <a href="#">Slider Home</a>
       </li>

       <li id="liGiftVoucherSideBar" section="7.0">
           <a href="#">Vales regalo</a>
       </li>

       <li class="nav-category">
        CATÁLOGO
    </li>
    <li>
        <a id="productsSectionSideMenu" href="#nav-ul-products" data-toggle="collapse" aria-expanded="false">
            Productos<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
        </a>
        <ul id="nav-ul-products" class="nav nav-second collapse">
            <li id="liProductsListSideBar" section="8.0"><a href="<?=URL?>LISTADO-PRODUCTOS"> Listado</a></li>
            <!--<li><a href="#l">Actividades</a></li>
            <li><a href="#"> Alojamiento</a></li>
            <li><a href="#"> Cruceros</a></li>
            <li><a href="#"> Seguros</a></li>-->
        </ul>
    </li>

    <!--<li>
        <a href="#material" data-toggle="collapse" aria-expanded="false">
            Material<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
        </a>
        <ul id="material" class="nav nav-second collapse">
            <li><a href="#"> Productos</a></li>
            <li><a href="#"> Equipamiento</a></li>
        </ul>
    </li>

    <li>
       <a href="#">Barco</a>
   </li>-->



   <li class="nav-category">
     FOURDIVERS
 </li>
 <li>
    <a id="companySectionSideMenu" href="#nav-ul-company" data-toggle="collapse" aria-expanded="false">
        Empresa<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
    </a>
    <ul id="nav-ul-company" class="nav nav-second collapse">
        <li id="liCompanyProfileSideBar" section="9.0"><a href="#"> Perfil</a></li>
        <li id="liCompanyPresentationSideBar" section="9.1"><a href="#l"> Presentación</a></li>
        <li id="liCompanyCertificatesSideBar" section="9.2"><a href="#"> Certificaciones</a></li>
    </ul>
</li>

<li>
    <a id="companyTeamSideMenu"  href="#nav-ul-team" data-toggle="collapse" aria-expanded="false">
        Equipo<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
    </a>
    <ul id="nav-ul-team" class="nav nav-second collapse">
        <li id="liCompanyHumanTeamSideBar" section="10.1"><a href="<?=URL?>LISTADO-EQUIPO-HUMANO"> Equipo humano</a></li>
    </ul>
</li>

<li class="nav-info">
   <a  href="<?=URLADMIN?>logout.php" data-toggle="tooltip" data-placement="top" title="" data-original-title="Salir">
    <i class="pe pe-7s-power text-accent"></i>
</a>
<div class="m-t-xs">
    <span class="c-white">FOUR DIVERS</span><br> admin PRO V 0.1
</div>
</li>
</ul>

</nav>

</aside>
    <!-- End navigation-->