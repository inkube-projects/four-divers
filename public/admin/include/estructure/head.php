    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="<?php echo $_SESSION['token']; ?>">
    <link rel="manifest" href="<?=URLADMIN?>assets/pwa/manifest.webmanifest">
    <link rel="apple-touch-icon" sizes="192x192" href="<?=URLADMIN?>assets/pwa/xz-ico_4x.png">
    <meta name="theme-color" content="#317EFB"/>
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>

    <!-- Page title -->
    <title>FOUR DIVERS ADMIN</title>
		
    <?php include('include/css.php'); ?>