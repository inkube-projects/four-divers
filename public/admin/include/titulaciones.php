<?php

include('include/app-init-login.php');
$useCustomCheck=true; $useCustomSelect=true; $useDateTimePicker=true; $useSweetAlert=true; $useDropZone=true; 
$plusTitle="";
$avatarImage=URLADMIN.'images/icoUser.png';
if( isset($_REQUEST['action']) && $_REQUEST['action']=="add" ){
   $action='add'; $rowDetail=array('id'=>0,'address'=>'{}','address_delivery'=>'{}'); 
}
else if( isset($_REQUEST['idObj']) && intval($_REQUEST['idObj'])>0 ) { 
 $action='update';
 $rowDetail = AdminClients::clientDetail($dbo,$_REQUEST['idObj']);
 $plusTitle=" - ".$rowDetail['name']." ".$rowDetail['surname'];
 if(strlen($rowDetail['avatar'])>0) $avatarImage = URLPUBLIC."assets/images/users/u-".$rowDetail['id']."/".$rowDetail['avatar'];
} else {
  echo "Incorrect access";
  exit();
}

$rowDetail['action']=$action;
$address = json_decode($rowDetail['address'],true);
$addressDelivery = json_decode($rowDetail['address_delivery'],true);
$provinces  = AdminBaseSection::readTable($dbo,'states', 'name', 'name','ASC', 'country_id=205');



?>
<!DOCTYPE html>
<head>
	<?php include('include/estructure/head.php'); ?>
	<link rel="stylesheet" href="<?=URLADMIN?>assets/css/user_detail.css"/>
</head>
<body>
    <!-- Wrapper-->
    <div class="wrapper">
        <?php include('include/estructure/header.php'); ?>
        <!-- Main content-->
        <section class="content">
            <div class="container-fluid" style="opacity: 1;">

                <!--FORMULARIO USUARIO-->
                <form action="#" id="detalleUsuario">
                   <input id="formToken"  type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
                   <input type="hidden" name="action" id="action" value="client-<?=$action?>" />
                   <input type="hidden" name="userId" id="userId" value="<?=$rowDetail['id']?>" />

                   <!--TITULAR  SECCION-->
                   <div class="row">
                    <div class="col-lg-12">
                        <div class="view-header">
                            <div  class="header-icon2"> 
                                <img data-filename="<?=$rowDetail['avatar']?>" id="avatarImage" src="<?=$avatarImage?>" class="avatar rounded-circle" alt="">
                            </div>
                            <div class="header-title">
                                <h3 class="m-b-xs"><?=$rowDetail['name']." ".$rowDetail['surname']?></h3>
                                <small>
                                   <span class="text-accent"> ID: <?=$rowDetail['nif']?></span>
                               </small>

                               <div class="m-xs pull-right">
                                   <div class="" style="width: 100px; height: 25px; display: inline;"><label>Newsletter </label> <input name="news" type="checkbox" class="js-switch2" <?php if($rowDetail['news']==1) echo 'checked' ?> /></div>
                                   <div class="" style="width: 100px; height: 25px; display: inline;"><label>Activo </label> <input name="active" type="checkbox" class="js-switch1" <?php if($rowDetail['active']==1) echo 'checked' ?> /></div>
                               </div>

                           </div>
                       </div>
                       <hr>
                   </div>
               </div>
               <!--END. TITULAR  SECCION-->
			   
			   
			  <div class="row">
			  	<div class="form-group col-md-12">
					<button type="submit" role="link" onclick="window.location='<?=URLADMIN?>DETALLE-CLIENTE/75'" class="btn btn-primary pull-right css-marginR10">
						<i class="fa fa-chevron-left"></i> <span class="bold">  volver </span>
					</button>
				</div>
			  </div>
				  
				<div class="row">
		
						<!--DATOS DE HISTÓRICO  CURSOS -->
						<div class="col-md-12">
							<div class="panel panel-filled">
								<div class="panel-heading">
								<h4> Histórico de titulaciones (Fourdivers)</h4>
								</div>
								
								<div class="panel-body">

									<div class="row">
									
									<div class="form-group col-xs-12 col-md-6 col-lg-6">
										<label>Titulación</label>
										<select class="select2_demo_1 form-control" multiple="multiple" id="userCoursesFours"  name="coursesFours[]"  style="width: 100%">
											<option value="OWD">Open Water Dive</option>
											<option value="AOWD">Advanced Open Water</option>
											<option value="EFR">Emergency First</option>
											<option value="RESCUE">Rescuer Diver</option>
											<option value="DIVEMASTER">Divemaster</option>
											<option value="NAVLICENSE">Licencia Navegación</option>
											<option value="SDJUNIOR">Scuba diver Junior</option>
											<option value="NITROX">Nitrox</option>
										</select>
									</div>
									

									<div class="form-group col-xs-12 col-md-6 col-lg-3">
										<label>CODE</label>
										<input type="text" value="" id="codeCurso" class="form-control" name="codeCurso">
										</div>

									<div class="form-group col-xs-12 col-md-6 col-lg-3">
										<label>DIVER NO.</label>
										<input type="text" value="" id="divernoCurso" class="form-control" name="divernoCurso">
									</div>

									 <div class="form-group col-xs-12 col-md-6 col-lg-3">
										<label>Fecha </label>
										<input type="text" value="" id="fechafinalCurso" class="form-control" name="fechafinalCurso">
									</div>

									 <div class="form-group col-xs-12 col-md-6 col-lg-3">
										<label>Comentario </label><br>
										<input type="text" value="" id="comentfinalCurso" class="form-control for" name="comentfinalCurso">
									</div>


									<div class="form-group col-lg-3">
										<label>Estado</label>
										<select class="select2_demo_1 form-control" id="estadorCoursesFours"  name="estadoCoursesFours[]"  style="width: 100%">
											<option value="Proceso">En proceso</option>
											<option value="Aprobado">Aprovado</option>
										</select>
									</div>
									
									<div class="form-group col-md-3">
									<label>Nuevo curso</label>
										<button type="submit" class="btn btn-accent btn-block"> añadir curso </button>			
									</div>


									</div>
								
								</div>
								
								
								
								<div class="panel-body">

								<div class="row">
								
											<p class="text-muted css-text18"><span class="pe-7s-info css-text34 text-warning"></span> NÚMERO IDENTIFICACIÓN <strong>PADI: </strong><span class="text-warning"> 02345697</span></p>
                         
                          <table id="tableReserva" class="table table-hover table-responsive-sm">
                                <thead>
                                <tr>
													  <th>Curso</th>
                                     <th>CODE</th>
                                     <th>DIVER NO</th>
													  <th>Fecha</th>
													  <th>Coment</th>
													  <th>Estado</th>
                                </tr>
                                </thead>
                                <tbody>
								
								
											   <tr>
													<td>Open Water Dive</td>
													<td>3456789</td>
                                   <td>8934758</td>
													<td>24/04/2020</td>
													<td></td>
													<td><i class="fa fa-check-circle text-success css-text20" aria-hidden="true"></i></td>
                                </tr>
								
											  	<tr>
													<td>Avanced Open Water</td>
													<td>3456789</td>
                                   <td> </td>
													<td>24/04/2020</td>
													<td>le falta una práctica</td>
													<td><i class="fa fa-check-circle text-warning css-text20" aria-hidden="true"></i></td>
                                </tr>

                                </tbody>
                            </table>
								</div>

								</div>
							</div>
						</div>

		
			</div>

</div>

</form>
<!--END FORMULARIO USUARIO-->

<?php include_once("include/modals/common-modals.php"); ?>
</div>
</section>
<!-- End main content-->

</div>
<!-- End wrapper-->

<?php 
include('include/js.php'); 
?>
<script>
    var userDetail=<?=json_encode($rowDetail)?>;
    console.log(userDetail);
</script>

<script type="text/javascript" src="<?=URLADMIN?>assets/js/admin/users/user-detail.js"></script>

</body>

</html>