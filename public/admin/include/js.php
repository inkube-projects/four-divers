<!-- JS Global -->
<script type="text/javascript">
  var adminUrl="<?=URLADMIN?>";
  var mainUrl = '<?=URL?>';
  var mainCarpet = '';
  var locale="<?=$_SESSION['langCode']?>";
  var processFile = adminUrl+"assets/inc/ajaxAdmin.inc.php";
  var tokenId = "<?=$_SESSION['token']?>";
</script>

<!-- Vendor scripts -->
<script src="<?=URLADMIN?>assets/js/jquery/dist/jquery.min.js"></script>
<script src="<?=URLADMIN?>assets/js/bootstrap/js/bootstrap.min.js"></script>
<script defer src="<?=URLADMIN?>assets/pwa/a2hs.js"></script>

<!-- App scripts -->
<script defer src="<?=URLADMIN?>assets/js/luna.js"></script>

 <!-- EMPLEA REDACTOR TEXTO -->
<?php if(isset($useTextRedactor)) { ?>
<!-- Redactor is here -->
<script src="<?=URLADMIN?>assets/js/redactor/redactor.js"></script>
<script src="<?=URLADMIN?>assets/js/redactor/langs/es.js"></script>
<script src="<?=URLADMIN?>assets/js/redactor/redactor-plugins/table.js"></script>
<script src="<?=URLADMIN?>assets/js/redactor/redactor-plugins/imagemanager-no-upload.js"></script>
<script src="<?=URLADMIN?>assets/js/redactor/redactor-plugins/fontcolor.js"></script>
<script src="<?=URLADMIN?>assets/js/redactor/redactor-plugins/fullscreen.js"></script>
<script src="<?=URLADMIN?>assets/js/redactor/redactor-plugins/filemanager.js"></script>
<?php } ?>

<?php if(isset($useDropZone)) { ?>
 <!--  Galería  -->
 <script src="<?=URLADMIN?>assets/js/dropzone.min.js"></script>
<?php } ?>
<?php if(isset($useImageUpload)) { ?> 
 <script src="<?=URLADMIN?>assets/js/adminImage.js"></script>
<?php } ?>
<?php if(isset($useFileUpload)) { ?>
 <script src="<?=URLADMIN?>assets/js/adminFile.js"></script>
<?php } ?>
<?php if(isset($useVideo)) { ?>
<!--  Vídeo  -->
<script src="<?=URLADMIN?>assets/js/adminVideo.js"></script>
<?php } ?>
<?php if(isset($useDataTables)) { ?>
 <!--  DataTables  -->
<script src="<?=URLADMIN?>assets/js/datatables/datatables.min.js"></script>
<script src="<?=URLADMIN?>assets/js/datatables/dataTables.bootstrap4.min.js"></script>
<?php } ?>
<?php if(isset($useDateTimePicker)) { ?>
<!-- Datetimepicker-->
<script defer type="text/javascript" src="<?=URLADMIN?>assets/js/moment.min.js"></script>
<script defer type="text/javascript" src="<?=URLADMIN?>assets/js/tempusdominus-bootstrap-4.min.js"></script>
<?php } ?>
<?php if(isset($useCustomCheck)) { ?>
<!--CHECK-->
<script defer src="<?=URLADMIN?>assets/js/switchery/switchery.min.js"></script>
<?php } ?>
<?php if(isset($useCustomSelect)) { ?>
<!--SELECT-->
<script defer src="<?=URLADMIN?>assets/js/select2/dist/js/select2.min.js"></script>
<?php } ?>
<?php if(isset($useSweetAlert)) { ?>
<!--SELECT-->
<script defer src="<?=URLADMIN?>assets/js/sweetalert.min.js"></script>
<?php } ?>

