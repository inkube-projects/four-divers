<?php

include('include/app-init-login.php');
$useCustomCheck=true; $useCustomSelect=true; $useDateTimePicker=true; $useSweetAlert=true; $useDropZone=true; 
$plusTitle="";
$avatarImage=URLADMIN.'images/icoUser.png';
if( isset($_REQUEST['action']) && $_REQUEST['action']=="add" ){
   $action='add'; $rowDetail=array('id'=>0,'address'=>'{}','address_delivery'=>'{}'); 
}
else if( isset($_REQUEST['idObj']) && intval($_REQUEST['idObj'])>0 ) { 
 $action='update';
 $rowDetail = AdminClients::clientDetail($dbo,$_REQUEST['idObj']);
 $plusTitle=" - ".$rowDetail['name']." ".$rowDetail['surname'];
 if(strlen($rowDetail['avatar'])>0) $avatarImage = URLPUBLIC."assets/images/users/u-".$rowDetail['id']."/".$rowDetail['avatar'];
} else {
  echo "Incorrect access";
  exit();
}

$rowDetail['action']=$action;
$address = json_decode($rowDetail['address'],true);
$addressDelivery = json_decode($rowDetail['address_delivery'],true);
$provinces  = AdminBaseSection::readTable($dbo,'states', 'name', 'name','ASC', 'country_id=205');



?>
<!DOCTYPE html>
<head>
	<?php include('include/estructure/head.php'); ?>
	<link rel="stylesheet" href="<?=URLADMIN?>assets/css/user_detail.css"/>
</head>
<body>
    <!-- Wrapper-->
    <div class="wrapper">
        <?php include('include/estructure/header.php'); ?>
        <!-- Main content-->
        <section class="content">
            <div class="container-fluid" style="opacity: 1;">

                <!--FORMULARIO USUARIO-->
                <form action="#" id="detalleUsuario">
                   <input id="formToken"  type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
                   <input type="hidden" name="action" id="action" value="client-<?=$action?>" />
                   <input type="hidden" name="userId" id="userId" value="<?=$rowDetail['id']?>" />

                   <!--TITULAR  SECCION-->
                   <div class="row">
                    <div class="col-lg-12">
                        <div class="view-header">
                            <div  class="header-icon2"> 
                                <img data-filename="<?=$rowDetail['avatar']?>" id="avatarImage" src="<?=$avatarImage?>" class="avatar rounded-circle" alt="">
                            </div>
                            <div class="header-title">
                                <h3 class="m-b-xs"><?=$rowDetail['name']." ".$rowDetail['surname']?></h3>
                                <small>
                                   <span class="text-accent"> ID: <?=$rowDetail['nif']?></span>
                               </small>

                               <div class="m-xs pull-right">
                                   <div class="" style="width: 100px; height: 25px; display: inline;"><label>Newsletter </label> <input name="news" type="checkbox" class="js-switch2" <?php if($rowDetail['news']==1) echo 'checked' ?> /></div>
                                   <div class="" style="width: 100px; height: 25px; display: inline;"><label>Activo </label> <input name="active" type="checkbox" class="js-switch1" <?php if($rowDetail['active']==1) echo 'checked' ?> /></div>
                               </div>

                           </div>
                       </div>
                       <hr>
                   </div>
               </div>
               <!--END. TITULAR  SECCION-->
			   
			   
			   		<!--NEVEGACIÓN HISTORICO USUARIO-->
				   <div class="row">
					<div class="form-group col-md-12">
						<button type="submit" role="link" onclick="window.location='<?=URLADMIN?>titulaciones.php'" class="btn btn-accent pull-right css-marginR10"><i class="fa fa-bookmark"></i> <span class="bold">  titulaciones </span></button>
						<button type="submit" class="btn btn-danger pull-right css-marginR10"><i class="fa fa-shopping-cart"></i> <span class="bold">  facturas </span></button>	
						<button type="submit" class="btn btn-primary pull-right css-marginR10"><i class="fa fa-star"></i> <span class="bold">  reservas </span></button>	
					</div>
					<!--END. NEVEGACIÓN HISTORICO USUARIO-->

				  </div>

               <div class="row">

                <!--DATOS PERSONALES-->
                <div class="col-md-6">
                    <div class="panel panel-filled">
                        <div class="panel-heading">
                          <h4> Datos Básicos</h4>
                      </div>
                      <div class="panel-body">

                        <div class="row">
                            <div class="form-group col-lg-6 offset-12">
                                <label>Email (usuario)</label>
                                <input type="" required="" value="<?=$rowDetail['email']?>" id="userEmail" class="form-control" name="email">
                                <span class="form-text small alertOculto">indica el email</span>
                            </div>
                            <div class="form-group col-lg-6">
                                <label>Móvil</label>
                                <input type="tel" value="<?=$rowDetail['mobile_phone']?>" id="userMobile" class="form-control" name="mobile_phone">
                                <span class="form-text small alertOculto">indica el número de teléfono</span>
                            </div>
                            <div class="form-group col-lg-6">
                                <label>Nombre</label>
                                <input type="text" value="<?=$rowDetail['name']?>" id="userName" class="form-control" name="name">
                                <span class="form-text small alertOculto">indica el nombre</span>
                            </div>
                            <div class="form-group col-lg-6">
                                <label>Apellidos</label>
                                <input type="text" value="<?=$rowDetail['surname']?>" id="userSurname" class="form-control" name="surname">
                                <span class="form-text small alertOculto">indica los apellidos</span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <!--DATOS EMERGENCIAS-->
            <div class="col-md-6">
                <div class="panel panel-filled">
                    <div class="panel-heading">
                      <h4> Emergencias</h4>
                  </div>
                  <div class="panel-body">

                    <div class="row">
                        <div class="form-group col-lg-6">
                            <label>Contacto</label>
                            <input type="text" required="" value="<?=$rowDetail['contact_full_name']?>" id="contactName" class="form-control" name="contact_full_name">
                            <span class="form-text small alertOculto">indica el nombre y apellidos emergencias</span>
                        </div>
                        <div class="form-group col-lg-6">
                            <label>Móvil</label>
                            <input type="text" value="<?=$rowDetail['contact_phone']?>" id="contactPhone" class="form-control" name="contact_phone">
                            <span class="form-text small alertOculto">indica el número de teléfono emergencias</span>
                        </div>
                        <div class="form-group col-lg-12">
                            <label> Comentarios</label>
                            <input type="text" value="<?=$rowDetail['contact_comments']?>" id="contactComments" class="form-control" name="contact_comments">
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!--END DATOS EMERGENICAS-->

        <!--SALUD, MEDIDAS Y TALLAS-->
        <div class="col-md-12">
            <div class="panel panel-filled">
                <div class="panel-heading">
                  <h4>Datos personales, medidas y salud</h4>
              </div>
              <div class="panel-body">

                <div class="row">


                  <div class="form-group col-xl-4">
                    <label>Profesión:</label>
                    <input type="text" value="<?=$rowDetail['profession']?>" id="userProfession" class="form-control" name="profession">
                    <span class="form-text small alertOculto">indica la profesión</span>
                </div>
                <div class="form-group col-xl-4">											
                   <label>Fecha de nacimiento</label>
                   <div class="input-group date" id="datetimepicker-birthdate" data-target-input="nearest">
                     <input name="birthdate" value="<?=$rowDetail['birthdate']?>" type="text" class="form-control datetimepicker-input" data-target="#datetimepicker-birthdate"/>
                     <div class="input-group-append" data-target="#datetimepicker-birthdate" data-toggle="datetimepicker">
                      <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                  </div>
              </div>
          </div>
          <div class="form-group col-xl-4">
            <label>Cuerpo especial</label>
            <select class="select2_demo_1 form-control" style="width: 100%" id="userProfessionSpecial"  name="profession_special" >
                <option value=""></option>
                <option value="1">Policia Local</option>
                <option value="2">Ejército</option>
                <option value="3">Guardía Civil</option>
                <option value="4">Policia Nacional</option>
                <option value="5">Bomberos</option>
            </select>
        </div>

        <div class="form-group col-6 col-sm-3 col-md-3 col-lg-2 col-xl-1">
            <label>Altura (cm)</label>
            <input type="text" value="<?=$rowDetail['height']?>" id="userHeight" class="form-control" name="height">
            <span class="form-text small alertOculto">indica la altura en cm. </span> 
        </div>
        <div class="form-group col-6 col-sm-3 col-md-3 col-lg-2 col-xl-1">
            <label>Peso (kg)</label>
            <input type="text" value="<?=$rowDetail['weight']?>" id="userWeight" class="form-control" name="weight">
            <span class="form-text small alertOculto">indica la peso en Kg.</span>
        </div>
        <div class="form-group col-6 col-sm-3 col-md-3 col-lg-2 col-xl-1">
            <label>Edad</label>
            <input type="text" value="<?=$rowDetail['age']?>" id="userAge" class="form-control" name="age">
            <span class="form-text small alertOculto">indica la edad</span>
        </div>
        <div class="form-group col-6 col-sm-3 col-md-3 col-lg-2 col-xl-1">
            <label>Sexo</label>
            <select  class="form-control"  id="userGender" name="gender">
                <option value=""></option>
                <option <?php if($rowDetail['gender']==1) echo 'selected' ?> value="1">M</option>
                <option <?php if($rowDetail['gender']==2) echo 'selected' ?> value="2">F</option>
            </select>
            <span class="form-text small alertOculto">indica el sexo</span>
        </div>

        <div class="form-group col-lg-6 col-xl-4">
            <label>Alguna enfermedad</label>
            <input type="text" value="<?=$rowDetail['diseases']?>" id="userDiseases" class="form-control" name="diseases">
            <span class="form-text small alertOculto">indica si padece alguna o varias enfermedades </span> 
        </div>

        <div class="form-group col-lg-6 col-xl-4">
            <label>Alergias</label>
            <input type="text" value="<?=$rowDetail['allergies']?>" id="userAllergies" class="form-control" name="allergies">
            <span class="form-text small alertOculto">indica si padece alguan o varias alergias</span> 
        </div>

        <div class="col-12 css-marginB20"><h4> Tallas</h4></div>

        <!-- TRAJES id en tabla products_categories = 1 -->
        <div class="form-group col-6 col-sm-3 col-md-3 col-lg-2 col-xl-1">
            <label>Traje</label>
            <select placeholder=" " class="form-control" id="size-equipment-1" name="equipment-1">
                <option value=""></option>
                <option value="T1">T1</option>
                <option value="T2">T2</option>
                <option value="T3">T3</option>
                <option value="T4">T4</option>
                <option value="T5">T5</option>
                <option value="T6">T6</option>
                <option value="T7">T7</option>
            </select>
            <span class="form-text small alertOculto">indica la talla</span>
        </div>
        <!-- JACKETS id en tabla products_categories = 6 -->
        <div class="form-group col-6 col-sm-3 col-md-3 col-lg-2 col-xl-1">
            <label>Jacket</label>
            <select class="form-control" id="size-equipment-6" name="equipment-6">
                <option value=""></option>
                <option value="XXS">XXS</option>
                <option value="XS">XS</option>
                <option value="S">S</option>
                <option value="M">M</option>
                <option value="L">L</option>
            </select>
            <span class="form-text small alertOculto">indica la talla</span>
        </div>
        <!-- ALETAS id en tabla products_categories = 7 -->
        <div class="form-group col-6 col-sm-3 col-md-3 col-lg-2 col-xl-1">
            <label>Aletas</label>
            <select class="form-control" id="size-equipment-7" name="equipment-7">
                <option value=""></option>
                <option value="XXS">XXS</option>
                <option value="XS">XS</option>
                <option value="S">S</option>
                <option value="M">M</option>
                <option value="L">L</option>
            </select>
            <span class="form-text small alertOculto">indica la talla</span>
        </div>
        <!-- ESCARPINES id en tabla products_categories = 9 -->
        <div class="form-group col-6 col-sm-3 col-md-3 col-lg-2 col-xl-1">
            <label>Escarpines</label>
            <select class="form-control" id="size-equipment-9" name="equipment-9">
                <option value=""></option>
                <?php for($i=33; $i<=45; $i++) { echo '<option value="'.$i.'-'.($i+1).'">'.$i.'-'.($i+1).'</option>'; } ?>
            </select>
            <span class="form-text small alertOculto">indica la talla</span>
        </div>
        <!-- CALZANTES id en tabla products_categories = 10 -->
        <div class="form-group col-6 col-sm-3 col-md-3 col-lg-2 col-xl-1">
            <label>Calzantes</label>
            <select class="form-control" id="size-equipment-10" name="equipment-10">
             <option value=""></option>
             <?php for($i=33; $i<=45; $i++) { echo '<option value="'.$i.'-'.($i+1).'">'.$i.'-'.($i+1).'</option>'; } ?>
         </select>
         <span class="form-text small alertOculto">indica la talla</span>
     </div>
     <!-- BOTELLAS id en tabla products_categories = 2 -->
     <div class="form-group col-6 col-sm-3 col-md-3 col-lg-2 col-xl-1">
        <label>Botellla</label>
        <select class="form-control" id="size-equipment-2" name="equipment-2">
            <option value=""></option>
            <option value="10">10</option>
            <option value="12">12</option>
            <option value="15">15</option>
        </select>
    </div>

    <div class="form-group col-lg-12 col-xl-6">
        <label>Comentarios</label>
        <input type="text" value="<?=$rowDetail['equipment_comment']?>" id="userEquipmentComment" class="form-control" name="equipment_comment">
    </div>

</div>

</div>
</div>
</div>
<!--SALUD, MEDIDAS Y TALLAS-->


<!--TITULACIONES-->
<div class="col-md-6">
    <div class="panel panel-filled">
        <div class="panel-heading">
          <h4> Experiencia</h4>
      </div>
      <div class="panel-body">

        <div class="row">

		<div class="form-group col-lg-8">
			<label>Titulación</label>
			<select class="select2_demo_3 form-control" multiple="multiple" id="userCourses"  name="courses[]"  style="width: 100%">
				<option value="OWD">Open Water Dive</option>
				<option value="AOWD">Advanced Open Water</option>
				<option value="EFR">Emergency First</option>
				<option value="RESCUE">Rescuer Diver</option>
				<option value="DIVEMASTER">Divemaster</option>
				<option value="NAVLICENSE">Licencia Navegación</option>
				<option value="SDJUNIOR">Scuba diver Junior</option>
				<option value="NITROX">Nitrox</option>
			</select>
			<span class="form-text small alertOculto">indica las titulaciones</span> 
		</div>
		
		<div class="form-group col-md-4">
			<label>Certificadora</label>
			<select class="select2_demo_1 form-control"  id="userKnownFor"  name="known_for"  style="width: 100%">
				<option value=""> </option>
				<option value="PADI">PADI</option>
				<option value="SSI">SSI</option>
				<option value="FEDAS">FEDAS</option>
				<option value="ACUC">ACUC</option>
				<option value="PROTEC">PROTEC</option>
				<option value="OTROS">Otros</option>
			</select>
			<span class="form-text small alertOculto">indica como nos has conocido</span> 
		</div>
		
		<div class="form-group col-lg-8">
			<label>Actividades</label>
			<select class="select2_demo_3 form-control" multiple="multiple" id="userActividad"  name="userActividad"  style="width: 100%">
				<option value="SNORKEL">Snorkel</option>
				<option value="BAUTIZO">Bautizo</option>
				<option value="INMERSIÓN">Inmersiones</option>
			</select>
			<span class="form-text small alertOculto">indica las actividades</span> 
		</div>
		
	   <div class="form-group col-md-4">
        <label>Inmersiones</label>
        <input type="text" value="<?=$rowDetail['immersion_number']?>" id="userInmmersionNumber" class="form-control" name="immersion_number">
    </div>

    <div class="form-group col-md-12">
        <label>Otros</label>
        <input type="text" value="<?=$rowDetail['courses_other']?>" id="userCoursesOther" class="form-control" name="courses_other">
    </div>

	</div>

	</div>
	</div>
</div>
<!--END TITULACIONES-->


<!--CONOCIDO DEPORTES-->
<div class="col-md-6">
    <div class="panel panel-filled">
        <div class="panel-heading">
          <h4>RSS y Deportes</h4>
      </div>
      <div class="panel-body">

        <div class="row">

		<div class="form-group col-md-12">
			<label>Conocicdo</label>
			<select class="select2_demo_1 form-control"  id="userKnownFor"  name="known_for"  style="width: 100%">
				<option value=""></option>
				<option value="1">Facebook</option>
				<option value="6">Twitter</option>
				<option value="3">Instagram</option>
				<option value="2">Web</option>
				<option value="4">Foro</option>
				<option value="5">Amig@</option>
				<option value="7">Otros</option>
			</select>
			<span class="form-text small alertOculto">indica como nos has conocido</span> 
		</div>
		
		<div class="form-group col-lg-6">
			<label>Instagram</label>
			<input type="tel" value="" id="instagramEmerg" class="form-control" name="instagramEmerg">
			<span class="form-text small alertOculto">indica instagram</span>
		</div>
		<div class="form-group col-lg-6">
			<label> Facebook</label>
			<input type="tel" value="" id="facebookEmerg" class="form-control" name="facebookEmerg">
			<span class="form-text small alertOculto">indica facebook</span>
		</div>

    <div class="form-group col-md-12">
       <label>Deportes</label>
       <select class="select2_demo_3 form-control" multiple="multiple" id="userSports"  name="sports[]"  style="width: 100%">
        <?php $sports = AdminBaseSection::readTable($dbo,'sports', 'id, name', 'name');
        foreach ($sports as $key => $value) { echo ' <option value="'.$value['id'].'">'.$value['name'].'</option>'; }
        ?>
    </select>
</div>

</div>

</div>
</div>
</div>
<!--END CONOCIDO DEPORTES-->


<!--SEGURO-->
<div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
    <div class="panel panel-filled">
        <div class="panel-heading">
          <h4>Seguro</h4>
      </div>
      <div class="panel-body">

        <div class="row">

          <div class="form-group col-xs-12 col-md-6 col-lg-4">
            <label>Fecha vencimiento seguro</label>

            <div class="input-group date" id="datetimepicker-insurance" data-target-input="nearest">
             <input name="insurance_validity_date" value="<?=$rowDetail['insurance_validity_date']?>" type="text" class="form-control datetimepicker-input" data-target="#datetimepicker-insurance"/>
             <div class="input-group-append" data-target="#datetimepicker-insurance" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
      </div>
  </div>

  <div class="form-group col-xs-12 col-md-6 col-lg-4">
    <label>Póliza Seguro</label>
    <input type="text" value="<?=$rowDetail['insurance_number']?>" id="userInsuranceNumber" class="form-control" name="insurance_number">
    <span class="form-text small alertOculto">indica la póliza</span> 
</div>

<div class="form-group col-lg-4">
  <label>Adjuntar Seguro</label><br>
  <label class="btn btn-accent" for="file-selector2">
   <input id="file-selector2" type="file" style="display:none;" onchange="$('#upload-file-info2').html($(this).val());">
   <span id="upload-file-info2">subir archivo</span>
</label>
</div>


</div>

</div>
</div>
</div>
<!--END.SEGURO-->


<!--REVISIÓN  MÉDICO-->
<div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
    <div class="panel panel-filled">
        <div class="panel-heading">
          <h4>Revisión Médica</h4>
      </div>
      <div class="panel-body">

        <div class="row">

           <div class="form-group col-xs-12 col-md-6 col-lg-4">
            <label>Válida hasta</label>
            <div class="input-group date" id="datetimepicker-medicalCheck" data-target-input="nearest">
             <input name="medical_revision_validity_date" value="<?=$rowDetail['medical_revision_validity_date']?>" type="text" class="form-control datetimepicker-input" data-target="#datetimepicker-medicalCheck"/>
             <div class="input-group-append" data-target="#datetimepicker-medicalCheck" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
      </div>
  </div>

  <div class="form-group col-xs-12  col-md-6 col-lg-4">
      <label>Adjuntar Revisión</label><br>
      <label class="btn btn-accent" for="file-selector3">
       <input id="file-selector3" type="file" style="display:none;" onchange="$('#upload-file-info2').html($(this).val());">
       <span id="upload-file-info3">subir archivo</span>
   </label>
</div>

</div>

</div>
</div>
</div>
<!--END.REVISIÓN MÉDICO-->


<!--DATOS DE FACTURACIÓN-->

<div class="col-md-6">
    <div class="panel panel-filled">
        <div class="panel-heading">
          <h4> Datos Facturación</h4>
      </div>
      <div class="panel-body">

        <div class="row">
            <div class="form-group col-xl-4">
                <label>NIF/CIF</label>
                <input type="text" value="<?=$address['nif']?>" id="address_nif" class="form-control" name="address_nif">
            </div>
            <div class="form-group col-xl-8">
                <label>Empresa/Nombre Apellidos</label>
                <input type="tel" value="<?=$address['name']?>" id="address_name" class="form-control" name="address_name">
            </div>
            <div class="form-group col-xl-8">
                <label>Dirección</label>
                <input type="text" value="<?=$address['address']?>" id="address_address" class="form-control" name="address_address">
            </div>
            <div class="form-group col-xl-2">
                <label>Nº - Puerta</label>
                <input type="text" value="<?=$address['number']?>" id="address_number" class="form-control" name="address_number">
            </div>
            <div class="form-group col-xl-2">
                <label>C.P.</label>
                <input type="text" value="<?=$address['cp']?>" id="address_cp" class="form-control" name="address_cp">
            </div>
            <div class="form-group col-xl-4">
                <label>Población</label>
                <input type="text" value="<?=$address['city']?>" id="address_city" class="form-control" name="address_city">
            </div>
            <div class="form-group col-xl-4">
               <label>Provincia</label>
               <select class="select2_demo_1 form-control" id="address_province"  name="address_province"  style="width: 100%">
                <option value=""></option>
                <?php 
                foreach ($provinces as $key => $value) { echo '<option value="'.$value['name'].'">'.$value['name'].'</option>'; }
                ?>
            </select>
        </div>
        <div class="form-group col-xl-4">
            <label>Pais</label>
            <input type="text" value="<?=$address['country']?>" id="address_country" class="form-control" name="address_country">

        </div>
        <div class="form-group col-md-12">
            <label>Comentarios facturación</label>
            <input type="text" value="<?=$address['comments']?>" id="address_comments" class="form-control" name="address_comments">
        </div>
    </div>

</div>
</div>
</div>
<!--END DATOS FACTURACION-->



<!--DATOS DE ENVIO-->
<div class="col-md-6">
    <div class="panel panel-filled">
        <div class="panel-heading">
          <h4> Datos Envio</h4>
      </div>
      <div class="panel-body">

        <div style=" position: absolute; right: 50px; top: 20px">
            <div class="form-check abc-checkbox abc-checkbox-warning">
                <input name="only_one_address" class="form-check-input" id="userOnlyOneAddress" type="checkbox" <?php if($rowDetail['only_one_address']==1) echo 'checked=""'; ?> >
                <label class="form-check-label" for="userOnlyOneAddress">
                    Igual a factura
                </label>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-xl-4">
                <label>NIF/CIF</label>
                <input type="text" value="<?=$addressDelivery['nif']?>" id="address_delivery_nif" class="form-control" name="address_delivery_nif">
            </div>
            <div class="form-group col-xl-8">
                <label>Empresa/Nombre Apellidos</label>
                <input type="tel" value="<?=$addressDelivery['name']?>" id="address_delivery_name" class="form-control" name="address_delivery_name">
            </div>
            <div class="form-group col-xl-8">
                <label>Dirección</label>
                <input type="text" value="<?=$addressDelivery['address']?>" id="address_delivery_address" class="form-control" name="address_delivery_address">
            </div>
            <div class="form-group col-xl-2">
                <label>Nº - Puerta</label>
                <input type="text" value="<?=$addressDelivery['number']?>" id="address_delivery_number" class="form-control" name="address_delivery_number">
            </div>
            <div class="form-group col-xl-2">
                <label>C.P.</label>
                <input type="text" value="<?=$addressDelivery['cp']?>" id="address_delivery_cp" class="form-control" name="address_delivery_cp">
            </div>
            <div class="form-group col-xl-4">
                <label>Población</label>
                <input type="text" value="<?=$addressDelivery['city']?>" id="address_delivery_city" class="form-control" name="address_delivery_city">
            </div>
            <div class="form-group col-xl-4">
               <label>Provincia</label>
               <select class="select2_demo_1 form-control" id="address_delivery_province"  name="address_delivery_province"  style="width: 100%">
                <option value=""></option>
                <?php 
                foreach ($provinces as $key => $value) { echo '<option value="'.$value['name'].'">'.$value['name'].'</option>'; }
                ?>
            </select>
        </div>
        <div class="form-group col-xl-4">
            <label>Pais</label>
            <input type="text" value="<?=$addressDelivery['country']?>" id="address_delivery_country" class="form-control" name="address_delivery_country">

        </div>
        <div class="form-group col-md-12">
            <label>Comentarios envíos</label>
            <input type="text" value="<?=$addressDelivery['comments']?>" id="address_delivery_comments" class="form-control" name="address_delivery_comments">
        </div>
    </div>

</div>
</div>
</div>
<!--END DATOS ENVIO-->

<div class="btnFixed">
	<div class="cnt_btnFixed">
		<a class="btn btn-accent save-user-btn" type="button" data-toggle="tooltip"  title="guardar"><i class="fa fa-repeat"></i></a>
		<a href="<?=URL?>LISTADO-CLIENTES" class="btn btn-primary"  data-toggle="tooltip"  title="volver"><i class="fa fa-step-backward"></i></a>
	</div>
</div>

<div class="form-group col-md-12">
	<a href="#" class="btn btn-accent save-user-btn"> guardar datos </a>
	<a href="#" class="btn btn-danger delete-user-btn"> x borrar perfil</a>
	<a href="<?=URL?>LISTADO-CLIENTES" class="btn btn-primary"> < volver al listado </a>
</div>

</div>

</form>
<!--END FORMULARIO USUARIO-->

<?php include_once("include/modals/common-modals.php"); ?>
</div>
</section>
<!-- End main content-->

</div>
<!-- End wrapper-->

<?php 
include('include/js.php'); 
?>
<script>
    var userDetail=<?=json_encode($rowDetail)?>;
    console.log(userDetail);
</script>

<script type="text/javascript" src="<?=URLADMIN?>assets/js/admin/users/user-detail.js"></script>

</body>

</html>