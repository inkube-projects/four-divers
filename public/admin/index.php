<?php include('include/app-init-login.php'); ?>
<!DOCTYPE html>
<html>
<head>
	  <?php include('include/estructure/head.php'); ?>
<style>

.panel p{
    color: #fff;
}

.tt-query {
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
     -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
}

.tt-hint {
  color: #999
}

.tt-menu {    /* used to be tt-dropdown-menu in older versions */
  width: 422px;
  margin-top: 4px;
  padding: 4px 0;
  background-color: #fff;
  border: 1px solid #ccc;
  border: 1px solid rgba(0, 0, 0, 0.2);
  -webkit-border-radius: 4px;
     -moz-border-radius: 4px;
          border-radius: 4px;
  -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
     -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
          box-shadow: 0 5px 10px rgba(0,0,0,.2);
}

.tt-suggestion {
  padding: 3px 20px;
  line-height: 24px;
}

.tt-suggestion.tt-cursor,.tt-suggestion:hover {
  color: #fff;
  background-color: #0097cf;
}

.tt-suggestion p {margin: 0;}
.twitter-typeahead{ width:100% }

</style>
</head>
<body>

<!-- Wrapper-->
<div class="wrapper">
	 <?php include('include/estructure/header.php'); ?>


    <!-- Main content-->
    <section class="content">
        <div class="container-fluid">

				   <div class="row">
                <div class="col-lg-12">
                <div class="view-header">
 
                        <div class="header-icon">
                            <i class="pe page-header-icon pe-7s-keypad"></i>
                        </div>
                        <div class="header-title">
                            <h3 class="m-b-xs">Inicio/Escritorio</h3>
                            <small>
                                Información más relevante de lo que ocurre en la aplicación.
                            </small>
                        </div>
                    </div>
                    <hr>
                </div>
            </div>	
			
	<div class="row">
  
	
						
            </div> <!-- .row -->
			


			
				


        </div> <!-- .container-fluid -->
    </section> <!-- End main content-->

</div>
<!-- End wrapper-->

 <?php include('include/js.php'); ?>
 <script type="text/javascript"> 
 var ruta="<?=URLADMIN?>"; var tokenId="<?=$_SESSION['token']?>"; 
 $(document).ready(function () {
		$('body').addClass('pace-done'); 
 });

 $('#nav-dashboard').addClass('active');
 </script>

</body>

</html>