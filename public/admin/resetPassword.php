<?php

session_start();
$adminArea=true;

$_SESSION['USER_TYPE']="ADMIN";	
include_once '../../sys/core/init.inc.php';

$_SESSION['company']=array();
// configuración Login System Afiliados o Administradores


if(!isset($_REQUEST['cCode'])){
	header("Location: ACCESO-INCORRECTO-ADMIN");
	exit;
	} 
	 if(!isset($_SESSION['company']['code'])){
		$_SESSION['company']['code'] = $_REQUEST['cCode']; 
	}



if( strpos($_SERVER[REQUEST_URI],"STORE-RESET-PASSWORD")!== false ) {
	
	include_once '../../sys/config/LS-store-cred.inc.php';
	$_SESSION['USER_TYPE']="STORE";
	} else {
	include_once '../../sys/config/LS-admin-cred.inc.php';
	}
	
	

$fp=$LS->forgotPassword();
if($fp[0]=="Error"){
	header("Location: ACCESO-INCORRECTO-ADMIN");
	exit;
	}	

	
function curPageURL() {
    $pageURL = 'http';
    if(isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on"){$pageURL .= "s";}
    $pageURL .= "://";
    if($_SERVER["SERVER_PORT"] != "80") {
      $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
    }else{
      $pageURL .= $_SERVER["SERVER_NAME"];
    }
    return $pageURL;
  }
  
 
?>
<!DOCTYPE html>
<html lang="es">
  <head>
  <?php include('include/estructure/head.php'); ?>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="admin">
  <meta name="author" content="Inkube">
  <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
  <title>ADMIN RESET PASSWORD </title>
  <style>

.bg { background: #02041a url(<?=URLADMIN?>images/bg_login.jpg) no-repeat center center; }
		
.bgDark { background-color: rgba(0,0,0,0.87)}
		
.container-center {
    max-width: 400px;
    margin: 5% auto 10% auto;
    padding: 20px;
}
		
	.panel.panel-filled {
    background-color: rgba(24, 69, 86, 0.9);
}
		
	.form-control {
    background-color: #000;
}

	.btn-accent {
    color: #FFF;
    background-color: #f4a735;
    border-color: #f4a735;
}
	


</style>
  </head>

	<body class="blank bg">
    
    <!-- Wrapper-->
<div class="wrapper">


   
       
        <div class="container-center animated slideInDown">
        
        
			<div class="col-xs-12 text-center mx-auto" style="margin-bottom: 40px;">
			<img src="<?=URLADMIN?>images/logo.png" width="200" height="125" alt=""/>
				</div>
			
<div class="view-header">
          <div class="header-icon">
                    <i class="pe page-header-icon pe-7s-unlock"></i>
                </div>
                <div class="header-title">
                    <h3>Contraseña</h3>
                    <small>
                       Acceso autorizado. Ahora puede cambiarla. 
                    </small>
                </div>
            </div>


           

            <div class="panel panel-filled">
                <div class="panel-body">
                     <?php  if( $fp[1]=="resetPasswordForm" ) { ?> 

          <form id="reset-password-form" action='javascript:;' method='POST'>
            <input type='hidden' name='tokenfp' value='<?=$fp[2]?>' />
            <input type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
            <input type="hidden" name="action" value="user-reset-password" />
            <input type="hidden" name="logSysForgotPassChange" value="1" />
            <div class="form-group">
             <label >Nueva contraseña</label>
              <input class="form-control"  type='password' name='logSysForgotPassNewPassword' />
            </div>
             <div class="form-group">
            <label>Repita contraseña</label>
             <input class="form-control"  type='password' name='logSysForgotPassRetypedPassword'/>
            </div>
            <p><button type="submit" class="btn btn-success btn-lg" id="submitBtn" name='logSysForgotPassChange'>Modificar contraseña</button></p>
          </form>

<?php } ?> 
                </div>
            </div>

        </div>
    
 <p class="sign__extra text-muted text-center">
             &copy; FOUR-DIVERS. All rights reserved.
            </p>
</div>
<!-- End wrapper-->
    
    
    
    
    
    
    
    
    
    
    
    
    
  
	<!-- JavaScript
    ================================================== -->
    <?php include_once("include/js.php") ?>
    <script type="text/javascript"> var ruta="<?=URLADMIN?>";</script>
    <script type="text/javascript" src="<?=URLADMIN?>assets/js/initAdmin.js"></script>
  </body>
</html>