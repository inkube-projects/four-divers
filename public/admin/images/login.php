<?php 
$adminArea=true;
$autoInitLoginSystem=false;
require_once '../../sys/core/init.inc.php';

$_SESSION['USER_TYPE']="ADMIN";  require_once '../../sys/config/LS-admin-cred.inc.php';  

?>


<!DOCTYPE html>
<html>
<head>
<?php include('include/head.php'); ?>
<style>
.bg {
   background: #02041a url(public/admin/images/bg_login.jpg) contain ;
}
.bgDark {
background-color: rgba(0,0,0,0.87)
}
.container-center {
    max-width: 400px;
    margin: 5% auto 10% auto;
    padding: 20px;
}
.panel.panel-filled {
    background-color: rgba(63, 8, 22, 0.9);
}		
.form-control {
    background-color: #000;
}
.btn-accent {
    color: #FFF;
    background-color: #f4a735;
    border-color: #f4a735;
}
</style>
</head>
<body class="blank bg">

<!-- Wrapper-->
<div class="wrapper">
    <!-- Main content-->
    <section class="">
        <div class="container-center ">
			

			<div class="col-xs-12 text-center mx-auto" style="margin-bottom: 40px;">
			<img src="<?=URLADMIN?>images/logo.png" width="200" height="125" alt=""/>
				</div>
			
<div class="view-header">
          <div class="header-icon">
                    <i class="pe page-header-icon pe-7s-unlock"></i>
                </div>
                <div class="header-title">
                    <h3>Acceso Admin</h3>
                    <small>
                        Bienvenido a tu admin.
                    </small>
                </div>
            </div>

            <div class="panel panel-filled">
                <div class="panel-body">
                     <form id="login-form" action="" method="POST" >
                     <input type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" /> 
                       <input type="hidden" name="action" value="user_login" />
                        <div class="form-group">
                            <label for="c_email">Usuario</label>
                            <input class="form-control" id="c_email" name="login" type="text" required />
                                     
                        </div>
                        <div class="form-group">
                            <label class="col-form-label" for="password">Contraseña</label>
                           <input class="form-control" id="c_password" name="password" type="password" required />
                        </div>
                        <div>
                            <button id="submitBtn" class="btn btn-w-md btn-accent">Acceder</button>
                        </div>
                    </form>
                </div>
            </div>
			


        </div>
    </section>
    <!-- End main content-->

</div>
<!-- End wrapper-->

 <?php include('include/js.php'); ?>
 <script type="text/javascript"> var ruta="<?=URLADMIN?>";</script>
    <script type="text/javascript" src="<?=URLADMIN?>assets/js/initAdmin.js"></script>
</body>

</html>