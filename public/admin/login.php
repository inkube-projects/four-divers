<?php
    $adminArea=true;
    $autoInitLoginSystem=true;
    require_once '../../sys/core/init.inc.php';
    $_SESSION['company']['code']="FOUR-DIVERS";

    function curPageURL() {
        $pageURL = 'http';
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") { $pageURL .= "s"; }
        $pageURL .= "://";
        $pageURL .= $_SERVER["SERVER_NAME"];
        return $pageURL;
    }
  
    $_SESSION['USER_TYPE']="ADMIN";  require_once '../../sys/config/LS-admin-cred.inc.php';  
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('include/estructure/head.php'); ?>
        <style>
            .bg { background: #02041a url(<?=URLADMIN?>images/bg_login.jpg) no-repeat center center; }
            .bgDark { background-color: rgba(0,0,0,0.87)}
		
            .container-center {
                max-width: 400px;
                margin: 5% auto 10% auto;
                padding: 20px;
            }
		
	        .panel.panel-filled {
                background-color: rgba(24, 69, 86, 0.9);
            }
		
	        .form-control {
                background-color: #000;
            }

	        .btn-accent {
                color: #FFF;
                background-color: #f4a735;
                border-color: #f4a735;
            }
        </style>
    </head>
        
    <body class="blank bg">
        <!-- Wrapper-->
        <div class="wrapper">
            <!-- Main content-->
            <section class="">
                <div class="container-center animated slideInDown">
                    <div class="col-xs-12 text-center mx-auto" style="margin-bottom: 40px;">
                        <img src="<?=URLADMIN?>images/logo.png" width="200" height="125" alt=""/>
                    </div>
        
                    <div class="view-header">
                        <div class="header-icon">
                            <i class="pe page-header-icon pe-7s-unlock"></i>
                        </div>
                        
                        <div class="header-title">
                            <h3>Acceso Admin</h3>
                            <small>Bienvenido a tu admin.</small>
                        </div>
                    </div>

                    <div class="panel panel-filled">
                        <div class="panel-body">
                            <form id="login-form" action="" method="POST" >
                                <input type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" /> 
                                <input type="hidden" name="action" value="user_login" />
                                <div class="form-group">
                                    <label for="c_email">Usuario</label>
                                    <input class="form-control" id="c_email" name="login" type="text" required />
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-form-label" for="password">Contraseña</label>
                                    <input class="form-control" id="c_password" name="password" type="password" required />
                                </div>
                                
                                <div>
                                    <button id="submitBtn" class="btn btn-w-md btn-accent">Acceder</button>
                                </div>
                            </form>
                        </div>
                
                
                        <div class="panel-footer">
                            <a href="#" data-toggle="collapse" data-target="#sign__resend-password">¿Has olvidado tu contraseña?</a>
                            <div class="collapse" id="sign__resend-password">
                                <form action='javascript:;' method='POST'  id="reset-password-form" class="form-inline">
                                    <input type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
                                    <input type="hidden" name="action" value="user-reset-password" />
                                    <input type="hidden" name="url" value="<?=curPageURL()?>/<?=$_SESSION['USER_TYPE']?>-RESET-PASSWORD" />
                                    <input type="hidden" name="code" value="FOUR-DIVERS" />
                                    <input type="hidden" name="webId" value="99" />
                                    <input type="hidden" name="logSysForgotPass" value="1" />
                                    
                                    <div class="form-group" style="margin-top:5px; margin-right:10px">
                                        <label class="sr-only">Introduce tu email</label>
                                        <input id='logSysIdentification'  name='identification' required type="email" class="form-control" placeholder="Introduce tu email">
                                    </div>
                                    
                                    <button type="submit" id="submitBtn" name='logSysForgotPass' class="btn btn-default">Reset</button>
                                </form>
                            </div>
                        </div>
                    </div>
		        </div>
            </section>
            <!-- End main content-->
        </div>
        <!-- End wrapper-->

        <?php include('include/js.php'); ?>
        <script type="text/javascript"> var ruta="<?=URLADMIN?>";</script>
        <script type="text/javascript" src="<?=URLADMIN?>assets/js/initAdmin.js"></script>
    </body>
</html>