<?php 
session_start();
$adminArea=true;
$_SESSION['USER_TYPE']="ADMIN";	
include_once '../../sys/core/init.inc.php';
?>

<!DOCTYPE html>
<html lang="es">
  <head>
  <?php include('include/head.php'); ?>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="admin">
  <meta name="author" content="Inkube">
  <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
  <title>ADMIN RESET PASSWORD </title>
  </head>

	<body class="blank">
    
    <!-- Wrapper-->
<div class="wrapper">


   
       
       <div class="container-center md animated slideInDown">

            <div class="view-header">
                <div class="header-icon">
                    <i class="pe page-header-icon pe-7s-close-circle"></i>
                </div>
                <div class="header-title">
                    <h3>404</h3>
                    <small>
                        Page Not Found
                    </small>
                </div>
            </div>

            <div class="panel panel-filled">
                <div class="panel-body">
                   Acceso incorrecto o página no encontrada

                </div>
            </div>
            <div>
                <a href="<?=URL?>FOUR-DIVERS_ADMIN" class="btn btn-accent">Back to app</a>
            </div>

        </div>
    
    

  
	<!-- JavaScript
    ================================================== -->
    <?php include_once("include/js.php") ?>
 
  </body>
</html>