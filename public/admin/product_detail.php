<?php

include('include/app-init-login.php');
$useCustomCheck=true; $useCustomSelect=true;  $useSweetAlert=true; $useDropZone=true; $useTextRedactor=true;
if( isset($_REQUEST['action']) && $_REQUEST['action']=="add" ){
 $action='add'; $rowDetail=array('id'=>0,'specs'=>'{}'); 
}
else if( isset($_REQUEST['idObj']) && intval($_REQUEST['idObj'])>0 ) { 
 $action='update';
 $rowDetail = AdminProducts::productDetail($dbo,$_REQUEST['idObj']);
 $specs = json_decode($rowDetail['specs'],true);
 $plusTitle=" - ".$rowDetail['name'];
} else {
  echo "Incorrect access";
  exit();
}

$rowDetail['action']=$action;


?>
<!DOCTYPE html>
<head>
  <?php include('include/estructure/head.php'); ?>
  <style>
    .dropzone {
    border: none;
    background: #494b54;
  }
  .dropzone .dz-preview.dz-image-preview { background: none; }
  .dropzone .dz-preview .dz-image {
    width: 200px;
    height: 200px;
    margin-bottom: 5px;
}

#redactor-modal-image-edit label {
  color: #484848;
  font-family: 'Roboto';

}
    
  </style>
  }
</head>

<!-- Wrapper-->
<div class="wrapper">
 <?php include('include/estructure/header.php'); ?>
 <!-- Main content-->
 <section class="content">
  <div class="container-fluid" style="opacity: 1">

   <!--FORMULARIO USUARIO-->
   <form action="#" id="detalleProducto">
     <input id="formToken"  type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
     <input type="hidden" name="action" id="action" value="product-<?=$action?>" />
     <input type="hidden" name="productId" id="productId" value="<?=$rowDetail['id']?>" />
     <input type="hidden" data-seoname="products" id="adminSection" name="adminSection" value="8.0" />

     <!--TITULAR  SECCION-->
     <div class="row">
      <div class="col-lg-12">
        <div class="view-header">

          <div class="header-icon">
            <i class="pe page-header-icon pe-7s-albums"></i>
          </div>
          <div class="header-title">
            <h3 class="m-b-xs">Catálogo</h3>
            <small>
              Detalle Producto 
            </small>
            <a  href="<?=URL?>LISTADO-PRODUCTOS" style="font-size: 24px; position: absolute; top:0px; right:30px; color: #fff;"><i class="pe page-header-icon pe-7s-back-2"></i></a>

            <div class="m-xs pull-right">
              <div class="" style="width: 100px; height: 25px; display: inline;"><label>Destacado </label> <input name="featured" type="checkbox" class="js-switch2" <?php if($rowDetail['featured']==1) echo 'checked' ?> /></div>
              <div class="" style="width: 100px; height: 25px; display: inline;"><label>Activo </label> 
                <input name="active" type="checkbox" class="js-switch1" <?php if($rowDetail['active']==1) echo 'checked' ?> /></div>
              </div>


            </div>

          </div>
          <hr>
        </div>
      </div>
      <!--END. TITULAR  SECCION-->

      <!--BLOQUE 1-->
      <div class="col-md-12">
        <div class="panel panel-filled">
          <div class="panel-heading">
            <h4>Categoría</h4>
          </div>
          <div class="panel-body">

            <div class="row">

              <div class="form-group col-xl-4">
                <label>Familia</label>
                <select id="productsFamilies" class="select2_demo_1 form-control" style="width: 100%">
                  <option value="">Seleccione</option>
                  <option value="1">Cursos</option>
                  <option value="2">Actividades</option>
                  <option value="3">Equipamiento</option>
                  <option value="4">Seguros</option>
                  <option value="5">Alojamiento</option>
                </select>
              </div>

              <div class="form-group col-xl-3">
                <label>Categoría</label>
                <select name="categoryId" id="productsCategories" class="select2_demo_1 form-control" style="width: 100%">
                </select>
              </div>

              <div class="form-group col-xl-3">
                <label>Proveedor</label>
                <select name="providerId" id="productsProviders" class="select2_demo_1 form-control" style="width: 100%">
                </select>
              </div>

            </div> <!-- end row -->

            <div class="row">
              <div class="form-group col-6 col-md-4 col-lg-4 ">
                <label>Referencia</label>
                <input type="text" value="<?=$rowDetail['reference']?>" id="productReference" class="form-control" name="reference">
              </div>

              <div class="form-group col-6 col-sm-3 col-md-3 col-lg-2 ">
                <label>Precio</label>
                <input type="text" value="<?=$rowDetail['price']?>" id="productPrice" class="form-control" name="price">
              </div>

            </div>

          </div> 
        </div>
      </div>
      <!--END BLOQUE 1-->
      <!--BLOQUE 2 -->
      <div class="col-md-12">
        <div class="panel panel-filled">
          <div class="panel-heading">
            <h4>Especificaciones</h4>
          </div>
          <div class="panel-body">
            <div class="row" id="specs-container"><!--init row -->
            </div> <!-- end row -->
          </div>
        </div>
      </div>

      <!--END BLOQUE 2-->
      <!--BLOQUE 3--> 
      <div class="col-md-12">
        <div class="panel panel-filled">
          <div class="panel-heading">
            <h4>Contenido variable en función del idioma</h4>
          </div>
          <div class="panel-body">


            <div class="row">

              <div class="form-group col-xl-3 ">
                <label>Idioma</label>
                <select name="language" id="languageSel" class="select2_demo_1 form-control" style="width: 100%">
                  <option value="">Seleccione</option>
                  <option selected="" value="1">Español</option>
                  <option <?php if( $action=="add") echo 'disabled' ?> value="2">Inglés</option>
                </select>
              </div>


              <div class="form-group  col-xl-12">
                <label >Nombre</label>
                <input id="productName"  type="text" name="name" class="form-control" value="<?=$rowDetail['name']?>">
              </div>

              <div class="form-group col-xl-12">
                <label>Descripci&oacute;n </label>
                <textarea name="description" rows="4" class="form-control" id="productDescription"><?=$rowDetail['description']?></textarea>
              </div>

              <div class="form-group col-xl-12">
                <label>Requisitos / Condiciones</label>
                <textarea name="requirements" rows="4" class="form-control" id="productRequirements"><?=$rowDetail['requirements']?></textarea>
              </div>
               <div class="form-group col-xl-12">
                <label>Incluye </label>
                <textarea name="included" rows="4" class="form-control" id="productIncluded"><?=$rowDetail['included']?></textarea>
              </div>
              <div class="form-group col-xl-12">
                <label>Objetivos / Comentarios</label>
                <textarea name="objectives" rows="4" class="form-control" id="productObjectives"><?=$rowDetail['objectives']?></textarea>
              </div>


              <div class="form-group col-xl-12">
                <label>Meta title</label>
                <input id="metaTitle" maxlength="70" type="text" name="metaTitle" class="form-control" value="<?=issetor($rowDetail['metaTitle'])?>">
                <p class="pSeo">Máximo 70 carácteres</p>
              </div>

              <div class="form-group col-xl-12">
                <label  >Meta Description</label>

                <input id="metaDescription" maxlength="160" name="metaDescription" type="text" class="form-control" value="<?=issetor($rowDetail['metaDescription'])?>">
                <p class="pSeo">Máximo 160 carácteres</p>

              </div>

              <div class="form-group col-xl-12">
                <label >Meta KeyWords</label>
                <input type="text"  name="metaKeywords" id="metaKeywords"  class="form-control" value="<?=issetor($rowDetail['metaKeywords'])?>">
                <p class="pSeo">De 2 a 10 keywords</p>
              </div>


            </div> <!-- row -->



          </div>
        </div>
      </div>
      <!--BLOQUE 3-->
  </form>
     


      <!--GALERIA-->

      <div class="col-md-12">

       <div class="panel panel-filled">
        <div class="panel-heading">
          <h4 style="display:inline" class="panel-title">Galería de IMÁGENES </h4>
          <button style="display:inline" id="addImgBtn" class="btn btn-success" >Añadir im&aacute;genes</button>
        </div>
        <div class="panel-body">

          <div class="col-xs-12">

           <form id="upload-widget" method="post" action="<?=URLADMIN?>archivos/uploadImg.php" class="dropzone">
             <input id="uploadToken"  type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
             <div class="fallback">
               <input name="file" type="file" />
             </div>
           </form>

         </div>

         <div class="col-xs-12">
          <hr>
        </div>

      </div>

    </div>
  </div>
  <!--.END GALERÍA-->



      <!--BLOQUE 3-->

      <div class="row">

       <div class="btnFixed">
        <div class="cnt_btnFixed">
          <a class="btn btn-accent save-product-btn" type="button" data-toggle="tooltip"  title="guardar"><i class="fa fa-repeat"></i></a>
          <a href="<?=URL?>LISTADO-PRODUCTOS" class="btn btn-primary"  data-toggle="tooltip"  title="volver"><i class="fa fa-step-backward"></i></a>
        </div>
      </div>

      <div class="form-group col-md-12">
        <a href="#" class="btn btn-accent save-product-btn"> guardar datos </a>
        <a href="#" class="btn btn-danger delete-product-btn"> x borrar producto</a>
        <a href="<?=URL?>LISTADO-PRODUCTOS" class="btn btn-primary"> < volver al listado </a>
      </div>


    </div>

 
  <!--END FORMULARIO USUARIO-->


</div>
</section>
<!-- End main content-->

</div>
<!-- End wrapper-->
<?php 
include_once("include/modals/common-modals.php");
include('include/modals/edit-image.php'); 
include('include/js.php'); 
?>
<script>
  var arrDetail=<?=json_encode($rowDetail)?>;
  console.log(arrDetail);
</script>
<script type="text/javascript" src="<?=URLADMIN?>assets/js/admin/products/product-detail.js"></script>
<script type="text/javascript" src="<?=URLADMIN?>assets/js/admin/products/product-detail-combos.js"></script>
<script type="text/javascript" src="<?=URLADMIN?>assets/js/adminImage.js"></script>
</body>

</html>