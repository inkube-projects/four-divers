<?php

$autoInit = ( isset($autoInitLoginSystem) ) ? $autoInitLoginSystem : true;

$LS = new LS(array(
  "db" => array(
    "host" => DB_HOST,
    "port" => 3306,
    "username" => DB_USER,
    "password" => DB_PASS,
    "name" => DB_NAME,
    "table" => DB_USER_ADMIN_TABLE,
	"token_table" => DB_USER_ADMIN_TOKEN_TABLE,
	"connection" => $dbo,
	"columns" => array(
        "email" => "email_private",
      )
  ),
  "features" => array(
    "auto_init" => $autoInit
  ),
  "classes" => array(
    "path" => "../../../../sys/class/"
  ) ,
  "email" => array(
    "email" => "",
	"password" => "",
	"smtpHost" => "",
	"fromName" => "",
	"templateReset" => "",
	"urlImages" => URLADMIN."img/"
  ),
  "pages" => array(
    "no_login" => array( // páginas a las que se puede acceder sin estar logueado
      "/public/rbac_test.php",
	   "/public/register.php",
	   "/public/test.php",
      "/ADMIN-RESET-PASSWORD",
    ),
    "everyone" => array(
      "/public/status.php"
    ),
	 "two_step_login" => array(
      "devices_table" => DB_USER_ADMIN_DEVICES_TABLE
    ),
    "login_page" => "/fourdivers/LOGIN-ADMIN",
    "home_page" => "/fourdivers/ADMIN-FOUR-DIVERS"
  )
));

?>