<?php

    /*
	* Enable sessions if needed.
	* Avoid pesky warning if session already active.
	*/
	//$status = session_status();
	//if ($status == PHP_SESSION_NONE){
		//There is no active session
		if (!isset($_SESSION)) { session_start(); }
		//inicio arrays;
		if(!isset($_SESSION['company'])){
		$_SESSION['filters'] = array(); // array para filtrar listados por página, compañía...
		$_SESSION['company']=array();
		}
		if(!isset($_SESSION['langCode']))$_SESSION['langCode']="es";
	//}
	
	
	// for admin area
	$pathAdmin = ( isset($adminArea) ) ? '../' : '../';
	
	// include_once $pathAdmin.'assets/languages/'.$_SESSION['langCode'].'/common.lang.php';
	include_once '../../public/admin/assets/languages/'.$_SESSION['langCode'].'/common.lang.php';
	
	// UTILS
	// check if exists value
	function issetor(&$var, $default = '') { return isset($var) ? $var : $default;}
	
	/*
	* Generate an anti-CSRF token if one doesn't exist
	Tokens. También llamado componente léxico es una cadena de caracteres que tiene un significado coherente en cierto lenguaje de programación. Ejemplos de tokens, podrían ser palabras clave (if, while, int,...), identificadores, números, signos, o un operador de varios caracteres, (por ejemplo, :=).
	*/
	if ( !isset($_SESSION['token']) )
	{
	$_SESSION['token'] = sha1(uniqid((string)mt_rand(), TRUE));
	}
	
	
	/*
	* Include the necessary configuration info
	*/
	require_once $pathAdmin.'../sys/config/db-cred.inc.php';  
	
	
	/*
	* Define constants for configuration info
	*/
	foreach ( $C as $name => $val )
	{
	define($name, $val);
	}
	
	/*
	* Create a PDO object
	$dsn = "mysql:host=" . DB_HOST . ";dbname=" . DB_NAME;
	$dbo = new PDO($dsn, DB_USER, DB_PASS);
    */
    
	/*
	* Create a Meekrodb object */
	$dbo = new MeekroDB(DB_HOST, DB_USER, DB_PASS,  DB_NAME, DB_PORT, DB_ENCODING);
	
	
	//$result = $dbo->query("SELECT * FROM events ");
	
	
	/*
	* Esta función dice a PHP la ruta que debe buscar para encontrar el archivo de cada clase que se declare.
	* Al instanciar p.e. new MeekroDB() irá a buscar en ../sys/class.MeekroDB.inc.php
	*/
	function __autoload($class){
		global $pathAdmin;
		$filename = $pathAdmin."../sys/class/class." . $class . ".inc.php";
		if ( file_exists($filename) ){
			include_once $filename;
		}
	
	} // class final
    
?>