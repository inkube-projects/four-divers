<?php

class AdminClients extends AdminBaseSection {

	private static $idClient;
	private  $columns;
	protected static $mainQueryWhere;


   /**
   * Intialize
   * @param array $db database
   */
   public function __construct($db){
   	self::$dbh=$db;

   	$this->columns = array(
   		array( 'db' => 'id', 'dt' => 'DT_RowId' ),
   		array( 'db' => 'date',  'dt' => 'date'),
   		array( 'db' => 'name',  'dt' => 'name','formatter' => function( $d, $row ) {
        $id = (strlen($row['nif'])>1) ? $row['nif'] : $row['id'];
        $urlType=($row['type']==1) ? 'CLIENTE' : 'EQUIPO-HUMANO';
        return '<small>ID: '.$id.'</small><br><a  href="'.URL.'DETALLE-'.$urlType.'/'.$row['id'].'" style="display:block; width:100%" class="copy" > '.$d.' </a>';
      } ),
   		array( 'db' => 'mobile_phone',  'dt' => 'mobile_phone' ),
   		array( 'db' => 'email',  'dt' => 'email' ),
   		array( 'db' => 'equipment_sizes','dt' => 'open_sizes', 'formatter' => function ($d, $row){
   			return "<button data-sizes='".$d."' type='button' class='btn btn-default btn-xs open-modal-tallas'> tallas </button>";
   		}
   	),
   		array( 'db' => 'status',  'dt' => 'status', 'formatter' => function ($d, $row){
   			if($d!=1)return '<i class="fa fa-times text-danger css-text20" aria-hidden="true"></i>';
   			else return '<i class="fa fa-check text-success css-text20" aria-hidden="true"></i>';
   		}
   	)

   	);

   }
   


   private static function createWhere (){

	    // WHERE
		self::$mainQueryWhere = new WhereClause('and'); // create a WHERE statement of pieces joined by ANDs

		$busqueda=$_POST['search']['value'];
		if(strlen($busqueda)>0){ 
			  $subclause = self::$mainQueryWhere->addClause('or'); // add a sub-clause with ORs
			  $subclause->add('`name` LIKE %ss', $busqueda); 
			  $subclause->add('surname LIKE %ss', $busqueda); 
			  $subclause->add('email LIKE %ss', $busqueda); 
			}

      self::$mainQueryWhere->add('type =  %i', $_POST['type'] );  


		//if( $_POST['var']!="0"){ self::$mainQueryWhere->add('var LIKE %ss', $_POST['var'] );  }

		}


		public function listClients(){
			if(isset($_POST['token']) && $_POST['token']==$_SESSION['token'] && isset($_POST['start']) ){

		    //self::$dbh->debugMode();	
				$order = self::order( $_POST, $this->columns );
				
				self::createWhere();

				$listado = self::$dbh->query("SELECT SQL_CALC_FOUND_ROWS id, DATE_FORMAT(created, '%l3') AS date, CONCAT(`name`,' ',surname) AS `name`, mobile_phone, email, type, active AS `status`, equipment_sizes, nif FROM users  WHERE %l2 ".$order." LIMIT %i0,%i1",$_POST['start'],$_POST['length'],self::$mainQueryWhere,'%d/%m/%x');

				$recordsTotalNum=self::$dbh->query("SELECT FOUND_ROWS();");

				$recordsFiltered = (strlen($busqueda)<=0) ? $recordsTotalNum[0]['FOUND_ROWS()'] : self::$dbh->count();

				if(!isset($_POST['isJs'])) return $listado;
				else {	 

					return json_encode(array(
						"draw"            => isset ( $_POST['draw'] ) ? intval( $_POST['draw'] ) :	0,
						"recordsTotal"    =>  $recordsTotalNum[0]['FOUND_ROWS()'] ,
						"recordsFiltered" => intval( $recordsFiltered ),
						"data"            => self::data_output( $this->columns, $listado )
					));

				}


		} // existe token y es correcto
		
	} 

	/*
	* Leer detalle de client
	* @param  Meekrodb object  $sb    
	* @param  int  $id  
	*/	

	public static function clientDetail($db,$id){
		if(intval($id)>0){
			self::$dbh=$db;
			self::$idClient = $id;
			$detail = self::$dbh->queryFirstRow("SELECT * FROM  users  WHERE id=%i0",$id);
      if($detail['birthdate']!="")$detail['birthdate']=date("d-m-Y", strtotime($detail['birthdate'] ) );
      if($detail['medical_revision_validity_date']!="")$detail['medical_revision_validity_date']=date("d-m-Y", strtotime($detail['medical_revision_validity_date'] ) );
      if($detail['insurance_validity_date']!="")$detail['insurance_validity_date']=date("d-m-Y", strtotime($detail['insurance_validity_date'] ) );
      return $detail;
    }

  }

  private function fillDataArray(){

    //Equipment
    $equipmentOptions = [1,2,6,7,9,10];
    $equipment=[];

    forEach ($equipmentOptions as $option){
      if($_POST['equipment-'.$option]!="")$equipment[$option]=$_POST['equipment-'.$option];
    }

    //Addresses
    $addressFields = ["nif","name","address","number","cp","city","province","country","comments"];
    $address=[];
    $address_delivery=[];

    forEach ( $addressFields as $addressField ){

      if($_POST['address_'.$addressField]!="") $address[$addressField]=$_POST['address_'.$addressField];
      
      if($_POST['address_delivery_'.$addressField]!="") $address_delivery[$addressField]=$_POST['address_delivery_'.$addressField];

    }


    
    $data = array (
      'active' => isset($_POST['active']) ? 1 : 0,
      'news' => isset($_POST['news']) ? 1 : 0,
      'only_one_address' => isset($_POST['only_one_address']) ? 1 : 0,
      'email'=> $_POST['email'],
      'name'=> $_POST['name'],
      'surname'=> $_POST['surname'],
      'mobile_phone'=> $_POST['mobile_phone'],
      'contact_full_name' => $_POST['contact_full_name'],
      'contact_phone' => $_POST['contact_phone'],
      'contact_comments' => $_POST['contact_comments'],
      'profession' => $_POST['profession'],
      'profession_special' => $_POST['profession_special']>0?$_POST['profession_special']:NULL,
      'height' => $_POST['height']>0?$_POST['height']:NULL,
      'weight' => $_POST['weight']>0?$_POST['weight']:NULL,
      'age' => $_POST['age'],
      'gender' => $_POST['gender']>0?$_POST['gender']:NULL,
      'diseases' => $_POST['diseases'],
      'allergies' => $_POST['allergies'],
      'birthdate' => $_POST['birthdate']!='' ? $this->traslateDate($_POST['birthdate']) : NULL,
      'allergies' => $_POST['allergies'],
      'courses' => '["' . implode('","', $_POST['courses']) . '"]',
      'courses_other' => $_POST['courses_other'],
      'certified_by' => $_POST['certified_by']>0?$_POST['certified_by']:NULL,
      'activities' => '["' . implode('","', $_POST['activities']) . '"]',
      'known_for' => $_POST['known_for']>0?$_POST['known_for']:NULL,
      'instagram_url' => $_POST['instagram_url'],
      'facebook_url' => $_POST['facebook_url'],
      'sports' => '["' . implode('","', $_POST['sports']) . '"]',
      'insurance_validity_date' => $_POST['insurance_validity_date']!='' ? $this->traslateDate($_POST['insurance_validity_date']) : NULL,
      'insurance_number' => $_POST['insurance_number'],
      'nif'=> $_POST['address_nif'],
      'medical_revision_validity_date' => $_POST['medical_revision_validity_date']!='' ? $this->traslateDate($_POST['medical_revision_validity_date']) : NULL,
      'equipment_sizes' => json_encode($equipment),
      'equipment_comment' => $_POST['equipment_comment'],
      'immersion_number' => $_POST['immersion_number']>0?$_POST['immersion_number']:NULL,
      'address' =>  json_encode($address),
      'address_delivery' =>  json_encode($address_delivery),
      'avatar' =>  $_POST['avatar'],
      'insurance_file' =>  $_POST['insurance_file'],
      'medical_file' =>  $_POST['medical_file'],
      'type' => $_POST['type'],

    );
    
    return $data;
    
  }
  

  private function checkEmailInUse($id=NULL){
    if(strlen($_POST['email'])==0) return false;

    $where = new WhereClause('and');
    $where->add('email=%s', $_POST['email'] );  

    if($id!=NULL){$where->add('id!=%i', $_POST['userId'] );  }

    $isInUse = self::$dbh->queryFirstRow("SELECT count(id) as cnt FROM users WHERE %l", $where);
    return $isInUse['cnt']>0 ? true : false;

  }

    /*
    *  ADD CLIENT
    */
    public function addClient(){
     if(isset($_POST['token']) && $_POST['token']==$_SESSION['token']  ){
       $resArr=array();
       if( $this->checkEmailInUse() ){ $resArr["error"]=1; }
       else {
         $data = $this->fillDataArray();
         $data['created'] = date("Y-m-d");
         self::$dbh->insert('users', $data);
         $insertedId=self::$dbh->insertId();
         if($insertedId>0){
          if( strlen( $_POST['avatar'] )>0 ) $this->moveFile($insertedId,'images',$_POST['avatar']);
          if( strlen( $_POST['insurance_file'] )>0 ) $this->moveFile($insertedId,'documents',$_POST['insurance_file']);
          if( strlen( $_POST['medical_file'] )>0 ) $this->moveFile($insertedId,'documents',$_POST['medical_file']);
          $resArr["id"] = $insertedId;
        }
        
      }
      self::$dbh->disconnect();
    }
    
    echo json_encode($resArr);
  } 
  
  /*
  *  UPDATE CLIENT
  */
  public function updateClient(){
   if( isset($_POST['token']) && $_POST['token']==$_SESSION['token'] ){
    $resArr=array();
    if( $this->checkEmailInUse($_POST['userId']) ){ $resArr["error"]=1; }
    
    else {
      $data = $this->fillDataArray();
      self::$dbh->update('users', $data, "id=%i ", $_POST['userId']);
      $counter = self::$dbh->affectedRows();
      $resArr["resultado"] = ($counter)  ? 1 : 0; 
    }

    self::$dbh->disconnect(); 
    echo json_encode($resArr);
    
  }
} 


/*
  *  UPDATE Padi number
  */
  public function updatePadiNumber(){
   if( isset($_POST['token']) && $_POST['token']==$_SESSION['token'] ){
      $resArr=array();
      self::$dbh->update('users', array('padi_number' =>$_POST['padi_number']), "id=%i ", $_POST['user_id']);
      $resArr["resultado"] = (self::$dbh->affectedRows())  ? 1 : 0; 
      self::$dbh->disconnect(); 
      echo json_encode($resArr);
  }
} 


public function deleteClient(){
    // verificar que no se emplea en ninguna tabla 
    /*$isInUse = self::$dbh->queryFirstRow("SELECT 
    ( SELECT count(id) FROM processed_cases WHERE firstReader_client_id=%i0 || secondReader_client_id=%i0 ) AS numPcc,
    ( SELECT count(id) FROM client_region_access WHERE client_id=%i0 ) AS numPra", $_POST['id']);
    */
    /*if( $isInUse['numPcc']>0 || $isInUse['numPra']>0 ) {   
       $resArr["resultado"]=-1; 
       self::$dbh->disconnect();  
       echo json_encode($resArr);
       exit;
    }
    else {*/
     self::deleteItem('users');
  //}

   } 


   private function moveFile($userId,$type='images',$fileName){

    $userTempFolder = "../../../assets/".$type."/users/temp";
    $userFolderPath = "../../../assets/".$type."/users/u-".$userId;

    if (!is_dir($userFolderPath)) {
     mkdir($userFolderPath, 0777, true);
     chmod($userFolderPath, 0777);
   }


   rename($userTempFolder."/".$fileName,  $userFolderPath."/".$fileName);

 }


public function searchClient (){

    $qWhere=self::$mainQueryWhere = new WhereClause('or');
    $qWhere->add('`name` LIKE %ss', $_POST['q']);
    $qWhere->add('surname LIKE %ss', $_POST['q']);
    $qWhere->add('nif LIKE %ss', $_POST['q']);

    $listado = self::$dbh->query("SELECT id, CONCAT(`name`,' ',surname) AS text FROM users  WHERE %l0 ORDER BY name LIMIT 50",$qWhere);

    return json_encode(array(
        "results"       => $listado,
        "pagination"    =>  ['more'=>false]
      ));


 }




} // final clase


?> 



