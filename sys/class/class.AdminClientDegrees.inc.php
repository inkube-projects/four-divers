<?php

class AdminClientDegrees extends AdminBaseSection {

	private static $idClient;
	private  $columns;
	protected static $mainQueryWhere;

   /**
   * Intialize
   * @param array $db database
   */
   public function __construct($db){
   	self::$dbh=$db;

   	$this->columns = array(
   		array( 'db' => 'id', 'dt' => 'DT_RowId' ),
      array( 'db' => 'course_id', 'dt' => 'course_id' ),
      array( 'db' => 'status', 'dt' => 'status' ),
   		array( 'db' => 'name',  'dt' => 'course'),
      array( 'db' => 'code',  'dt' => 'code'),
   		array( 'db' => 'diver_no',  'dt' => 'diver_no' ),
   		array( 'db' => 'date',  'dt' => 'date' ),
      array( 'db' => 'comment',  'dt' => 'comment' ),
   		array( 'db' => 'status',  'dt' => 'status_txt', 'formatter' => function ($d, $row){
   			if($d!=2)return '<i class="fa fa-check-circle text-warning css-text20" aria-hidden="true"></i>';
   			else return '<i class="fa fa-check-circle text-success css-text20" aria-hidden="true"></i>';
   		   }
    	),
      array(
        'db'        => 'id',
        'dt'        => 'delete',
        'formatter' => function( $d, $row ) {
          return '<button type="button" class="btn btn-danger btn-flat deleteCase" data-id="'.$d.'"><i class="fa fa-trash" aria-hidden="true"></i></button>';
          
        }
      )

   	);

   }
   

   private static function createWhere (){

	    // WHERE
		self::$mainQueryWhere = new WhereClause('and'); // create a WHERE statement of pieces joined by ANDs

		}


		public function listClientDegrees(){
			if( isset($_POST['token']) && $_POST['token']==$_SESSION['token'] && isset($_POST['start'])  && isset($_POST['user_id']) ){

		    //self::$dbh->debugMode();	
				$order = self::order( $_POST, $this->columns );
				
				self::createWhere();
        self::$mainQueryWhere->add( 'user_id=%i', $_POST['user_id'] );
        self::$mainQueryWhere->add( 'language_id=%i', 1 );

				$listado = self::$dbh->query("SELECT SQL_CALC_FOUND_ROWS ud.id, DATE_FORMAT(date, '%l3') AS date, ud.code, ud.diver_no, ud.status, ud.comment, pl.name, ud.course_id  FROM user_degrees ud INNER JOIN product_language pl ON pl.product_id=ud.course_id WHERE %l2 ".$order." LIMIT %i0,%i1",$_POST['start'],$_POST['length'],self::$mainQueryWhere,'%d/%m/%x');

				$recordsTotalNum=self::$dbh->query("SELECT FOUND_ROWS();");

				$recordsFiltered = (strlen($busqueda)<=0) ? $recordsTotalNum[0]['FOUND_ROWS()'] : self::$dbh->count();

				if(!isset($_POST['isJs'])) return $listado;
				else {	 

					return json_encode(array(
						"draw"            => isset ( $_POST['draw'] ) ? intval( $_POST['draw'] ) :	0,
						"recordsTotal"    =>  $recordsTotalNum[0]['FOUND_ROWS()'] ,
						"recordsFiltered" => intval( $recordsFiltered ),
						"data"            => self::data_output( $this->columns, $listado )
					));

				}


		} // existe token y es correcto
		
	} 


  private function fillDataArray(){
    
    $data = array (
      'status' => isset($_POST['status']) ? $_POST['status'] : NULL,
      'comment' => $_POST['comment'],
      'code' => $_POST['code'],
      'date' => $_POST['date']!='' ? $this->traslateDate($_POST['date']) : NULL,
      'diver_no' => $_POST['diver_no'],
      'course_id' => $_POST['course_id'],
      'user_id' => $_POST['user_id']
    );
    
    return $data;
    
  }

    /*
    *  ADD DEGREE
    */
    public function addDegree(){
     if(isset($_POST['token']) && $_POST['token']==$_SESSION['token']  ){
       $resArr=array();
         $data = $this->fillDataArray();
         self::$dbh->insert('user_degrees', $data);
         $insertedId=self::$dbh->insertId();
         $resArr["resultado"] = ($insertedId>0)  ? 1 : 0; 
      self::$dbh->disconnect();
    }
    
    echo json_encode($resArr);
  } 
  
  /*
  *  UPDATE DEGREE
  */
  public function updateDegree(){
   if( isset($_POST['token']) && $_POST['token']==$_SESSION['token'] ){
      $resArr=array();
      //self::$dbh->debugMode();
      $data = $this->fillDataArray();
      self::$dbh->update('user_degrees', $data, "user_id=%i AND course_id=".intval($_POST['course_id']), $_POST['user_id']);
      $counter = self::$dbh->affectedRows();
      $resArr["resultado"] = ($counter)  ? 1 : 0; 
    }

    self::$dbh->disconnect(); 
    echo json_encode($resArr);
    
  
} 



public function deleteDegree(){

     self::deleteItem('user_degrees');

   } 


  



} // final clase


?> 



