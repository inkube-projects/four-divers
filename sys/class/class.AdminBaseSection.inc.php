<?php 


  class AdminBaseSection{
	
	protected static $dbh;
	// leguaje de referencia en la que obligatoriamente ha de existir toda entrada, en los otros idiomas es opcional
    public static $mainLanguageId=1; // español
	public static $imageUrl='https://www.fourdivers.com/public/assets/images/';
	public static $fileUrl='https://www.fourdivers.com/public/files/';
	
	public function __construct($db){}
	
	
	/* FUNCIONES PARA DATATABLES plugin de jquery para el tratamiento de tablas */
	/**
	 * Pull a particular property from each assoc. array in a numeric array, 
	 * returning and array of the property values from each item.
	 *
	 *  @param  array  $a    Array to get data from
	 *  @param  string $prop Property to read
	 *  @return array        Array of property values
	 */
	static function pluck ( $a, $prop )
	{
		$out = array();
		for ( $i=0, $len=count($a) ; $i<$len ; $i++ ) {
			$out[] = $a[$i][$prop];
		}
		return $out;
	}
	
	static public function issetor($var, $default = '') { return isset($var) ? $var : $default;}
	
	// pasar fecha de formato español a inglés
	static public function traslateDate($d) { 
	  $d = preg_replace("([^0-9/-])", "", $d); // evitar inyección, sólo aceptamos dígitos y -/
	  return implode('-', array_reverse(explode('-', $d)));
	}
	
	/**
	 * Ordering
	 *
	 * Construct the ORDER BY clause for server-side processing SQL query
	 *
	 *  @param  array $request Data sent to server by DataTables
	 *  @param  array $columns Column information array
	 *  @return string SQL order by clause
	 */
	static function order ( $request, $columns )
	{
		$order = '';
		if ( isset($request['order']) && count($request['order']) ) {
			$orderBy = array();
			$dtColumns = self::pluck( $columns, 'dt' );
			for ( $i=0, $ien=count($request['order']) ; $i<$ien ; $i++ ) {
				// Convert the column index into the column data property
				$columnIdx = intval($request['order'][$i]['column']);
				$requestColumn = $request['columns'][$columnIdx];
				$columnIdx = array_search( $requestColumn['data'], $dtColumns );
				$column = $columns[ $columnIdx ];
				if ( $requestColumn['orderable'] == 'true' ) {
					$dir = $request['order'][$i]['dir'] === 'asc' ?
						'ASC' :
						'DESC';
					$orderBy[] = '`'.$column['db'].'` '.$dir;
				}
			}
			$order = ' ORDER BY '.implode(', ', $orderBy);
		}
		return $order;
	}
	   
	  
	 /**
	 * Create the data output array for the DataTables rows
	 *
	 *  @param  array $columns Column information array
	 *  @param  array $data    Data from the SQL get
	 *  @return array          Formatted data in a row based format
	 */
	static function data_output ( $columns, $data )
	{
		$out = array();
		
		
		for ( $i=0, $ien=count($data) ; $i<$ien ; $i++ ) {
			$row = array();
			for ( $j=0, $jen=count($columns) ; $j<$jen ; $j++ ) {
				$column = $columns[$j];
				// Is there a formatter?
				if ( isset( $column['formatter'] ) ) {
					$row[ $column['dt'] ] = $column['formatter']( $data[$i][ $column['db'] ], $data[$i] );
				}
				else {
					$row[ $column['dt'] ] = $data[$i][ $columns[$j]['db'] ];
				}
			}
			$out[] = $row;
		}
		return $out;
	}
	 
	
	
	// READ
	static function readTable($db,$table, $fields, $orderField=NULL, $orderDir="ASC",$where=NULL){
		$qw = (!is_null($where)) ? " WHERE ".$where : '';
		$orderBy = (!is_null($orderField))? " ORDER BY ".$orderField." ".$orderDir : '';
		return $db->query(" SELECT ".$fields." FROM ".$table.$qw.$orderBy);
		}
		
		
	
	
		// DELETE item 
	
  public function deleteItem($table,$id=NULL,$field='id'){
		  $resArr=array();
		  $deleteId = ( is_null($id) ) ? $_POST['id'] : $id;
		  if( isset($_POST['token']) && $_POST['token']==$_SESSION['token'] ){

			   self::$dbh->query("DELETE FROM ".$table." WHERE ".$field."=%i ", $deleteId);
		       $counter =self::$dbh->affectedRows();
		       $resArr["resultado"]=$counter; 
				
			
	self::$dbh->disconnect();	
	echo json_encode($resArr);
			  
			  }
		 
		} 
		
    	
	 // Company info, por defecto marco superadmin web 99, para pérdida de password afiliados
	public static function getCompanyInfo($dbo,$webId=99){
		self::$dbh=$dbo;
		$companyInfo = self::$dbh->queryFirstRow("SELECT CONCAT('logo_email_',emp.codigo,'.png') AS companyLogo, emp.email, emp.emailHost, emp.emailPassword, emf.nombre AS companyName, emp.url, CONCAT (emf.direccion,' ',emf.poblacion) AS companyAddress, emf.telefono AS companyPhone, emp.email AS companyEmail FROM empresas emp
		LEFT JOIN empresasInfoFiscal emf ON emf.id=emp.infoFiscal
WHERE emp.id=%i0",$webId);

         $affInfoSet=0;
         
		 $companyInfo['varsFooter']=array (
		                      array( 'name' => 'affiliateFooter', 'value' => $affInfoSet , 'type' => 'zerodelete' ),
							  array( 'name' => 'companyName', 'value' => $companyInfo['companyName'], 'type' => 'text' ),
							  array( 'name' => 'companyPhone', 'value' => $companyInfo['companyPhone'], 'type' => 'text' ),
							  array( 'name' => 'companyEmail', 'value' => $companyInfo['email'], 'type' => 'text' ),
							  array( 'name' => 'footerEmailImage', 'value' => self::$imageUrl."companies/".$companyInfo['companyLogo'], 'type' => 'text' ),
							  			  
			);	
			
        return $companyInfo;
		
		}
		
		
		
	public function changeLanguage(){
			  $resArr=array();
			  $resArr['langCode']=$_SESSION['langCode'];
	          if($_SESSION['langCode']==$_POST['val']){ $resArr["resultado"]= 0;}
	          else { $_SESSION['langCode']=$_POST['val']; $resArr["resultado"]= 1; }
	          echo json_encode($resArr);
	 		}
			
	
	public static function encrypString($plaintext) {
        $key = pack('H*', "xcb04b7e103a0cd7b54763077cef08bc55aZe029fdebaf2e1d417e2ffb2a00a3");//change this
        $key_size =  strlen($key);
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_DEV_URANDOM);
        $ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key,$plaintext, MCRYPT_MODE_CBC, $iv);
        $ciphertext = $iv . $ciphertext;
        # encode the resulting cipher text so it can be represented by a string
        $ciphertext_base64 = base64_encode($ciphertext);
        return $ciphertext_base64;//important rawurlencode for + symbol in url
    }



    public static function get_ip() { 
			if (preg_match('/^(\d{1,3}\.){3}\d{1,3}$/s', $_SERVER["HTTP_CLIENT_IP"])) {
				$ip = $_SERVER["HTTP_CLIENT_IP"]; }
			else { 
		 		 if (preg_match('/^(\d{1,3}\.){3}\d{1,3}$/s', $_SERVER["HTTP_X_FORWARDED_FOR"])) {
			 		 $ip = $_SERVER["HTTP_X_FORWARDED_FOR"]; 
					 }
		 		 else {
			 		 if (preg_match('/^(\d{1,3}\.){3}\d{1,3}$/s', $_SERVER["REMOTE_HOST"])) {
				  		$ip = $_SERVER["REMOTE_HOST"]; 
						}
			 		 else {
					    $ip = $_SERVER["REMOTE_ADDR"]; 
				       }
				 }
	  	  } 
		 return($ip); 
	 }
	
	
	} // final clase



?>