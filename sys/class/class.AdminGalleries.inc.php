<?php

class AdminGalleries extends AdminBaseSection {
	
	
   /**
   * Intialize
   * @param array $db database
   */
   public function __construct($db){
	   
        self::$dbh=$db;
		
     }
	 
	 
	 
	public static function as3Permission($db,$token){
		self::$dbh=$db;
		//self::$dbh->debugMode();
		return  self::$dbh->queryFirstRow("SELECT * FROM accessFromAs3 WHERE tokenForHtml=%s AND ip=%s",$token,self::get_ip()); 
		
		}
			
	
 
	public static function listGalleries($db,$budgetId){
		self::$dbh=$db;
		
		$adminGallery = ( !isset($_SESSION['cu'])  ||  $_SESSION['cu']!=1 )  ? ' AND onlyAdmin IS NULL ' : ''; 
		
		$arrIcons=array('photo','note2','film');
		$arrTypes=array('IMAGENES','DOCUMENTOS','VIDEOS');
		$galerias = self::$dbh->query("SELECT id, tipo, nombre, idPresu, (SELECT count(id) FROM archivos2 WHERE idB=gp.id AND origen=%i0) AS nArchivos FROM galeriasPresupuesto gp WHERE idPresu=%i0 ".$adminGallery." ORDER BY gp.id DESC",$budgetId); 
		foreach($galerias as $key=>$galeria){
			$galerias[$key]['enc']=self::encrypString($galeria['idPresu']."-".$galeria['id']);
			$galerias[$key]['icon']=$arrIcons[intval($galerias[$key]['tipo'])-1];
			$galerias[$key]['tipoNombre']=$arrTypes[intval($galerias[$key]['tipo'])-1];
			}
		
		return $galerias;
		}
		
		
	public static function galleryDetail($db,$galleryId){
		self::$dbh=$db; $arrTypes=array('IMAGENES','DOCUMENTOS','VIDEOS');
		$galeria = self::$dbh->queryFirstRow("SELECT * FROM galeriasPresupuesto WHERE id=%i0 ",$galleryId); 
		$galeria['enc']=self::encrypString($galeria['idPresu']."-".$galeria['id']);
		$galeria['tipoNombre']=$arrTypes[intval($galeria['tipo'])-1];
		return $galeria;
	}
	
	
	public static function galleryItems($idPresu,$idGallery,$type=1){
		
		$ruta=SERVEROOT."archivos2/presupuestos/p".$idPresu;
		$ds="/";

		$imagenes = self::$dbh->query("SELECT * FROM archivos2 WHERE idB=%i0 AND tipo=%i1 ORDER BY id ASC",intval($idGallery),$type);
	    $result  = array();
		foreach ( $imagenes as $row) {	
			$obj=array();
		    $obj['name'] = $row['nombre'];
			$obj['id'] = $row['id'];
			$obj['alt'] = $row['comentario'];
			$obj['ruta_min'] = $ruta."/min".$ds.$row['nombre'];
			$obj['ruta_max'] = $ruta.$ds.$row['nombre'];
			
			$array = explode('.', $row['nombre']);
			$obj['extension']=end($array);
			
            $result[] = $obj;
		}
			return $result;
   }
		
	
	/*
	* ADD GALLERY 
	*/
		
	 public function addGallery(){
		 if(isset($_POST['token']) && $_POST['token']==$_SESSION['token']  ){
			 
			 $exists = self::$dbh->queryFirstField("SELECT COUNT(*) FROM galeriasPresupuesto WHERE idPresu=%i0 AND nombre=%s1",$_POST['ppto_id'],$_POST['name']);
			 
			 $resArr=array("status"=>1);
			 
			 if( $exists==0 ){
			
				 self::$dbh->insert('galeriasPresupuesto', array('idPresu'=>$_POST['ppto_id'],'nombre'=>$_POST['name'],'descripcion'=>$_POST['description'],'tipo'=>$_POST['type']));
				 $insertedId=self::$dbh->insertId();
				 if($insertedId>0){$resArr["id"] = $insertedId;}
				 else { $resArr["status"]=0; $resArr["error"]="Error insertando registro."; }
				 self::$dbh->disconnect();	    
		 	}
			
			else {
				$resArr["status"]=0;
				$resArr["error"]="Ya existe una galería con ese nombre";
				}
			
			
			 echo json_encode($resArr);
		 }
	 }
	 
	 
	 /*
	 * SEND GALLERY LINK BY EMAIl
	 */
	 
	 public function emailGallery(){
		  if(isset($_POST['token']) && $_POST['token']==$_SESSION['token']  ){
		 
		 $resArr=array("status"=>1);
		  
		 $message=" Nombre de la galería: ".$_POST['name']."<br>";
		 if(strlen($_POST['description'])>0)$message.=" Descripción de la galería: ".$_POST['description']."<br>";
		 $message.=" Ref. presupuesto: ".$_POST['ppto_ref']."<br><br>";
		 
		 if(strlen($_POST['message'])>0)$message.=" Mensaje: ".$_POST['message']."<br><br>";
		 
		 $message.=" Pulse sobre el siguiente vínculo para ver la galería: <a href='".$_POST['link']."'>".$_POST['name']."</a><br><br>";
		 
		 $object = array();
		 
		 $object['message'] = $message;
		 $object['subject'] = $_POST['subject'];
		 $object['to'] = $_POST['email'];
		 
		  $emailDispatcher = new AdminEmailDispatcher(self::$dbh);
		  $emailRes=$emailDispatcher->dispatchEmail($object,$_POST['email']);
		   // email enviado correctamente, actualizo fecha enviado
		   if($emailRes!=true){
			   $resArr["status"]=0;
			   $resArr["config"]=$emailRes;
			   }
		
		 
		 echo json_encode($resArr);
		 
		  }
		 
	 }
	 
	 
		
	/*
	* DELETE GALLERY
	* replantear este código: transaccional, seleccionar imágenes eliminarlas borrar de bd y al final galería 
	*/
	
	public function deleteGallery(){
		 if(isset($_POST['token']) && $_POST['token']==$_SESSION['token']  ){
			$resArr=array("status"=>1);
	        self::$dbh->query("DELETE FROM galeriasPresupuesto WHERE id=%i ", $_POST['gallery_id']);
		       $counter = self::$dbh->affectedRows();
		       if($counter){
				   
				   
				   
				   
				   self::$dbh->query("DELETE FROM archivos2 WHERE idB=%i AND origen>1", $_POST['gallery_id']);
				   
				 }  else {
					 $resArr["status"]=0;
					 $resArr["error"]="Error al eliminar la galería";
				 }
		
		 echo json_encode($resArr);
		}
	}
	
	
	
	/*
	* UPDATE GALLERY
	*/
	public function updateGallery(){
		 if(isset($_POST['token']) && $_POST['token']==$_SESSION['token']  ){
			 
			  $exists = self::$dbh->queryFirstField("SELECT COUNT(*) FROM galeriasPresupuesto WHERE idPresu=%i0 AND nombre=%s1 AND id!=%i2",$_POST['ppto_id'],$_POST['name'],$_POST['id']);
			 
			 $resArr=array("status"=>1);
			 
			 if( $exists==0 ){
				 self::$dbh->update('galeriasPresupuesto', array('nombre'=>$_POST['name'],'descripcion'=>$_POST['description']), "id=%i", $_POST['id']);
				 $counter = self::$dbh->affectedRows();
				 if($counter==0){$resArr["status"]=0; $resArr["error"]="Error actualizando registro."; }
				 self::$dbh->disconnect();	    
		 	}
			
			else {
				$resArr["status"]=0;
				$resArr["error"]="Ya existe una galería con ese nombre";
				}
			
			
			 echo json_encode($resArr);
		 }
	}
	
	
} // final clase


?> 



