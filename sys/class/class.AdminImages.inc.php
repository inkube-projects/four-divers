<?php

class AdminImages {
	
	
  /**
   * @var \PDO Database handler
   */
  private static $dbh;
  

 /**
   * Intialize
   * @param array $db database
   */
 public function __construct($db){
  self::$dbh=$db;

}




public function listImages(){
  $ruta=URLPUBLIC."assets/images/".$_POST['sectionName']."/id-".$_POST['id'];
  $ds="/";
  $section="[".$_POST['sectionId']."]";

  if(isset($_POST['token']) && $_POST['token']==$_SESSION['token'] && isset($_POST['id']) ){

   		// CONSULTA
		//self::$dbh->debugMode();
    $archivos = self::$dbh->query("SELECT * FROM files WHERE origin_id=%i0 AND type=1 AND section=%s1 ORDER BY id ASC",intval($_POST['id']),$section);
    $result  = array();

    foreach ( $archivos as $row) {

     $obj=array();
     $obj['name'] = $row['name'];
     $obj['id'] = $row['id'];
     $obj['alt'] = (is_null($row['comment']))?'':$row['comment'];
     $obj['ruta'] = $ruta.$ds.$row['name'];
     $obj['size'] = $this->file_get_size("../../../assets/images/".$_POST['sectionName']."/id-".$_POST['id'].$ds.$row['name']);
     if(isset($_POST['redactorModal'])){ // redactor modal for insert image
        $obj['thumb'] = $ruta.$ds."min".$ds.$row['name'];
     }

     $result[] = $obj;
   }
   return json_encode($result);

 }


}


public function updateImage(){

  if(isset($_POST['token']) && $_POST['token']==$_SESSION['token']  ){

   $resp=0;
   $idImg=intval($_POST['idImg']);


   if( $idImg>0 ){

    if( isset($_POST['alt']) ){

     $arrField=array();

     if(isset($_POST['alt'])) $arrField[]="comment='".$_POST['alt']."'";
     $fieldQ=implode(", ",$arrField);

     $query="UPDATE files SET ".$fieldQ." WHERE id=%i";

					// CONSULTA
     $updateAlt = self::$dbh->query($query,$idImg);
     self::$dbh->disconnect();	

     if($updateAlt) $resp=1;

   }	

	} // $idImg>0

  return $resp;

}

}


public function deleteImageMultiple(){

  if(isset($_POST['token']) && $_POST['token']==$_SESSION['token']  ){

   foreach($_POST['idsImg'] as  $idImg){

    $_POST['idImg']=$idImg;
    $deleted=$this->deleteImage();


  }

  return 1;

}
}


public function deleteImage(){
  if(isset($_POST['token']) && $_POST['token']==$_SESSION['token']  ){

		 //consultamos nombre ya que al subirlo se renombra
    $imgName = self::$dbh->queryFirstField("SELECT name FROM files WHERE id=%i", $_POST['id']);

    if( $_POST['id']>0 ){
     $query="DELETE from files WHERE id=%i";
			// CONSULTA
     $eliminado = self::$dbh->query($query,$_POST['id']);
     self::$dbh->disconnect();	

     if($eliminado) {

      $folders=['/','/min/','/med/'];

      foreach ($folders as $folder) {

        $nombre_archivo = '../../../assets/images/'.$_POST['sectionName'].'/id-'.$_POST['ownerId'].$folder.$imgName;  
        if (file_exists($nombre_archivo)) {unlink($nombre_archivo);} 

      }
     
      return 1;

    }

  }

}
}


public function rotateImage(){
 if(isset($_POST['token']) && $_POST['token']==$_SESSION['token']  ){

    //consultamos nombre ya que al subirlo se renombra
    $imgName = self::$dbh->queryFirstField("SELECT name FROM files WHERE id=%i", $_POST['id']);

    if( $_POST['id']>0 ){

      $folders=['/','/min/','/med/'];

      foreach ($folders as $folder) {

         $nombre_archivo = '../../../assets/images/'.$_POST['sectionName'].'/id-'.$_POST['ownerId'].$folder.$imgName;

         if (file_exists($nombre_archivo)) {
          $source = imagecreatefromjpeg($nombre_archivo);
          // Rotate
          $rotate = imagerotate($source, 90, 0);
          imagejpeg($rotate,$nombre_archivo);
          imagedestroy($source);
        }
      
      }

      return 1;

    }
  }
}



private function file_get_size($file) {
    //open file
  $fh = fopen($file, "r");
    //declare some variables
  $size = "0";
  $char = "";
    //set file pointer to 0; I'm a little bit paranoid, you can remove this
  fseek($fh, 0, SEEK_SET);
    //set multiplicator to zero
  $count = 0;
  while (true) {
        //jump 1 MB forward in file
    fseek($fh, 1048576, SEEK_CUR);
        //check if we actually left the file
    if (($char = fgetc($fh)) !== false) {
            //if not, go on
      $count ++;
    } else {
            //else jump back where we were before leaving and exit loop
      fseek($fh, -1048576, SEEK_CUR);
      break;
    }
  }
    //we could make $count jumps, so the file is at least $count * 1.000001 MB large
    //1048577 because we jump 1 MB and fgetc goes 1 B forward too
  $size = 1048577*$count;
    //now count the last few bytes; they're always less than 1048576 so it's quite fast
  $fine = 0;
  while(false !== ($char = fgetc($fh))) {
    $fine ++;
  }
    //and add them
  $size = $size+$fine;
  fclose($fh);
  return $size;
}



} // final clase


?> 



