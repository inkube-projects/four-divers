<?php

class AdminFiles extends AdminBaseSection {
	
 
 /**
   * Intialize
   * @param array $db database
   */
   public function __construct($db){
        self::$dbh=$db;
       
    }
	
	
	
	
	public function listFiles(){
		
		$ruta=SERVEROOT."archivos2/presupuestos/p".$_POST['idPresu'];
		$ds="/";
		
		if(isset($_POST['token']) && $_POST['token']==$_SESSION['token'] && isset($_POST['id']) ){
		
   		// CONSULTA
		//self::$dbh->debugMode();
		$archivos = self::$dbh->query("SELECT * FROM archivos2 WHERE idB=%i0 AND tipo=2 ORDER BY id ASC",intval($_POST['id']));
	    $result  = array();
		
		foreach ( $archivos as $row) {	
		
			$obj=array();
		    $obj['name'] = $row['nombre'];
			$obj['id'] = $row['id'];
			$obj['alt'] = $row['comentario'];
			$array = explode('.', $row['nombre']);
			$obj['extension']=end($array);
			
            //$obj['size'] = $this->file_get_size('../../../../../archivos2/presupuestos/p'.$_POST['idPresu'].$ds.$row['nombre']);
			$obj['ruta'] = $ruta.$ds.$row['nombre'];
			
            $result[] = $obj;
		}
			return json_encode($result);
			
	  }
		
			
	}
			
	
	
	
	
private function file_get_size($file) {
    //open file
    $fh = fopen($file, "r");
    //declare some variables
    $size = "0";
    $char = "";
    //set file pointer to 0; I'm a little bit paranoid, you can remove this
    fseek($fh, 0, SEEK_SET);
    //set multiplicator to zero
    $count = 0;
    while (true) {
        //jump 1 MB forward in file
        fseek($fh, 1048576, SEEK_CUR);
        //check if we actually left the file
        if (($char = fgetc($fh)) !== false) {
            //if not, go on
            $count ++;
        } else {
            //else jump back where we were before leaving and exit loop
            fseek($fh, -1048576, SEEK_CUR);
            break;
        }
    }
    //we could make $count jumps, so the file is at least $count * 1.000001 MB large
    //1048577 because we jump 1 MB and fgetc goes 1 B forward too
    $size = bcmul("1048577", $count);
    //now count the last few bytes; they're always less than 1048576 so it's quite fast
    $fine = 0;
    while(false !== ($char = fgetc($fh))) {
        $fine ++;
    }
    //and add them
    $size = bcadd($size, $fine);
	$size = floor( $size/1000 );
    fclose($fh);
    return $size;
}
	
	
	
} // final clase


?> 



