<?php

class WebProducts extends AdminBaseSection
{
    private  $columns;
	protected static $mainQueryWhere;
    private $productfields;

    /**
     * Intialize
     * @param array $db database
     */
    public function __construct($db){
        $this->columns = array(
            array( 'db' => 'id', 'dt' => 'DT_RowId' ),
            array( 'db' => 'reference',  'dt' => 'reference' ),
            array( 'db' => 'family',  'dt' => 'family' ),
            array( 'db' => 'category',  'dt' => 'category' ),
            array( 'db' => 'name',  'dt' => 'name' ),
            array( 'db' => 'price',  'dt' => 'price' ),
            array( 'db' => 'stock',  'dt' => 'stock' ),
            array( 'db' => 'active',  'dt' => 'status', 'formatter' => function ($d, $row) {
                if($d != 1) return '<i class="fa fa-times text-danger css-text20" aria-hidden="true"></i>';
                else return '<i class="fa fa-check text-success css-text20" aria-hidden="true"></i>';
            })
        );
    }

    static function getCourses($dbo, $limit = "4")
    {
        $q = "SELECT p.id, p.reference, pl.name FROM products p INNER JOIN product_language pl ON pl.product_id = p.id AND pl.language_id='1' WHERE active='1' ORDER BY reference ASC LIMIT 0, ".$limit;

        $products = $dbo->query($q);
        return $products;
    }
}


?>