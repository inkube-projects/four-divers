<?php


   class AdminEmailDispatcher extends AdminBaseSection  {
	 
	
	/**
   * @var Meekrodb Database handler
   */   
  
   private  $emailObj;
   private  $templateObj;
   private  $testMessage;
   
   private $config;
   
   private $templateHeader;
   private $templateFoot;
	   
   /**
   * Intialize
   * @param array $db database
   */
   public function __construct($db){
        self::$dbh=$db;
		
		
	$this->config["email"] = array(
    "email" => "app@fourdivers.com",
	"password" => "fN5fi&58",
	"smtpHost" => "mail.fourdivers.com",
	"fromName" => "FOUR DIVERS",
	"urlImages" => self::$imageUrl."emails/"
     );
		
	
	
	  // cargamos html	
	    require ($_SERVER['DOCUMENT_ROOT'].'/sys/assets/email_template/template.php');
		
		
    }
	
	
	
	
		public function sendEmail($emailObj, $returnTemplateFilled=false,$returnOnlyMessage=false){
		
		$res = new stdClass();
		/* es fundamental clonar (clone $emailObj) el objeto para que éste sea independiente ya que se le añaden valores en esta función y en caso de ser reutilizado ya los tendría asignados y daría error.
		// En AdminOrdersEmailsBodyCreator en la función  orderConfirmed se generaba este objeto se pasaba aquí a esta función la cual le añadía valores (message y subject) pero en realidad lo hacía a la instancia del objeto generado allí y como posteriormente lo empleábamos para, modificados unos valores, pasarlo a la función emailSaleAffiliate y al ya tener asignado los valores que se generan aquí no se reescribian dando como error el que se grababa en base de datos (storeEmail) siempre el mismo asunto y mensaje.
		if(!isset($this->emailObj->message)) como quedaba definido la primera vez ya no se reasignaba.
		$this->testMessage=$testMessage;
		*/
		
		
		
		$this->emailObj = clone $emailObj;
		
		$addBCC = ( isset($this->emailObj->addBCC) ) ?  true : false; // copia oculta
		
		
		if(isset($this->emailObj->emailConfig)){
			
			$this->config["email"]['email']=$this->emailObj->emailConfig['email'];
			$this->config["email"]["password"] = $this->emailObj->emailConfig['password'];
	        $this->config["email"]["smtpHost"] = $this->emailObj->emailConfig['host'];
			$this->config["email"]["fromName"] = $this->emailObj->emailConfig['from'];
			
		}
		
		
		$this->templateHeader = str_replace("[mainEmailImage]", $this->emailObj->mainEmailImage,$this->templateHeader);
		// 1º) Derimimos si emplea plantilla o no 
	
		if($this->emailObj->templateId>0){
			
			// get template content
			$this->templateObj=self::getTemplateDetail($this->emailObj->templateId,$this->emailObj->languageId);
			if(!is_null($this->templateObj['id'])){
				// rellenamos aquellas áreas del email de las que recibamos valores (mensaje/body se da por supuesto)
				$this->templateObj['message']=$this->fillTemplate($this->templateObj['message'],$this->emailObj->varsMessage);
				if(count($this->emailObj->varsFrom)>0)$this->templateObj['from']=$this->fillTemplate($this->templateObj['from'],$this->emailObj->varsFrom);
				if(count($this->emailObj->varsSubject)>0)$this->templateObj['subject']=$this->fillTemplate($this->templateObj['subject'],$this->emailObj->varsSubject);
				
				if(count($this->emailObj->varsFooter)>0)$this->templateFoot=$this->fillTemplate($this->templateFoot,$this->emailObj->varsFooter);
				
				
				// NO se recibe un cuerpo de mensaje como tal, sino un conjunto de valores y se construye en la plantilla reemplazando las [claves] por esos valores
				// en tal caso se lo asignamos a la variable que espera función para almacenar $this->storeEmail()
				if(!isset($this->emailObj->message)) $this->emailObj->message=$this->templateObj['message'];
				if(!isset($this->emailObj->subject)) $this->emailObj->subject=$this->templateObj['subject'];
					// template Ok 
					
					$att=isset( $this->emailObj->attachment ) ? $this->emailObj->attachment : '';
					
					// si sólo queremos que se parsee la plantilla pero que no se envíe desde aquí, la retornamos
					if($returnTemplateFilled==true){ 
					    
					    if($returnOnlyMessage==true) { // sólo retornamos el cuerpo (PDF regalo p.e.), no la plantilla en este caso no lo guardamos
							$this->emailObj->message=$this->templateObj['message'];
						}
					    else { // se rellena la plantilla del correo y será enviado de una web satélite
							$this->emailObj->message=$this->templateHeader.$this->templateObj['message'].$this->templateFoot;
							
						 }
						
						$stored=$this->storeEmail();
					    return $this->emailObj; exit();
					  }
					
					
					if($this->dispatchEmail($this->templateObj,$this->emailObj->to,$att,$addBCC)){
						$res->sendResult=1;
						$res->attachment=$att;
						$res->message=$this->templateObj['message'];
						$res->allowUrlOpen=ini_get('allow_url_fopen');
						// save email in database 
						$res->storeEmailResult=$this->storeEmail();
						} else {
							$res->error="Error sending email: ".$this->config["email"]['email']." ".$this->config["email"]["password"]." ".$this->config["email"]["smtpHost"]." ".$this->config["email"]["fromName"];
							}
					
				
				} else { // error template not found
					$res->error="Template not found, template Id:".$emailObj->templateId.", language Id: ".$emailObj->languageId;
					
					
					}
			
			} // final templateId>0
		
		
		 return  $res;
	
		
	} 
	

	
	private function storeEmail (){
		
		if( isset($this->emailObj->section) && isset($this->emailObj->objectId) && isset($this->emailObj->message) ){
			
			date_default_timezone_set('Europe/Madrid'); 
			$date = date('Y-m-d H:i:s', time());
			$att = (isset($this->emailObj->attachment)) ? $this->emailObj->attachment : NULL;
			
			$messageNoStyle = $this->delete_all_between('<style>', '</style>', $this->emailObj->message); 
			
			self::$dbh->insert('emailSent_store', array(
					'section' => $this->emailObj->section,
					'objectId' => $this->emailObj->objectId,
					'subject' => $this->emailObj->subject,
					'body' => $messageNoStyle,
					'to' => $this->emailObj->to,
					'attachment' => $att,
					'date_send' => $date
					));
					
			return self::$dbh->insertId(); 
			
			
			}
			
			return "SECTION:".$this->emailObj->section." OBJECT:".$this->emailObj->objectId." MESSAGE ".$this->emailObj->message;
		
		}
		
		
	private static function getTemplateDetail($templateId,$languageId){
		   
			 // si la plantilla no estuviera en el idioma indicado la tomamos en inglés
			 $detail = self::$dbh->queryFirstRow("SELECT el.id, el.subject, el.from, el.message  
FROM emails ems
LEFT JOIN email_language el ON el.emailId=ems.id AND languageId=(CASE WHEN (SELECT COUNT(*) FROM email_language WHERE el.emailId=ems.id AND languageId=%i1) > 0 THEN %i1 ELSE %i2 END)
WHERE ems.id=%i0",$templateId,$languageId,2);
			
				return $detail;
				
		}
	

		
	//***** SEND EMAIL PHPMAILER
	
	 public function dispatchEmail($objEmail,$email,$attachment=false,$addBCC=false,$stringAttachment=false,$addHeaderFooter=true,$urlAttachment=false){
	  
	    $mail = new PHPMailer(); 
		//Enable SMTP debugging. 
		
		// la versión 5.6 de PHP  valida los certificados en las conexiones ssl/tls
		// si el servidor no tuviera un certificado debemos  poner las siguientes SMTPOptions (No recomendado)
		$mail->SMTPOptions = array(
			'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true
			)
		);
		
		//$mail->SMTPDebug = 3;                               
		//Set PHPMailer to use SMTP.
		$mail->isSMTP();            
		//Set SMTP host name                          
		$mail->Host =  $this->config['email']['smtpHost']; //"smtp.mailgun.org";
		//Set this to true if SMTP host requires authentication to send email
		$mail->SMTPAuth = true;                          
		//Provide username and password     
		$mail->Username = $this->config['email']['email']; //"postmaster@sandboxcff8db24e3d04d37be29b794c94db1e4.mailgun.org";                  
		$mail->Password =  $this->config['email']['password'];     //"79efd2458504726d40e82400bd856749";
		
		/* MAIL GUN 
		$mail->Host = "smtp.mailgun.org";
		$mail->Username = "postmaster@sandboxcff8db24e3d04d37be29b794c94db1e4.mailgun.org";                  
		$mail->Password = "79efd2458504726d40e82400bd856749";
		*/
		                      
		//If SMTP requires TLS encryption then set it
		$mail->SMTPSecure = "tls";                           
		//Set TCP port to connect to 
		$mail->Port = 587;                                   
		$mail->From = $this->config['email']['email'];
		$mail->FromName = $this->config['email']['email']; //$objEmail['from'];
		$mail->addAddress($email, "");
		$mail->isHTML(true);
		$mail->Subject = $objEmail['subject'];
		
		$mail->Body = ($addHeaderFooter) ? $this->templateHeader.$objEmail['message'].$this->templateFoot : $objEmail['message'];
		
		$mail->AltBody = "This is the plain text version of the email content";
		
		if($addBCC){ $mail->AddBCC($this->config['email']['email'], "");  }
		
		if($attachment){
			
			$attachment=realpath(__DIR__ . '/../..').trim($attachment);
			
			if (file_exists(rtrim($attachment))) {
    			$existe = "El fichero $nombre_fichero existe";
			} else {
   				 $existe = "El fichero $nombre_fichero no existe";
			}
			
			
			if($stringAttachment==false){
				if(!$mail->addAttachment($attachment,"")){echo "Error adjuntando archivo:".$attachment.", is_readable ".is_readable($attachment)." file exists ".$existe; exit();
				}
			}
			
			// añadimos pdf como cadena 
			
			else $mail->addStringAttachment($attachment, 'attachment.pdf','base64','application/pdf');
			
			
			}
			
		
		// Activo condificacción utf-8
        $mail->CharSet = 'UTF-8';
		if(!$mail->send()) 
		{
			return $this->config["email"]["smtpHost"]." ".$this->config["email"]['email'].$this->config["email"]["password"]." allow open ".ini_get('allow_url_fopen');
		} 
		else 
		{
			return true;
		}
	   
    
  }
  
	
		//******************************************************** Parse email body: Replace/Delete/Fill tags 	
	private function fillTemplate($fillable,$vars){
	
		// recorremos todas las variables 
		foreach ($vars as $varArray) {
			$openTag='[';
			$closeTag=']';
			// etiquetas comentadas para poder ser incluidas en áreas donde no puede haber contenido de texto sin el uso previo de etiquetas html (entre dos <tr> p.e., listado de productos)
			if( isset($varArray['anchorComented']) ){ 
			    $openTag='<!--['; 
				$closeTag=']-->';
				}
			// según el tipo obraremos
			if ($varArray['type'] == 'text') {
				$fillable = str_replace($openTag.$varArray['name'].$closeTag, $varArray['value'],$fillable);
				  
			} elseif ($varArray['type'] == 'image') {
				
				$size = (isset($varArray['size_mm'])) ?  ' style="margin-top:2mm; width: '.$varArray['size_mm'].'mm"' : '';
				
				
				$fillable = str_replace($openTag.$varArray['name'].$closeTag, '<img src="'.$varArray['value'].'" '.$size.' />',$fillable);
				  
			} elseif ($varArray['type'] == 'boolean') { 
				
				$optionDelete=( $varArray['value']==1 ) ? '0' : '1';
				// Option not selected: tags and content between to delete [namevar_$optionDelete_init] ...content...[namevar_$optionDelete]
				$fillable=$this->deleteTagFromInitToEnd($varArray['name']."_".$optionDelete,$fillable,$openTag,$closeTag);
				 
				// Option selected: remove only tags embrace right option
				$arrayTagsSelected=array($openTag.$varArray['name']."_".$varArray['value']."_init]",$openTag.$varArray['name']."_".$varArray['value'].$closeTag);
				
				$fillable=str_replace($arrayTagsSelected,'',$fillable);
				
	
			} elseif ($varArray['type'] == 'zerodelete') {
				
				if($varArray['value']===0){
					
					$fillable=$this->deleteTagFromInitToEnd($varArray['name'],$fillable,$openTag,$closeTag);
					
					}
				else { // delete init tag and replace close tag with value
				
					$fillable=str_replace($openTag.$varArray['name']."_init".$closeTag,'',$fillable);
					
					$printValue = ($varArray['value']=="void") ? '' : $varArray['value'].";";
					
					$fillable=str_replace($openTag.$varArray['name'].$closeTag,$printValue,$fillable);
					
					}
   				 
			}
		 
		}
   
   
		return $fillable;
	}
		
		
		
		//******************************************** Tags removal functions **************************
		
		private function deleteTagFromInitToEnd($value,$string,$openTag,$closeTag){
			    $initDelete=$openTag.$value."_init".$closeTag;
				$endDelete=$openTag.$value.$closeTag;
				$string=$this->delete_all_between($initDelete, $endDelete, $string);
				return $string;
			}
		
		
		private function delete_all_between($beginning, $end, $string) {
			  $beginningPos = strpos($string, $beginning);
			  $endPos = strpos($string, $end);
			  if ($beginningPos === false || $endPos === false) {
				return $string;
			  }
			
			  $textToDelete = substr($string, $beginningPos, ($endPos + strlen($end)) - $beginningPos);
			
			  return str_replace($textToDelete, '', $string);
			}

	
		

	
	   
	} // class end



?> 