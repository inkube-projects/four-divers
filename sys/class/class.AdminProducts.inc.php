<?php

class AdminProducts extends AdminBaseSection {

	private  $columns;
	protected static $mainQueryWhere;
  private $productfields;

   /**
   * Intialize
   * @param array $db database
   */
   public function __construct($db){
   	self::$dbh=$db;
   	$this->columns = array(
   		array( 'db' => 'id', 'dt' => 'DT_RowId' ),
      array( 'db' => 'reference',  'dt' => 'reference' ),
      array( 'db' => 'family',  'dt' => 'family' ),
      array( 'db' => 'category',  'dt' => 'category' ),
      array( 'db' => 'name',  'dt' => 'name' ),
      array( 'db' => 'price',  'dt' => 'price' ),
      array( 'db' => 'stock',  'dt' => 'stock' ),
      array( 'db' => 'active',  'dt' => 'status', 'formatter' => function ($d, $row){
        if($d!=1)return '<i class="fa fa-times text-danger css-text20" aria-hidden="true"></i>';
        else return '<i class="fa fa-check text-success css-text20" aria-hidden="true"></i>';
      }
    )

    );

   }
   

   private static function createWhere (){

	 // WHERE
		self::$mainQueryWhere = new WhereClause('and'); // create a WHERE statement of pieces joined by ANDs
    
    self::$mainQueryWhere->add('p.deleted IS NULL'); // productos borrados no se muestran

		$busqueda=$_POST['search']['value'];

		if(strlen($busqueda)>0){ 
			  $subclause = self::$mainQueryWhere->addClause('or'); // add a sub-clause with ORs
			  $subclause->add('pl.`name` LIKE %ss', $busqueda); 
        $subclause->add('reference LIKE %ss', $busqueda);
      }

      if( $_POST['family_id']>0 ) self::$mainQueryWhere->add('pf.id = %i', $_POST['family_id']); 
     
    }

   /*
   *  LISTADO DE PRODUCTOS
   */
    public function listProducts(){
     if(isset($_POST['token']) && $_POST['token']==$_SESSION['token'] && isset($_POST['start']) ){

		    //self::$dbh->debugMode();	
      $order = self::order( $_POST, $this->columns );

      self::createWhere();

      $listado = self::$dbh->query("SELECT SQL_CALC_FOUND_ROWS p.id, p.stock, reference, pl.`name`, price, active, pc.`name` AS category, pf.`name` AS family FROM products p
        INNER JOIN product_language pl ON pl.product_id = p.id AND pl.language_id = 1
        LEFT JOIN products_categories pc ON pc.id = p.category_id
        LEFT JOIN products_families pf ON pf.id = pc.family_id WHERE %l2 ".$order." LIMIT %i0,%i1",$_POST['start'],$_POST['length'],self::$mainQueryWhere);

      $recordsTotalNum=self::$dbh->query("SELECT FOUND_ROWS();");

      $recordsFiltered = (strlen($busqueda)<=0) ? $recordsTotalNum[0]['FOUND_ROWS()'] : self::$dbh->count();

      if(!isset($_POST['isJs'])) return $listado;
      else {	 

       return json_encode(array(
        "draw"            => isset ( $_POST['draw'] ) ? intval( $_POST['draw'] ) :	0,
        "recordsTotal"    =>  $recordsTotalNum[0]['FOUND_ROWS()'] ,
        "recordsFiltered" => intval( $recordsFiltered ),
        "data"            => self::data_output( $this->columns, $listado )
      ));

     }

		} // existe token y es correcto
		
	} 


	/*
	* Leer detalle de producto
	* @param  Meekrodb object  $sb    
	* @param  int  $id  
	*/	

	public static function productDetail($db,$id){
		if(intval($id)>0){
			self::$dbh=$db;
			$detail = self::$dbh->queryFirstRow("SELECT p.*, pl.name, pl.description, pl.requirements, pl.included, pl.objectives , pl.metaKeywords, pl.metaTitle, pl.metaDescription, pc.family_id FROM  products p INNER JOIN product_language pl ON pl.product_id=p.id LEFT JOIN products_categories pc ON pc.id = p.category_id WHERE p.id=%i0",$id);
      return $detail;
    }

  }



private function fillProdructData(){
  $data = array (
    'active' => isset($_POST['active']) ? 1 : NULL,
    'featured' => isset($_POST['featured']) ? 1 : NULL,
    'provider_id' => $_POST['providerId'],
    'category_id' => $_POST['categoryId'],
    'reference' => $_POST['reference'],
    'price' => floatval($_POST['price']),
    'specs' => $_POST['specs'],
  );
  return $data;
}


/*
* ADD PRODUCT
*/
public function addProduct(){

 if(isset($_POST['token']) && $_POST['token']==$_SESSION['token'] && isset($_POST['language']) ){
  //self::$dbh->debugMode();
  if( intval($_POST['id'])==0 && $this->checkReferenceInUse() ){ $resArr["error"]='duplicated-reference'; echo json_encode($resArr);  return; }
  else {
   if( isset($_POST['id']) && intval($_POST['id'])==0 && $_POST['language']==self::$mainLanguageId){
    self::$dbh->insert('products', $this->fillProdructData() );
    $_POST['id'] = self::$dbh->insertId(); 
  }
  //Add new language entry
  if( isset($_POST['id']) && intval($_POST['id'])>0 ){ // ya existe entrada en español sólo añadir language
       self::$dbh->insert('product_language', array(
         'product_id' => intval($_POST['id']),
         'language_id' => intval($_POST['language'])
       ));

       $this->updateProduct($_POST['id']);

     }
   }
 }

} 


/*
* UPDATE PRODUCT
*/

 public function updateProduct($addedNew=0){

  if( isset($_POST['token']) && $_POST['token']==$_SESSION['token'] ){
    $resArr=array();
    if( $addedNew==0 && $this->checkReferenceInUse($_POST['id']) ){ $resArr["error"]='duplicated-reference'; }
    else{
      $this->productfields=array(
        'name' => $_POST['name'],
        'description' => $_POST['description'],
        'requirements' => $_POST['requirements'],
        'included' => $_POST['included'],
        'objectives' => $_POST['objectives'],
        'metaTitle' => $_POST['metaTitle'],
        'metaKeywords' => $_POST['metaKeywords'],
        'metaDescription' => $_POST['metaDescription'],
        //'url_canonical' => $_POST['url_canonical']
      );

       $where = new WhereClause('and'); // create a WHERE statement of pieces joined by ANDs
       $where->add('product_id=%i', $_POST['id']);
       $where->add('language_id=%i', $_POST['language']);
       self::$dbh->update('product_language', $this->productfields, '%l', $where);

       $counter = self::$dbh->affectedRows();

      // si fuera nueva ya he añadido antes en la función addCategory.  
       if($addedNew==0){
        self::$dbh->update('products', $this->fillProdructData(), "id=%i ", $_POST['id']);
        $counter2 = self::$dbh->affectedRows();
      } else { $counter2=0; }

      $resArr["resultado"] = ($counter || $counter2)  ? 1 : 0; 
        // si he añadido alguna nueva paso id a detalle
      if($addedNew>0) $resArr["id"] = $addedNew;
      self::$dbh->disconnect(); 

    }
    
    echo json_encode($resArr);

  }
} 


/*
* Language product info
*/
public function languageProduct($return=false){
 if(isset($_POST['token']) && $_POST['token']==$_SESSION['token'] && isset($_POST['languageId']) ){
   $resArr["resultado"]=array();
   $detail= self::$dbh->queryFirstRow("SELECT p.*, pl.* FROM products p INNER JOIN product_language pl ON pl.product_id=p.id WHERE p.id=%i0 AND pl.language_id=%i1 ",$_POST['productId'],$_POST['languageId']);
   if(is_null($detail)) $resArr["resultado"]=0;
   else {
     $resArr["resultado"]=1;
     $resArr["data"]=$detail;
   }
   self::$dbh->disconnect();  
   if($return){
     return $detail;
   } else {
     echo json_encode($resArr);
   }

 }
}

  /**
  * Avoid duplicate reference
  */
   private function checkReferenceInUse($id=NULL){
    //self::$dbh->debugMode();
    if(strlen($_POST['reference'])==0) return false;
    $where = new WhereClause('and');
    $where->add('reference=%s', $_POST['reference'] );    
    if($id!=NULL){$where->add('id!=%i', $_POST['id'] );  }
    $isInUse = self::$dbh->queryFirstRow("SELECT count(id) as cnt FROM products WHERE %l", $where);
    return $isInUse['cnt']>0 ? true : false;
      
  }



  public static function getBasicInfoByFamily($db,$familyId){
    self::$dbh=$db;
    $products = self::$dbh->query("SELECT p.id, p.`reference`, pl.name FROM products p 
      INNER JOIN product_language pl ON pl.product_id = p.id AND pl.language_id=%i0
      WHERE active=1 AND category_id IN (SELECT id FROM products_categories WHERE family_id=1) ORDER BY `reference` ASC",1,$familyId);
    return $products;
  }


 /*
 * Load categories an providers from family
 */
  public function getCategoriesAndProviders(){
     $res=array();
     $res['categories']=self::readTable(self::$dbh,'products_categories', 'id, name', 'name','ASC', 'family_id='.intval($_POST['family_id']));
     $res['providers']=self::readTable(self::$dbh,'products_providers', 'id, name', 'name','ASC', 'product_families LIKE "%\"'.intval($_POST['family_id']).'\"%"');

     echo json_encode($res);
  }

 // daremos valor a su campo is_delete con fecha de hoy, una tarea CRON deberá de borrar definitivamente todos los borrados pasados x tiempo
  public function deleteProduct(){
    if(isset($_POST['token']) && $_POST['token']==$_SESSION['token'] && isset($_POST['id']) ){
     self::$dbh->update('products', ['deleted'=>date('Y-m-d')], "id=%i ", $_POST['id']); 
      $resArr["resultado"]=(self::$dbh->affectedRows())?1:-1; 
    }
      echo json_encode($resArr);
  } 


} // final clase


?> 



