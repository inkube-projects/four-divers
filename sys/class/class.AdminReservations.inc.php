<?php

class AdminReservations extends AdminBaseSection {

	private  $columns;
	protected static $mainQueryWhere;
  private $reservationfields;

   /**
   * Intialize
   * @param array $db database
   */

   /*
   { "data": "DT_RowId" },
        { "data": "date" },
        { "data": "client" },
        { "data": "type" },
        { "data": "status" }
   */

   public function __construct($db){
   	self::$dbh=$db;
   	$this->columns = array(
   		array( 'db' => 'id', 'dt' => 'DT_RowId' ),
      array( 'db' => 'date',  'dt' => 'date' ),
      array( 'db' => 'client',  'dt' => 'client' ),
      array( 'db' => 'type',  'dt' => 'type' ),
      array( 'db' => 'status',  'dt' => 'status', 'formatter' => function ($d, $row){
        if($d!=1)return '<i class="fa fa-times text-danger css-text20" aria-hidden="true"></i>';
        else return '<i class="fa fa-check text-success css-text20" aria-hidden="true"></i>';
      }
    )

    );

   }
   

   private static function createWhere (){

	 // WHERE
		self::$mainQueryWhere = new WhereClause('and'); // create a WHERE statement of pieces joined by ANDs
    
    self::$mainQueryWhere->add('r.deleted IS NULL'); // reservationos borrados no se muestran

		$busqueda=$_POST['search']['value'];

		if(strlen($busqueda)>0){ 
			  $subclause = self::$mainQueryWhere->addClause('or'); // add a sub-clause with ORs
			  $subclause->add('u.`name` LIKE %ss', $busqueda); 
        $subclause->add('u.surname` LIKE %ss', $busqueda);
        $subclause->add('r.id = %i', $busqueda);
      }

      //if( $_POST['family_id']>0 ) self::$mainQueryWhere->add('pf.id = %i', $_POST['family_id']); 
     
    }

   /*
   *  LISTADO DE PRODUCTOS
   */
    public function listReservations(){
     if(isset($_POST['token']) && $_POST['token']==$_SESSION['token'] && isset($_POST['start']) ){

		    //self::$dbh->debugMode();	
      $order = self::order( $_POST, $this->columns );

      self::createWhere();

      $listado = self::$dbh->query("SELECT SQL_CALC_FOUND_ROWS r.id, r.status, DATE_FORMAT(r.date_created,'Y-m-d') AS date, CONCAT (u.`name`,' ',u.surname) AS client, (SELECT count(id) FROM reservation_items WHERE reservation_id=r.id) AS type FROM reservations r INNER JOIN users u ON u.id=r.user_id WHERE %l2 ".$order." LIMIT %i0,%i1",$_POST['start'],$_POST['length'],self::$mainQueryWhere);

      $recordsTotalNum=self::$dbh->query("SELECT FOUND_ROWS();");

      $recordsFiltered = (strlen($busqueda)<=0) ? $recordsTotalNum[0]['FOUND_ROWS()'] : self::$dbh->count();

      if(!isset($_POST['isJs'])) return $listado;
      else {	 

       return json_encode(array(
        "draw"            => isset ( $_POST['draw'] ) ? intval( $_POST['draw'] ) :	0,
        "recordsTotal"    =>  $recordsTotalNum[0]['FOUND_ROWS()'] ,
        "recordsFiltered" => intval( $recordsFiltered ),
        "data"            => self::data_output( $this->columns, $listado )
      ));

     }

		} // existe token y es correcto
		
	} 


	/*
	* Leer detalle de reservationo
	* @param  Meekrodb object  $sb    
	* @param  int  $id  
	*/	

	public static function reservationDetail($db,$id){
		if(intval($id)>0){
			self::$dbh=$db;
			$detail = self::$dbh->queryFirstRow("... WHERE p.id=%i0",$id);
      return $detail;
    }

  }



private function fillProdructData(){
  $data = array (
    'status' =>  1 ,
    'date_created' => date ('Y-m-d H:i')
  );
  return $data;
}


/*
* ADD PRODUCT
*/
public function addReservation(){

 if(isset($_POST['token']) && $_POST['token']==$_SESSION['token'] ){

 }

} 


/*
* UPDATE PRODUCT
*/

 public function updateReservation($addedNew=0){

  if( isset($_POST['token']) && $_POST['token']==$_SESSION['token'] ){
    
   
    
    echo json_encode($resArr);

  }
} 




 // daremos valor a su campo is_delete con fecha de hoy, una tarea CRON deberá de borrar definitivamente todos los borrados pasados x tiempo
  public function deleteReservation(){
    if(isset($_POST['token']) && $_POST['token']==$_SESSION['token'] && isset($_POST['id']) ){
     self::$dbh->update('reservations', ['deleted'=>date('Y-m-d')], "id=%i ", $_POST['id']); 
      $resArr["resultado"]=(self::$dbh->affectedRows())?1:-1; 
    }
      echo json_encode($resArr);
  } 


} // final clase


?> 



